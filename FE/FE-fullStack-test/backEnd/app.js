
var express = require('express')
var app = express()
var cors = require('cors');

var path = require("path");
var fs = require("fs");
const csv = require('csv-parser')

// Then use it before your routes are set up:

var MongoClient = require('mongodb').MongoClient;
var mongoURL = "mongodb://localhost:27017/"
var dbName = "csvdb";
var collectionName = "persondetail";
var bodyParser = require('body-parser')

const port = 4000
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false })) 
app.use(bodyParser.json())

app.get('/', function (req, res) {
  res.send('Hello World')
});

// mongoDB insert data
insertManyToMongo = (mongoURL, dbName, collectionName, data) => {
    MongoClient.connect(mongoURL, dbName, collectionName, data, function(err, db) {
        if (err) throw err;
        var dbo = db.db(dbName);  // use db csvdb
        dbo.collection(collectionName).insertMany(data, function(err, res) {  // use persondetail table(collection)
          if (err) throw err;
          console.log("Number of documents inserted: " + res.insertedCount);
          db.close();
        });        
      });
}

// test  
app.post('/api/test', function (req, res) {  
    console.log('in /api/test');  
    console.log(req.body);
});  

app.get('/api/loadCSVFile', function (req, res) {  

  console.log('in /api/loadCSVFile');  
  var stream = fs.createReadStream("data/coding-test-file.csv");
  var resData = []; 
  var csvStream = csv()
      .on("data", function(data){
          resData.push(data);
          //console.log(data);
      })
      .on("end", function(){
          console.log('load ended');
          console.log(resData);
          // store data into db
          insertManyToMongo(mongoURL, dbName, collectionName, resData);
          // return data to frontEnd
          res.json(resData);
      });
  stream.pipe(csvStream);      // Reading from one file(form the stream) and writing to another stream(connecting two streams using pipe)   
                               // the files stream will be writing to csvStream
});  

app.post('/api/CSVFile', function (req, res) {  
    console.log('in /api/CSVFile', req.body.message);
    // store the data to mongoDB
    // parepare data
    console.log('type of req.body.message', typeof req.body.message);

    var colNumber = req.body.message[0].length;
    var data = [];
    for (let i = 1; i < req.body.message.length; i++) {
        let tmpRow = {};
        for (let j = 0; j < req.body.message[0].length; j++) {
            let key = req.body.message[0][j];
            let value = req.body.message[i][j];
            tmpRow[key] = value;
        }
        data.push(tmpRow);
    }
    console.log('data-----',data);
    insertManyToMongo(mongoURL, dbName, collectionName, data);
  });  

app.get('/showCSVFile', function (req, res) { 
    res.sendFile(path.join(__dirname+'/web/showCSVFileFromBackend.html'));   
  })

app.get('/postCSVFile', function (req, res) { 
    res.sendFile(path.join(__dirname+'/web/postDataToBackEnd.html'));   
  })

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
