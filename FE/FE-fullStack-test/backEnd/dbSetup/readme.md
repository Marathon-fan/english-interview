

install mongo docker
```sh
docker-compose up -d
docker ps
```

create mongo db 
```sh

docker exec -it dbsetup_mongodb_1 /bin/bash

# tradeLedger is the db name, sampleCollection is the collection name

mongo
show dbs
use csvdb
show tables
use persondetail
db.persondetail.find({})
db.persondetail.find().pretty()

```


# commands collections

https://dzone.com/articles/top-10-most-common-commands-for-beginners

https://stackoverflow.com/questions/30380751/importing-json-from-file-into-mongodb-using-mongoimport

