import React,  { Component }from 'react';

import { Button, Comment,  Form, Header  } from 'semantic-ui-react'
import moment from 'moment';
import ReactDOM from 'react-dom'
import CSVReader from 'react-csv-reader'
import axios from 'axios';
import SortableTbl from '../../node_modules/react-sort-search-table';
// import SortableTbl from '../node_modules/react-sort-search-table/lib/react-sort-search-table';

let tableData = []

class Assessment extends Component {

  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
    this.postData = this.postData.bind(this);
    this.handleFileLoaded = this.handleFileLoaded.bind(this);
    this.state = {
      serverMsg: 'Thanks for talking to me!',
      userMsg: 'Let me ask you something',
      textArea: 'Please in your question'
    };
  }

  handleSubmit(event) {
    const data = new FormData(event.target);
  }
 
  handleChangeTextArea(event) {
    this.setState({ textArea: event.target.value });
  }  

  postData(jsonData) {
    console.log(jsonData);

    for (let i = 1; i < jsonData.length; i++) {
      let tmpRow = {};
      tmpRow["name"] = jsonData[i][0];
      tmpRow["date"] = jsonData[i][1];
      tmpRow["number"] = jsonData[i][2];
      tmpRow["description"] = jsonData[i][3];
      tableData.push(tmpRow);
    }
    console.log('tableData', tableData);
    this.render();


    //tableData.push

    axios.post(`http://localhost:4000/api/CSVFile`, {message: jsonData})
      .then(res => {
        console.log(res);
      })
  }

  handleFileLoaded(event) {
    this.postData(event);
  }



  render() {
    return(
      <div>
       <CSVReader
        cssClass="csv-input"
        label="Select Your file"
        onFileLoaded={this.handleFileLoaded}
        onError={this.handleDarkSideForce}
        inputId="ObiWan"
        inputStyle={{color: 'red'}}
       />
       <ProductsTblPage/>
       </div>

    );
  };
};


var ProductsTblPage = (props) => {
  let col = [
      "name",
      "date",
      "number",
      "description"
  ];
  let tHead = [
    "name",
    "date",
    "number",
    "description"
  ];

  return (
      <SortableTbl tblData={tableData}
          tHead={tHead}
          dKey={col}
      />
  );
};
ProductsTblPage.propTypes = {
  
};

//ReactDOM.render(<ProductsTblPage/>, document.getElementById("Assessment"));

export default Assessment;