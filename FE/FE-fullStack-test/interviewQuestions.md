

## how to run it  




Please build a single page application for patients using the CSV file attached.

 
```
Part 1

Create a Ruby on Rails application.
With the provided CSV file, you will need to create a form that can upload the file and import
the data into a database (prefer PostgreSQL)

Once the data has been imported, you will need to display the imported data.
```




Part 2 

 
```
Once Part 1 is completed, you will need to add the ability to order the Name, Date and Number columns by ascending/descending when clicking on the column headers. This should be achieved without reloading the page. 

Add a search field so that you can also filter the table results by Name. This should also be done without reloading the page.

Please send the task back after 1 hour regardless of if all Parts are completed.  

If the task isn’t received after 1.5 hour (MAX.), it will not be assessed.
``` 

When complete

Zip and email the code to scott@zenoit.com.au
If you are familiar with Heroku, after the test - please also upload & send us the link
