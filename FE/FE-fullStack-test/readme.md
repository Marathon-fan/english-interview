

## key points 



```sh
use fast-csv to parse csv file
#use csv-parser to parse csv
use csvtojson to convert csv file to json 


```




## how to run it   

```sh

cd backEnd/dbSetup
docker-compose up -d # set up db

cd backEnd
npm install
node app

cd frontend-react-app
npm install
npm start


navigate to http://localhost:3000/Assessment

 
```

