


## react-styled-component   



### files strucutre
```sh
/src           # angular5 files
    /components
        /common            # buttons, etc
        /containers        # interact with data, and generally contain business logic 
        /pages             # consist of top-level pages, like home page, dashboard, landing page



```


### create new react-app.  

```sh
sudo npm install -g create-react-app

create-react-app react-styled-component

npm install --save styled-components
```
