# AngularTodolist

After editing, click "save all"    
**save all**

## how to run it  

```sh
npm install  
npm start   
```

## environment, class, service and components   

```sh
sudo npm install -g typescript
sudo npm install -g @angular/cli
brew install watchman  # helps Angular CLI when it needs to monitor files in your filesystem for changes


ng new angular-todolist
cd angular-todolist
npm install --save bootstrap
# modify the file
modily: angular.json file (in angular2, .angular-cli.json)
"styles":[
        "./node_modules/bootstrap/dist/css/bootstrap.min.css"
]

ng g class items/shared/item
ng g service items/shared/item
ng g component items/item-detail 
ng g component items/items-list
ng g component items/item-form
```

## source    

### item  
item.ts
```java
export class Item {
  title: string;
  body: string;
  timeStamp: Date = new Date();
}
```

### item service  

item.service.ts
```java
import { Injectable } from '@angular/core';
import { Item } from './item';

@Injectable()
export class ItemService {

  public items: Item[] = [];

  constructor() { }

  getList(): Item[] {
    return this.items;
  }

  addItem(curItem: Item): void {
    this.items.push(curItem);
  }

  deleteItem(curItem: Item): void {
    const Index = this.items.indexOf(curItem);
    this.items.splice(Index, 1);
  }

}
```

in app.module.ts
```java
import { ItemService } from './items/shared/item.service';
  providers: [ItemService],
```

### item form    

item-form.component.ts
```java
import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Item } from '../shared/item';
import { ItemService } from '../shared/item.service';

@Component({
  selector: 'app-item-form',
  templateUrl: './item-form.component.html',
  styleUrls: ['./item-form.component.css']
})
export class ItemFormComponent implements OnInit {

  curItem: Item = new Item();

  constructor(private itemSvc: ItemService) { }

  ngOnInit() {
  }

  onclick() {
    this.itemSvc.addItem(this.curItem);
    this.curItem = new Item();
  }
}


```

item-form.component.html
```html
<input  placeholder="input the content" class = "input"
       [(ngModel)] = "curItem.title"
    #title = 'ngModel' autofocus>
<button class = "is-primary" (click) = "onclick()">add a new task</button>

```


in app.module.ts
```java
import { FormsModule } from '@angular/forms';
  imports: [
    FormsModule
  ]
```

### item detail     

item-detail.component.ts
```java
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Item } from '../shared/item';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css']
})

export class ItemDetailComponent implements OnInit {

  @Input()
  item: Item = null;

  @Output()
  itemDeleteEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  deleteThisItem() {
    this.itemDeleteEvent.emit(this.item);
  }
}
```

in app.module.ts
```java
import { FormsModule } from '@angular/forms';
  imports: [
    FormsModule
  ]
```

item-detail.component.html
```html
<div>
    <label>{{item.title}}</label>
    <button class = "bg-danger" (click) = "deleteThisItem()">DELETE</button>
</div>
  

```


### items list      

items-list.component.ts   
```java
import { Component, OnInit } from '@angular/core';
import { Item } from '../shared/item';
import { ItemService } from '../shared/item.service';
import { ItemDetailComponent} from "../item-detail/item-detail.component";

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.css']
})
export class ItemsListComponent implements OnInit {

  public items: Item[] = null;

  constructor(private itmSvc: ItemService) { }

  ngOnInit() {
    this.items = this.itmSvc.getList();
  }

  deleteItemInList(curItem: Item) {
    this.itmSvc.deleteItem(curItem);
  }

}

```

```html
<div *ngFor = "let item of items">
  <app-item-detail [item] = "item"></app-item-detail>
</div>

```

### delete an item  

**the EventEmitter's name in item-detail(child) is the same as the event's name in items-list(parent)**

in item-detail.component.ts
```java
export class ItemDetailComponent implements OnInit {

  ...
  @Output()
  itemDeleteEvent = new EventEmitter();  
  ...

```

in items-list's html:
```
<div *ngFor = "let item of items">
  <app-item-detail [item] = "item" (itemDeleteEvent) = "deleteItemInList($event)" ></app-item-detail>
</div>  
```

### app

in app.component.html
```html
<h2>todoList</h2>
<app-items-list></app-items-list>
<app-item-form></app-item-form>
```

--=====
ng g 

Available Schematics:

```
class
component
directive
enum
guard
interface
module
pipe
service
application
library
universal
```

--=====debug info   


```
Can't bind to 'item' since it isn't a known property of 'app-item-detail'.
```


