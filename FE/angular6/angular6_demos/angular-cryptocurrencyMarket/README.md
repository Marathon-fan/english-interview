# cryptocurrency market

After editing, click "save all"    
**save all**

## how to run it  

```sh
npm install  
npm start   
```

## environment, class, service and components   

```sh
sudo npm install -g typescript
sudo npm install -g @angular/cli
brew install watchman  # helps Angular CLI when it needs to monitor files in your filesystem for changes


ng new angular-cryptocurrencyMarket
cd angular-cryptocurrencyMarket
ng add ng-zorro-antd   # use ant design angular version

ng g class items/shared/item                          # the basic stock info
ng g service items/shared/item                        # the stock info service      add a stock, delete a stock                                                      
ng g component items/item-detail                      # the component to show a stock                               
ng g component items/items-list                       # the component to show a list of stock   
```


## source    

### item  

```java
export class Item {
    icon: string;
    name: string;
    symbol: string;
    price: number;
    marketCap: number;
    change: number;
}
```

### item service  

item.service.ts
```java
import { Injectable } from '@angular/core';
import { Item } from './item';

@Injectable()
export class ItemService {

  public items: Item[] = [];

  constructor() { }

  getList(): Item[] {
    return this.items;
  }

  addItem(curItem: Item): void {
    this.items.push(curItem);
  }

  deleteItem(curItem: Item): void {
    const Index = this.items.indexOf(curItem);
    this.items.splice(Index, 1);
  }

}
```

in app.module.ts
```java
import { ItemService } from './items/shared/item.service';
  providers: [ItemService],
```

### item detail     

item-detail.component.ts
```java
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Item } from '../shared/item';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css']
})

export class ItemDetailComponent implements OnInit {

  @Input()
  item: Item = null;

  @Output()
  itemDeleteEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  deleteThisItem() {
    this.itemDeleteEvent.emit(this.item);
  }
}
```

in app.module.ts
```java
import { FormsModule } from '@angular/forms';
  imports: [
    FormsModule
  ]
```

item-detail.component.html
```html
<div>
    <label>{{item.icon}}</label>
    <label>{{item.name}}</label>
    <label>{{item.symbol}}</label>
    <label>{{item.price}}</label>
    <label>{{item.marketCap}}</label>
    <label>{{item.change}}</label>
</div>

```


### items list  

items-list.component.ts   
```java
import { Component, OnInit } from '@angular/core';
import { Item } from '../shared/item';
import { ItemService } from '../shared/item.service';
import { ItemDetailComponent} from "../item-detail/item-detail.component";

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.css']
})
export class ItemsListComponent implements OnInit {

  public items: Item[] = null;

  constructor(private itmSvc: ItemService) { }

  ngOnInit() {
    this.items = this.itmSvc.getList();
  }

  deleteItemInList(curItem: Item) {
    this.itmSvc.deleteItem(curItem);
  }

}

```

```html
<div *ngFor = "let item of items">
  <app-item-detail [item] = "item"></app-item-detail>
</div>

```


/////////////////////


todo:
```
1 use angular to show a picture  
2 paging  
3 use chart.js  
4 deploy and CI/CD system    

5 add redis   
6 distributed architecture from scarch!!!    
7 add nginx   
8 use WebSocket   
9 add a file system(like mongoDB's GridFS)    
```


