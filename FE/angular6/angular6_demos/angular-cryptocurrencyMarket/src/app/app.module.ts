import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgZorroAntdModule, NZ_I18N, zh_CN } from 'ng-zorro-antd';
import { registerLocaleData } from '@angular/common';
import zh from '@angular/common/locales/zh';

import { ItemService } from './items/shared/item.service';
import { ItemDetailComponent } from './items/item-detail/item-detail.component';
import { ItemsListComponent } from './items/items-list/items-list.component';


registerLocaleData(zh);

@NgModule({
  declarations: [
    AppComponent,
    ItemDetailComponent,
    ItemsListComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgZorroAntdModule
  ],
  providers: [{ provide: NZ_I18N, useValue: zh_CN }, ItemService],
  bootstrap: [AppComponent]
})
export class AppModule { }

