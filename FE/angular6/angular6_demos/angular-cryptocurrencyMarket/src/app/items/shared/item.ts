export class Item {
    icon: string;
    name: string;
    symbol: string;
    price: number;
    marketCap: number;
    change: number;
}