import { Injectable } from '@angular/core';
import { Item } from './item';

@Injectable()
export class ItemService {

  public items: Item[] = [];

  constructor() { 
    this.items.push({  
      icon: 'BTCIcon',
      name: 'Bitcoin',
      symbol: 'BTC',
      price: 6832.68,
      marketCap: 118.36,
      change: 513.86}
    );

    this.items.push({  
      icon: 'ETHIcon',
      name: 'Ethereum',
      symbol: 'ETH',
      price: 214.44,
      marketCap: 22.00 ,
      change: 13.07}
    );

    this.items.push({  
      icon: 'XRPIcon',
      name: 'Ripple',
      symbol: 'XRP',
      price: 0.4605,
      marketCap: 46.05,
      change: 0.037}
    );

  }

  getList(): Item[] {
    return this.items;
  }

  addItem(curItem: Item): void {
    this.items.push(curItem);
  }

  deleteItem(curItem: Item): void {
    const Index = this.items.indexOf(curItem);
    this.items.splice(Index, 1);
  }

}