
```sh
sudo npm install -g typescript
sudo npm install -g @angular/cli

brew install watchman  # helps Angular CLI when it needs to monitor files in your filesystem for changes

```

```sh
ng new angular-hello-world
cd angular-hello-world
ng serve   # or ng serve --port 9001
```

**The app-root tag is where our application will be rendered.**

## Angular component

**we will teach the browser new tags that have custom functionality attached to them.**


generate the hello-world component
```sh
ng generate component hello-world
```

A basic Component has two parts:
```
1. A Component decorator
2. A component definition class
```

the ng serve command live-compiles our .ts to a .js file automatically.

## decorator  

We can think of decorators as **metadata added to our code.** When we use @Component on the HelloWorld class, we are “decorating” HelloWorld as a Component.

```
@Component({
selector: 'app-hello-world' // ... more here
})
```

with selector we’re defining a new tag that we can use in our markup.


## rendering the template

using two curly brackets {{ }} to display the value of the variable in the template. For example,   
```sh
<p>
Hello {{ name }}
</p>
```

## ngFor and array  
```sh

# in user-list.component.ts
export class UserListComponent implements OnInit { names: string[];
constructor() {
this.names = ['Ari', 'Carlos', 'Felipe', 'Nate'];
}
  ngOnInit() {
  }
}


# in user-list.component.html
<ul>
<li *ngFor="let name of names">Hello {{ name }}</li> 
</ul>

```

## add input for component

**the var property(the var that needs to accept the input) to have an decorator of @Input.** 

**To pass values to a component we use the bracket [] syntax in our template**


```sh

# use @Input() decorator in the component  
...
export class UserItemComponent implements OnInit {
@Input() name: string; // <-- added Input annotation

constructor() {
    // removed setting name
}

...


# in user-list.component.html, use [] to accept value
<ul>
	<li *ngFor="let name of names">
	    <app-user-item [name]="name"></app-user-item> 
	</li>
</ul>
```  

## main entry point  

```sh
• angular.json specifies a "main" file, which in this case is main.ts
• main.ts is the entry-point for our app and it bootstraps our application
• The bootstrap process boots an Angular module – we haven’t talked about modules yet, but
we will in a minute
• WeusetheAppModuletobootstraptheapp.AppModuleisspecifiedinsrc/app/app.module.ts
• AppModule specifies which component to use as the top-level component. In this case it is
     AppComponent
• AppComponent has <app-user-list> tags in the template and this renders our list of users.
```

## NgModule

**NgModules configure the injector and the compiler and help organize related things together.**


code/first-app/angular-hello-world/src/app/app.module.ts
```java
@NgModule({
  declarations: [
    AppComponent,
    HelloWorldComponent,
    UserItemComponent,
    UserListComponent
], 
imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```


**declaration**
```
declarations specifies the components that are defined in this module.  
You have to declare components in a NgModule before you can use them in your templates.   
You can think of an NgModule a bit like a “package” and declarations states what components are
“owned by” this module.
```

**imports**
```
imports describes which dependencies this module has. We’re creating a browser app, so we want to import the BrowserModule.    

The short answer is that you put something in your NgModule’s imports if you’re going to be using it in your templates   

note: the imports here is different from when JS wants to import something using import keyword
```

**providers**
```
providers is used for dependency injection. 
```

**bootstrap**
```
bootstrap tells Angular that when this module is used to bootstrap an app, we need to load the AppComponent component as the top-level component.
```


## Angular modularity    

Modules are a great way to organize an application and extend it with capabilities from external libraries.   

Angular libraries are NgModules, such as FormsModule, HttpClientModule, and RouterModule. Many third-party libraries are available as NgModules such as Material Design, Ionic, and AngularFire2.   



