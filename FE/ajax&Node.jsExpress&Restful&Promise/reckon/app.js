
var express = require('express')
var app = express()
var path    = require("path");
var bodyParser = require('body-parser')

const port = 9999
app.use(bodyParser.urlencoded({ extended: false })) 
app.use(bodyParser.json())

const Question1 = require('./Question1.js');
const Question2 = require('./Question2.js');

let q1 = new Question1();
q1.callEndpoint();

let q2 = new Question2();
q2.callEndpoint();

app.get('/', function (req, res) {
  res.send('Hello reckon')
})

//console.log(q1.data);

app.get('/api/question1', function (req, res) {    
    res.json(q1.data);
  })

app.get('/api/question2', function (req, res) {    
    q2.postData();
    res.json(q2.data);    
  })

// test  
app.post('/api/test', function (req, res) {  
    console.log('in /api/test');  
    console.log(req.body);

});  

app.get('/question1', function (req, res) { 
    res.sendFile(path.join(__dirname+'/web/question1.html'));   
  })

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

