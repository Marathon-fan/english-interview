var Promise = require('promise');

module.exports = class Question2{

    constructor() {
        this.endpoint1 = 'https://join.reckon.com/test2/textToSearch';
        this.endpoint2 = 'https://join.reckon.com/test2/subTexts';
        this.request = require('promise-request-retry');
        this.data = {};

        // https://join.reckon.com/test2/submitResults 

        this.options = {
            method: 'POST',
            uri: 'http://localhost:9999/api/test', 
            headers: {
                'User-Agent': 'Request-Promise'
            },
            json: true, // Automatically parses the JSON string in the response, 
            retry : 5
        };     
    }

    postData() {
        this.request(this.options).then().catch();
    }

    callEndpoint(){
       var theObj = this;

       // call endpoint 1
       this.v1 = this.request({
            "method":"GET", 
            "uri": this.endpoint1,
            "json": true,
            "retry" : 5
          }).then(function(response){
              theObj.text = response.text;
              return response;
          }, 
          console.log)
          .catch(function (err) {
            console.log("tryCount over!");
        });

        // call endpoint 2
        this.v2 = this.request({
            "method":"GET", 
            "uri": this.endpoint2,
            "json": true,
            "retry" : 5

          }).then(function(response){
            theObj.subTexts = response.subTexts;
            return response;
          }, 
          console.log)
          .catch(function (err) {
            console.log("tryCount over!");
        });


        //begin the calculation
          Promise.all([this.v1, this.v2])
          .then(res => {
              var data = {};
              data.candidate = 'John Zhou';
              data.text = theObj.text;
              data.result = [];
              theObj.subTexts.forEach( e => {
                var resultForE = theObj.findTextOneIndexBased(theObj.text, e);
                data.result.push({subtext:e, result:resultForE});
              });
              // 
              theObj.data = data;
              theObj.options.body = theObj.data;    
              theObj.postData();                   
          })
          .catch(err => console.log('error', err))   
    }
    
    findTextOneIndexBased(str1, str2) {
        var res = '';
        const len1 = str1 === null ? 0 : str1.length;
        const len2 = str2 === null ? 0 : str2.length;
        for (let i = 0; i + len2 <= len1; i++) { 
            if (this.exactMatch(str1, i, str2, 0, len2) === true) {
                res += (i + 1).toString() + ', ';    // starting point is 1-indexed
            }        
        }
        if (res === '') {
            return '<No Output>';
        }
        return  res.substring(0, res.length - 2); // delete the last ', '
    }
    
    exactMatch(str1, start1, str2, start2, len) {
        for (let i = 0; i < len; i++) {
            if (str1.charAt(start1 + i).toUpperCase() !== str2.charAt(start2 + i).toUpperCase()) {
                return false;
            }
        }
        return true;
    }
   
};