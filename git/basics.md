
```
Branches are just pointers to commits, no copies involved. Also branches do not contain commits. Any commit that is reachable by a branch is safe. If you didn't understand those statements, I'd recommend you to read this: Think like (a) git.
```
Share
Report
Save


level 2
gumnos
5 points
·
1 year ago
The only value in keeping the branch around after a merge would be

if you plan to continue work on that branch

if you need to refer to that point in time for purposes of diffing, annotating ("at this point we added the new Frobniculation functionality"), or the like. However, this could just as easily be converted into a tag and then let the branch disappear.

Otherwise, if they're fully merged and not going to receive continued work, feel free to delete the branch (possibly creating a tag for that point in time of you want to refer to it)