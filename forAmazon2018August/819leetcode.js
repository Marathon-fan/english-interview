
//Here is a shortened ES6 solution with explanation; It's in O(N) where N is the number of words in the paragraph;

const mostCommonWord = function(paragraph, banned) {
    //Split the paragraph into an array of words in lowercase
    //const words = paragraph.toLowerCase().split(/\W/);
    //const words = paragraph.toLowerCase().split(/\W+/);   
    const words = paragraph.toLowerCase().split(/\W/);    // even if we use split(/\W+/), there will still be a '' in the last position
    console.log(words);
    //Create a map to act as histogram of words
    const mp = Object.create(null);
    //Filter out empty strings and make the histogram
    words.filter(x => x).map(x => mp[x] = x in mp ? mp[x] + 1 : 1);
    //words.map(x => mp[x] = x in mp ? mp[x] + 1 : 1);
    //Rather than deleting banned words, just set its value to a negative number
    //console.log(mp);
    banned.map(x => mp[x] = -1)
    //Return the word with the highest count in the histogram
    return Object.keys(mp).reduce((a, b) => mp[a] > mp[b] ? a : b);
};




// ```js
// const p = "Bob hit a ball, the hit BALL flew far after it was hit.";
// const words1 = p.toLowerCase().split(/\W/);
// console.log(words1);
// ```


const p = ".Bob hit a ball, the hit BALL flew far after it was hit.";
const words1 = p.toLowerCase().split(/\W/);
console.log(words1);
const words2 = p.toLowerCase().split(/\W+/);

console.log(words2)

const s1 = 'AAAwAAAAAwAAAw';
const s2 = 'AAAwAAAAAwwAAAw';
const s3 = 'wAAAwAAAAAwAAAw';
const ss1 = s1.split(/w/);
const ss2 = s2.split(/w/);
const ss3 = s3.split(/w/);

console.log(ss1);
console.log(ss2);
console.log(ss3);

////////////

var mostCommonWord_v2 = function(paragraph, banned) {
    const words = paragraph.toLowerCase().split(/\W+/);
    let mp = {};
    words.filter(w => w).map( w => w in mp ? mp[w] = mp[w] + 1 : mp[w] = 1);
    //  words.filter(w => w).map( w => w in Object.keys(mp) ? mp[w] = mp[w] + 1 : mp[w] = 0);
    //  it's completely differently when we use Object.keys(mp) and mp in the for loop
    //  if use Object.keys(mp), the Object.keys(mp) is only calculated once at the inception of the loop
    //  if use mp, every time the loop uses the current value of mp
    console.log(mp);
    banned.map(w => mp[w] = -1);
    return Object.keys(mp).reduce((a, b) => mp[a] > mp[b] ? a : b);
}

mostCommonWord_v2(p, ['hit']);
// it's completely 

//Do you know why there are always some '' when encountering punctuations in string p?


// words1  [ 'bob',
//   'hit',
//   'a',
//   'ball',
//   '',
//   'the',
//   'hit',
//   'ball',
//   'flew',
//   'far',
//   'after',
//   'it',
//   'was',
//   'hit',
//   '' ]


// console.log('words1 ', words1);

// const banned = ["hit"];
// console.log(mostCommonWord(p, banned));


