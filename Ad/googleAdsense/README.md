
# README

# introduction     

**blogging.org**


## What is Google Adsense ?

Small ads on your site, served by Google that make you money

## What do I need to get started ?

```
A domain, site, good content and some traffic
```

How much money can I make with Google Adsense?
```
Most sites make a few dollars per day, but there is no limit(like thousands of dollars a day).
```

How do you get paid from Google Adsense?
```
Checks, Electronic Funds Transfer and Western Union Quick Cash
```

## How do I apply for Google Adsense ?

```
Visit Easy - google.com/adsense    

sign up    
```


# Most profitable Google Keywords & Niches   

http://blogging.org/resources


## top key words
http://blogging.org/blog/most-profitable-keywords

```
insurance         $ 54.91 per click
loans             $ 44
mortgage          $ 42
attorney          $ 47
credit            $ 36
lawyer            $ 42.5
donate            $ 42.02
degree            $ 40.61  
hosting           $ 31.9
claim             $ 45.5

```

97% google revenue is from Ads   


```
wikiHow


```

# the $ 132,994.97 Adsense Check   

Where did all your traffic come from?
```
About 70% of the traffic was direct 15% from search engines and 15% from referrals
```


How much traffic did the site get to get that much money?(132k US $ per month)
```
About 75k uniques per day
```

# Google Adsense Best practices   

## bigger Ad units are better   

```
728*90
300*250
```

## how to position Ads on your site

```
Blend your ads and site to look the same    
Know Premium Locations on Your Site
Perform the Necessary Keyword Research - write about topcis about will trigger high paying advertisers
Use Google Search for Adsense - setup for google results or your own site results    
Expand Outside of Just Web Site - Content, Search, Video, Mobile and Games      
Follow the rules -- Don't give Google an easy reason to terminate your account    
Test, Split Test... Repeat -- Change colors, ads sizes, locations and setup tracking tags
```

# Google Adsense Alternatives   

```
media.net         - powered by Yahoo & Bing  
infolinks         - contextual advertising within your content      
affiliate-program.amazon.com        - earn a commission on referred sales
buysellads.com   - sell banner ad placement on your site
clicksor.com     - contextual and banner based advertising    
qadabra.com      - banner based advertising     
linkvehicle.com  - sponsored blog content    
```
