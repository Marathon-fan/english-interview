


```
Kinesis  ~= managed Kafka 
```


1 partition key
```
assigned to the data record by the data producer
used for partitioning of data across shards
md5 hash determines shards
```

2 sequence Number
```
unique identifier of a data record
assigned by kinesis on write
```

3 shard
```
a shard is a group of data records in a stream
a stream is composed of multiple shards
you scale Kinesis streams by adding or removing shards
each shard provides a fixed unit of capacity
each shard ingests up to 1 MB/sec of data up to 1000 records/sec
```


