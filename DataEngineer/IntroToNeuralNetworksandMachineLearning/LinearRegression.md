
IntroToNeuralNetworksandMachineLearning

https://www.cs.toronto.edu/~rgrosse/courses/csc321_2018/


## Example of recurring themes throughout the course:    

```sh
choose an architecture and a loss function
forulate an optimization problem
solve the optimization problem using one of two strategies:
    direct solution (set derivatives to zero)
    gradient descent
vectorize the algorithm, i.e. represent in terms of linear algebra
make a linear model more powerful using features
understand how well the model generalizes
```

## linear regression  

Task: predict scalar-valued targets, e.g. stock prices (hence "regression")   

Architecture: linear function of the inputs (hence "linear")    

Model: y is a linear function of x:
    y = wx + b
```
y is the prediction
w is the weight
b is the bias
```    

Loss function: squared error
```
L(y, t) = (1 / 2) (y - t)(y - t)

(y - t) is the residual, and we want to make this small in magnitude
the (1 / 2)  factor is just to make the calculations convenient
```

cost function: loss function averaged over all training examples

![cost function](pics/2-costFunction.jpg)


**multivariable regression**   

![multivariable Regression](pics/2-multivariableRegression.jpg)


**Vectorization**   

For-loops in Python are slow, so we vectorize algorithms by expressing them in terms of vectors and matrices

Why vectorize
```
The equations, and the code, will be simpler and more readable. Gets rid of dummy variables/indices

Vectorized code is much faster
    Cut down on Python interpreter overhead
    Use highly optimized linear algebra libraries
    Matrix multiplication is very fast on A graphics Processing Unit(GPU)
```


Why gradient descent, if we can find the optimum directly? 
```java
GD can be applied to a much broader set of models
GD can be easier to implement than direct solutions, especially with automatic differentiation software
For regression in high-dimensional spaces, GD is more efficient than direct solution (matrix inversion is an O(D3) algorithm).
```


![polynomialRegression](pics/2-polynomialRegression.jpg)


![featureMapping](pics/2-featureMapping.jpg)


**Underfitting**: The model is too simple -- does not fit the data    

![underFitting](pics/2-underFitting.jpg)



**Overfitting**: The model is too complex -- fits perfectly, does not generalize  

![Overfitting](pics/2-Overfitting.jpg)


Generalization
```
We would like our models to generalize to data they haven't seen before
The degree of the polynomial is an example of a hyperparameter, something we can't include in the training procedure itself
We can tune hyperparameters using a validation set
```


