
IntroToNeuralNetworksandMachineLearning

https://www.cs.toronto.edu/~rgrosse/courses/csc321_2018/


## linear classification      


## binary classification   

**The goal is to predict a binary-valued target**     

examples:

```
You want to train a medical diagnosis system to predict whether a patient has a given disease. You have a training set consisting of a set of patients, a set of features for those individuals (e.g. presence or absence of various symptoms), and a label saying whether or not the patient had the disease.

You are running an e-mail service, and want to determine whether a given e-mail is spam. You have a large collection of e-mails which have been hand-labeled as spam or non-spam.

You are running an online payment service, and want to determine whether or not a given transaction is fraudulent. You have a labeled training dataset of fraudulent and non-fraudulent transactions; features might include 
the type of transaction, 
the amount of money, or 
the time of day.
```

Weight space
```
Weight space, where each set of classification weights corresponds to a vector. Each training case corresponds to a constraint in this space, where some regions of weight space are “good” (classify it correctly) and some regions are “bad” (classify it incorrectly).
```



