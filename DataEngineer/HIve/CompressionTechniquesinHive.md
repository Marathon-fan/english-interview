

Files can be compressed in any phase of Job

`Compressing Input file`
`Compressing Map Output`
`Compressing Reducer Output`


| Compression format  | Tool  | Algorithm | File extention  | Splittable |
|---| ---| ---| ---| ---| 
| Gzip  | gzip | Default | .gz | No |
| bzip2  | bzip2 | bzip2 | .bz2 | Yes |
| LZO  | lzop | LZO | .lzo | Yes if indexed |
| Snappy  | N/A | Snappy | .snappy | No |


auto compress map output file
```sh
# set map output
hive> set mapred.compress.map.output;
hive> set mapred.compress.map.output = true;

hive> set mapred.compress.map.output.compression.codec=snappy;
hive> set mapred.compress.map.output.compression.codec=gzip;

#
hive> set mapred.output.compress = true;
hive> set mapred.output.compression.codec;
hive> set mapred.output.compression.codec = lzo;;


```