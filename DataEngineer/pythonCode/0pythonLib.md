
## isinstance()   
```
Definition and Usage
The isinstance() function returns True if the specified object is of the specified type, otherwise False.
```

Syntax
isinstance(object, type)

```python
# Check if "Hello" is one of the types described in the type parameter:

x = isinstance("Hello", (float, int, str, list, dict, tuple))
```


```python
# Check if y is an instance of myObj:

class myObj:
  name = "John"

y = myObj()

x = isinstance(y, myObj)
```

## rstrip()  
```
The method rstrip() returns a copy of the string in which all chars have been stripped from the end of the string (default whitespace characters).
```

Following is the syntax for rstrip() method −
```
str.rstrip([chars])
```

```python
#!/usr/bin/python

str = "     this is string example....wow!!!     ";
print str.rstrip()
str = "88888888this is string example....wow!!!8888888";
print str.rstrip('8')
```
When we run above program, it produces following result −
```
this is string example....wow!!!
88888888this is string example....wow!!!
```



## readlines() 

```

The method readlines() reads until EOF using readline() and returns a list containing the lines. If the optional sizehint argument is present, instead of reading up to EOF, whole lines totalling approximately sizehint bytes (possibly after rounding up to an internal buffer size) are read.

An empty string is returned only when EOF is encountered immediately.
```

program
```python
#!/usr/bin/python

# Open a file
fo = open("foo.txt", "r")
print "Name of the file: ", fo.name

# Assuming file has following 5 lines
# This is 1st line
# This is 2nd line
# This is 3rd line
# This is 4th line
# This is 5th line

line = fo.readlines()
print "Read Line: %s" % (line)

line = fo.readlines(2)
print "Read Line: %s" % (line)

# Close opend file
fo.close()

```

When we run above program, it produces following result −
```
Name of the file:  foo.txt
Read Line: ['This is 1st line\n', 'This is 2nd line\n', 'This is 3rd line\n', 'This is 4th line\n', 'This is 5th line']
Read Line: []

```




## iteritems() and items()   


In Python 2.x, both methods are available, but in Python 3.x iteritems() is deprecated.

As far as Python 2.x is concerned, items() method of dictionary object returns list of two element tuples, each tuple containing key and value. On the other hand iteritems() is a generator which provides an iterator for items in a dictionary


python2
```python
>>> d = {'1': 1, '2': 2, '3': 3}
>>> d.items() 
[(1, 1), (2, 2), (3, 3)]
>>> for i in d.iteritems(): 
   print i 

('1', 1)
('2', 2)
('3', 3)
```

In Python 3, items() method behaves like iteritems() in Python 2
```python
>>> d={'1': 1, '2': 2, '3': 3}
>>> d1.items()
dict_items([('1', 1), ('2', 2), ('3', 3)])
>>> d.items()
dict_items([('1', 1), ('2', 2), ('3', 3)])
>>> for i in d.items():
   print (i)

('1', 1)
('2', 2)
('3', 3)
```




## sorted()   
  
The sorted() function returns a sorted list of the specified iterable object.

You can specify ascending or descending order. Strings are sorted alphabetically, and numbers are sorted numerically.

Note: You cannot sort a list that contains BOTH string values AND numeric values.

Syntax
```python

sorted(iterable, key=key, reverse=reverse)
```

```python
a = (1, 11, 2)
x = sorted(a)
print(x)

###############

# Sort ascending:

a = ("a", "b", "c", "d", "e", "f", "g", "h")
x = sorted(a, reverse=True)
print(x)
```

