

# This is Python 2

# the function will determine if an array is smooth  
def isSmooth(array):
  if (len(array) <= 0):
    return True
  cur = int(array[0]);
  for i in range(1, len(array)):
    next = int(array[i])

    if (abs(cur - next) > 1):
      return False
    cur = next
  return True    

#################################### 
import sys
N = sys.stdin.readline()
numberLine = sys.stdin.readline()
arr = numberLine.split(' ');
  
if (isSmooth(arr) == True):
    print "YES"
else:
    print "NO"

  
