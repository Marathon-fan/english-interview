
# Example of metric definition
cnt_telstra_network_download_success = metrics_v2.NftfMetric(
	metric_name = "connectivity_telstra_network_download_cnt_success",
	output_unit = units.Units.count,
	description = "Indication of whether or not the application on the user device was able to successfully connect to the Internet",
	aggregation_logic = F.coalesce (
		  F.sum(F.col("connectivity_telstra_network_download_cnt_success").cast("int")),
		  F.lit(0)
		),
	version = 1
)   
