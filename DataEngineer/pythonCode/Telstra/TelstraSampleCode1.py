
# Example of measurement difinition

standardized_rat = metrics_v2.Measurement(
	identifier = 1,
	version = 1,
	measure_name = "radio_access_technology",
	output_unit = units.Units.string,
	description = "standardization of the radio access type in the sandvine data",
	input_column_mapping = [
	    ("rat", F.col("rat")),
	],
	measurement_calculation_logic = (
		F.when(F.col("rat").isin({"2G", "2"}), F.lit("2G"))
		 .when(F.col("rat").isin({"3G", "3"}), F.lit("3G"))
		 .when(F.col("rat").isin({"4G", "4"}), F.lit("4G"))
		 .otherwise(F.lit(None))
	)
)

