
https://hadoop.apache.org/docs/r0.23.11/hadoop-project-dist/hadoop-common/CommandsManual.html

```sh
# show all commands
hadoop fs
```


```sh
# show all commands
hadoop fs -ls <dir>
hadoop fs -ls /
hadoop fs -ls /user

```


```sh
# show all commands
hadoop fs -cd /user
hadoop fs -mkdir sample
hadoop fs -ls 
```


```sh
# show all commands
hadoop fs -put sample_file sample
hadoop fs -ls /user/sample

hadoop fs -copyFromLocal sample_file /user/sample
hadoop fs -ls /user/sample
```


```sh
# show all commands
hadoop fs -cat sample_file | head 50
```


```sh
# show all commands
hadoop fs -get sample_file <localDest>
hadoop fs -get sample_file /tmp/sample

```


```sh
# set seperation factor
hadoop fs -setrep 1 sample_file
hadoop fs -setrep 10 sample_file
hadoop fsck /user/sample_file -files -blocks -locations

```

# Accessing HDFS through Java program  

```sh
# show all commands
hadoop fs 
```


```sh
# show all commands
hadoop fs 
```



