



## introduction to Redshift   

What is Redshift
```java
Amazon fast and fully managed, petabyte-scale data warehouse service in the cloud  

Announced in 2012

Designed for online analytic processing (OLAP) and Business Intelligence (BI). DO NOT use for online transaction processing (OLTP)

ANSI SQL compatible

Column-oriented

```  

```java
provision multi-terabyte clusters in minutes at a lower cost

Stroing and managing large amounts of data is far easier

Data will always need to be stored and managed

Create powerful solutions by using Redshift with other AWS services

Data is a hot skill in the market - 

```

## Redshift Architecture

```java
the leader node
    SQL endpoint
    Coordinates paralled query execution
    Stores metadata

compute node     
    Multiple ndoes execute queries in paralled
    Dedicated CPU, memory and local storage
    Scale out/in, up/down  
    backups down in paralled
    Slices:
      Portition of memory and disk
      Data loaded in paralled
      Number of slices depend on the node

5439 default port to Redshift 



```

![RedshiftCluster](./pics/c4-RedshiftCluster.jpg)   
RedshiftCluster   

**a Redshift Cluster is a single-availability zone concept.**    
```java
1 for performance reaons, the leader node and the compute node should be in close proximity and they require high speed network communication  
2 a Redshift Cluster(just like an EMR cluster), should be in a single-availability zone(though the latency between availability is not a issue) 
```
   
MPP Database -- Massively Paralled Processing Database
```java
Data stored on multiple ndoes
Dedicated CPU, memory and local storage
"Shared nothing" architecture
```

## Redshift in the AWS Ecosystem   

S3   
```java
copy thermostat_logs from 's3://redshiftdemosk/xxx.csv' credentials 'aws_iam_role=arn:aws:iam::account_id:role/xx'

```
   
copy command can be used under S3/DynamoDB/EMR/EC2 Instance/Data Pipeline  

AWS Lambda(like dropping files in S3 and automatically loaded into Redshift using Lambda)  

Amazon QuickSight 

AWS Database Migration Service

AWS Database Migration Service

Amazon Kinesis

Amazon Machine Learning(write the data to S3 from Redshift, then use Amazon ML to process the data)   



## Columnar Databases

Redshift is a Columnar Database: stores data tables as columns rather than as rows      


Benefits


```Java
Queries on few columns

Data aggregation

Compression

Lower Total Cost of ownership
```

Columnar Databases are not for:
```
Needle in haystack query
small amount of data
binary large objects
OLTP(Online Transaction Processing)
```



## Redshift Table Design - Introduction

Redshift architecture and its relationship to table design     

```java
Know your data
What aqre your data relationships
Think differently if you are new to column-oriented databases

```

a different approach to design
```java
Designing will be different than typical row-based RDBMS...
    Linking tables/Referential Integrity
    Indexes

```

In Redshift, use sort key(similar to index in row-oriendted DB)

Data Model
```java
The star schema is the simplest style of data mart schema and is the approach most widely used to develop data warehouses and dimensional data marts.
The start schema consists of one or more fact tables referencing any number of dimension tables


```

![star schema](./pics/c4-starSchema.jpg)    
Star Schema


What is A Star Schema?
```java
The simplest form of database structure used in a data warehouse
Answers the basic questions:
    what happend, who did it, when did they do it...etc
Focuses on one, single business area    

What advantages does a Star Schema offer?
    Separates data into two main categories
        Facts
        Dimensions(Measurements or descriptive informaiton)

What is a fact?
   Fact(what happened)
       product sold 
       customer who bought
       etc.
   dimension (attributes that describe what happend)
      when the product was sold
           day, date, year, quarter, day of the week, etc.
      Where the product was sold                
```


## Redshift Table Design - Distribution Styles, Sort Keys, Compression, Constraints, Column Sizing, Data Types   

Three distribution styles
```java
Even distribution
    Rows distributed across the slices regardless of values in a paritcular column(in a round-robin fashion)
    default distribution style

Key distribution
    distribute data evenly among slices
    collocate matching rows on the same slice
    used when we want to join large tables (by this close values are in the same nodes(or close nodes) thus greatly avoiding cross nodes traffic)
    // with distribution key, the difference when joining tables could be pretty significant

ALL distribution    
    a copy of the entire table is in each node thus needing more disk space

```   

when to choose which distribution styles
```java
Even distribution
    No joins, reduced parallelism, where KEY and ALL are not a clear choice
    Rows distributed across the slices regardless of values in a particular column
    Default distribution style

Key distribution
     talbes used in joins
     large fact tables in a star schema
     distribute data evenly among slices
     collocate matching rows on the same slice

ALL distribution    
    Data that does not change
    Reasonably sized tables (a few million rows)
    No common distribution key
```   



![even distribution](./pics/c4-evenDistribution.jpg)      
even distribution    

![key distribution](./pics/c4-keyDistribution.jpg)      
key distribution    

![all distribution](./pics/c4-allDistribution.jpg)      
all distribution    


DDL    
DDL (Data Definition Language) is a language used by a database management system (like Oracle) that allows users to define the database and specify data types, structures and constraints on the data. Examples DDL statements are: CREATE TABLE, CREATE INDEX, ALTER, and DROP.Sep 6, 2018

```java
// key distribution
CREATE TABLE public.lineitem
(
  l_orderkey BIGINT NOT NULL ENCODE mostly32 DISTKEY,
  ...

)
SORTKEY
(
  l_shipdate
);


// even distribution
CREATE TABLE public.nation
(
  n_nationkey INTEGER NOT NULL,
  ....
)
DISTSTYLE EVEN;


// ALL distribution
CREATE TABLE public.nationALL
(
  n_nationkey INTEGER ENCODE lzo,
  ....
)
DISTSTYLE ALL;

```


distribution styles
```java
    
1 use the guidelines, but also experiment and test

2 AWS Big Data Blog: Amazon Redshift Engineering Advanced Table Design Playbook

```

For the exam
```java
know the distribution styles
know when you would use them
know their benefits and drawbacks
```


## Redshift    x   
placeholder   



## Redshift    x   
placeholder   


## Machine Learning - Introduction     
placeholder


## Machine Learning - Multiclass Classification Model   
placeholder


## Machine Learning - Binary Classification Model with Amazon Machine Learning   

examples(get a Yes/No outcome)
```java
Is an article retrieved from a search relevant
Will a customer buy a product
Will a home owner default on a loan
Is the transaction fraudulent
```




## Machine Learning - Regression Model   

placeholder   


## Machine Learning Lab   

placeholder   





## Elasticsearch   x   
!!! placeholder




