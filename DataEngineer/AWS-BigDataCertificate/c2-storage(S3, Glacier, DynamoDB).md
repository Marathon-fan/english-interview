

# Glacier and Big Data

## Amazon S3 Glacier   

**Amazon S3 Glacier**
Amazon S3 Glacier is an extremely low-cost storage service that provides secure, durable, and flexible storage for data backup and archival.

AWS Big Data portfoilio
```
keep all your data at amuch lower cost
compliance requirement to keep your data

```

**Glacier**   
Glacier is part of the Amazon Web Services suite of cloud computing services, and is designed for long-term storage of data that is infrequently accessed and for which retrieval latency times of 3 to 5 hours are acceptable. Storage costs are a consistent $0.004 per gigabyte per month, which is substantially cheaper than Amazon's own Simple Storage Service (S3).

**In Amazon S3 Glacier, first we need to create Vault**    
A vault is a container for storing archives. An archive is any object, such as a photo, video, or document, which you store in a vault.

Compliance
```
Vault Lock
   Deploy and enforce controls for a vault with a vault lock policy
   Policies
       Locked from editing
       Policies cannot be changed after locking
       Enforce compliance controls
       Created using IAM
   Vault Lock
       Initiate Vault Lock
           Attaches a vault lock policy to your vault
           The lock is set to an InProgress state and a lock ID is returned
           24 hours to validate the lock(expires after 24 hours)
       Complete Vault Lock
           InProgress state to the  Locked state(immutable)        
```

**vault lock policy**

For the exam:

```
Vault Lock controls -  time-based retention, "undeleteable" or both
Implement control using IAM policies
```

## DynamoDB   

### DynamoDB Introduction   

DynamoDB: **A fully managed, NoSQL Database service**

```
A fully managed, NoSQL Database service
Predictable fully managedable performance, with seamless scalability
No visible servers
No practical storage limitations
SSD   
Fully resilient and highly available
Performance scales - in a linear way
Fully integrated with IAM - rich and controllable security

```

**In DynamoDB, when you write data, you are writing to multiple datacenters at a time, Data is replicated across three geographically distinct datacenters at a time**

```
a collection of Tables
Tables are the highest level structure within a Database
Its on tables that you specify the performance requirements
Write Capacity Units(WCU) - Number of 1KB blocks per second
Read Capacity Unit(RCU) - Number of 4KB blocks per second
Eventually Consistent Reads(Default)
Strongly Consistent Reads(If we don't use strongly consistent reads, then it may lead to reading older version of data(in a short amount of time) but cheaper )
```

DynamoDB uses the performance and the data quality to manage underlying resource provisioning

Unlike SQL databases, the data structure or Schema is NOT defined at the table level


**Partition Key(= Hash Key)**   

Partition key: A simple primary key, composed of one attribute known as the partition key. Attributes in DynamoDB are similar in many ways to fields or columns in other database systems.


**Sort Key(= Range Key):**    

In a DynamoDB table, the primary key that uniquely identifies each item in the table can be composed not only of a partition key, but also of a sort key.

Well-designed sort keys have two key benefits:

They gather related information together in one place where it can be queried efficiently. Careful design of the sort key lets you retrieve commonly needed groups of related items using range queries with operators such as starts-with, between, >, <, and so on.

Composite sort keys let you define hierarchical (one-to-many) relationships in your data that you can query at any level of the hierarchy.

Attribute Types   
```sh
String
Number
Binary
Boolean
Null
Document(List/Map)
Set 
```

DynamoDB in the AWS Ecosystem

```
Redshift   DynamoDB    EMR(including HIVE)    S3    Data Pipeline     EC2 Instances    Lambda      Kinesis
```

```java
Using the COPY command in Redshift, you can copy data directly from DynamoDB to Redshift

On EMR, DynamoDB is integrated with Apache Hive(a data warehouse application built on top of hadoop)
By using Hive on EMR, you can read and write data in Dynamo tables. This allows you query live DynamoDB data using SQL-like language. 
You can copy data from a DynamoDB table into HDFS(Hadoop distributed file system) and vice versa    

You can perform join operations on DynamoDB tables

You can copy data from a DynamoDB table to a bucket in S3 and vice versa
You can export DynamoDB data to S3 and you can import data from S3 intto DynamoDB by using Data Pipeline.(When you want to export data from DynamoDB, Data Pipelien launches an EMR cluster, EMR reads from the DynamoDB table, and writes your data to the S3 bucket. If you want to import data from S3 into DynamoDB, again Data Pipeline launches an EMR cluster, reads from S3 buckets, and writes to DynamoDB tables) 

Using EC2 Instance and DynamoDB API, you can create applications that write to and read from DynamoDB

DynamoDB is also integrated with Lambda. You can create triggers that automatically respond to events in DynamoDB streams. With triggers, you can build applicaition that reacts to data modifications in DynamoDB tables.  

Kinesis Streams
Using Kinesis Streams cloud library and the Kinesis connector library, consumer applications can consume data from Kinesis streams and emit that data to DynamoDB 


```

### DynamoDB Partitions    

**partitions**  

**Partitions are underlying storage and processing nodes of DynamoDB.**  

DynamoDB stores data in partitions. A partition is an allocation of storage for a table, backed by solid-state drives (SSDs) and automatically replicated across multiple Availability Zones within an AWS Region. Partition management is handled entirely by DynamoDB—you never have to manage partitions yourself.


```
A partition can handle 3000 RCU and 1000 WCU
There is a capacity and performance relationship to the number of partitions!!!
Design tables and applications to avoid I/O "hot spots"/"hot keys"
When > 10 GB or > 3000 RCU OR > 1000 WCU required, a new partition is added and the data is spread between them over time   
```

how is the data distributed?
```
Based on its Partition key(HASH)
```

```
Partitions will automatically increase
while there is an automatic split of data scross partitons, there is not automatic decrease when load/performance reduces
allocated WCU and RCU is split between partitions
Each partition key is limited to 10GB data
                       limited to 3000 RCU 1000 WCU

Be careful increasing and decreasing WCU/RCU
```

key concepts
```
be aware of the underlying storage infrastructure - partitions
be ware of what influences the number of partitions
capacity
Performance (WCU/RCU)
be aware that they increase, but they don't decrease
be aware that table performance is split across the partitions - what you apply at a table level isn't what you often get
true performance is based on performance allocated, key structure, and time and key distribution of reads and writes
```

**For the exam**

```
the key concepts on partitions 
RCU and WRU impact performance
how partition key choices impact performance
sort key selection  

```

### DynamoDB GSI/LSI(Global Secondary Index (GSI) and Local Secondary Index (LSI))    

introduction

Dynamo DB offers two main data retrieval operations, SCAN and QUERY

   Without indexes, your ability to retrieve informationo is limited to your primary table structure
   Indexes allow secondary representations of the data in a table, allowing efficient queries on those representations
   Indexes come in two forms: Global Secondary and Local Secondary

Local Secondary Indexes


### DynamoDB Streams & Replication   

**DynamoDB Stream** is an ordered record of updates to a DynamoDB table

When a stream is enabled on a table, it records changes to a table and stores those values for 24 hours

A stream can be enabled on a table from the console or API, but can only be read or processed via the streams endpoint and API requests

DynamoDB stream endpoint is like:(different from DynamoDB endpoints)
```
streams.dynamodb.us-west-2.amazonaws.com
```

AWS guarantee that each change 

Changes recorded in streams have a "limitation"
Streams have a number of configurations to overcome this "limitation"
Streams can be configured with four 'views'
    KES_ONLY       only the key attributes are written to the stream
    NEW_IMAGE      the entire item POST update is written to the stream
    OLD_IMAGE      the entire item PRE the update is written to the stream
    NEW_AND_OLD_IMAGE the pre and post operation state of the item is written to the stream allowing more complex comparison operations to be performed

Streams & Replication   
Use cases  
```
Replication
Triggers
DR() - a database table in one AWS region, replicating to another AWS region for DR failover

A lambda function triggered when items are added to a DynamoDB stream, performing analytics on data

A lambda function triggered when a new user signup happens on your web app and data is entered into a users table

Large distributed applications with users worldwide - using a multimaster database model. A synced set of tables operating worldwide

```


**Replication**

Kinesis is coupled with EC2 instance, but Lambda is not

```
DynamoDB   -------DynamoDB Streams------>   Lambda   ------>  writes  ------>   DynamoDB

```

**For the exam**
```
the capability and use-cases of streams 
Streams + Lambda allow traditional DB style triggers 
four types of stream views: KEYS_ONLY, NEW, OLD_IMAGE and NEW_AND_OLD_IMAGE
Cross-region replication
```


### DynamoDB Performance Deep Dive 1 

```
partitons

two formulas

formaula 1(performance)
Performance:       partitions = (desired RCU / 3000 RCU) + (Desired WCU / 1000 WCU)
for example:       partitions = (7500 RCU / 3000 RCU) + (3000 WCU / 1000 WCU) = 2.5 + 3 = 5.5 = 6


formula 2(size)
size                partitions = Data Size in GB / 10 GB
for example:        partitions = 65 GB / 10 GB = 6.5 = 7


the actual number is the MAX of the performance and size partitions. In our case MAX(6, 7) = 7
```

Our allocated reads and writes are distributed across partitions    


Key selection   

What makes a good key?(the following criteria will assist DynamoDB with scaling performance)   
```
the attribute should have many distinct values
the attribute should have a uniform wirte pattern across all partition key values
the attribute should hava a uniform temporal write pattern acorss time
if any of the above aren't possible with a existing value - you should consider a synthetic/created/hybrid vlaue
You shouldn't mix HOT and COLD key values within a table
```


### DynamoDB Performance Deep Dive 2   

Global Secondary Indexes - Performance Considerations   
   GSI's have their own RCU and WCU values and use alternative keys


```
partitions - what they are, how they work, how to calculate their number

how partitons influence performance together with partition keys

best practice for partition keys, and how to structure data based on key-space and load

global indexes and how to select keys, and how this selection impacts the index and the table

```



## Quiz

```
The Kinesis Connector Library allows you to emit data from a stream to various AWS services. Which of the following services can receive data emitted from such a stream? (Choose 4)


Lambda
RDS
S3
DynamoDB
Elasticsearch
Redshift


Answer:
S3, DynamoDB, Elasticsearch, Redshift
```


```
True or False: Kinesis streams are appropriate for persistent storage of your streaming data.

Answer:
False

Kinesis stream data is, by default, only stored for 24 hours. However, this timeframe can be extended to 7 days.

What Is Kinesis ?
If you are used to Apache Kafka, Kinesis is a cloud-based managed alternative to Kafka.
In other words, Kinesis is a system used for building real-time data pipelines and streaming apps and storing the same data to AWS Redshift or S3.
```

```
You have an application based on the Amazon Kinesis Streams API, and you are not using the Kinesis Produce Library as part of your application. While you won't be taking advantage of all the benefits of the KPL in your application, you still need to ensure that you add data to a stream efficiently. Which API operation allows you to do this?

PutItem
PutRecord
PutItems
PutRecords

Answer:
PutRecords
The PutRecords operation writes multiple data records into an Amazon Kinesis stream in a single call. Use this operation to send data into the stream for data ingestion and processing.
```

```
True or False: With both local secondary indexes and global secondary indexes, you can define read capacity units and write capacity units on the index itself — so that you don't have to consume them from the base table.

Answer:
False

A global secondary index has its own provisioned throughput settings for read and write activity. Queries or scans on a local secondary index consume read capacity units from the base table. When you write to a table, its local secondary indexes are also updated; these updates consume write capacity units from the base table. Further information: http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/SecondaryIndexes.html
```


```
In terms of data write-rate for data input, what is the capacity of a shard in a Kinesis stream?

6 MB/s
4 MB/s
1 MB/s
2 MB/s

Answer:
1 MB/s
Each shard in a Kinesis stream can support a maximum total data write rate of 1 MB per second.
One shard provides a capacity of 1MB/sec data input and 2MB/sec data output.
```


```
True or False: You can add a local secondary index to a DynamoDB table after it has been created.

Answer:
False
You cannot add a local secondary index to a DynamoDB table after it has been created. Secondary indexes must be created at the time of table creation.

```

```
Which of the following attribute data types can be table or item keys? (Choose 3)

Binary
Map
Blob
Number
String

Answer:
String, Number, and Binary data types (scalars) can be table or item keys. Further information: http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/HowItWorks.NamingRulesDataTypes.html

```

```
Which of the following must be defined when you create a table? (Choose 4)

The Table Name
The WCU (Write Capacity Units)
The table capacity, number of GB.
The DCU (Delete/Update Capacity Units)
Partition Key
The RCU (Read Capacity Units)

Answer:
The Table Name
Partition Key
The WCU (Write Capacity Units)
The RCU (Read Capacity Units)

The table name, RCUs, WCUs, and table key(Partition Key) are required when creating a table.


```


```java
Your company has a number of consumer applications to get records from various Kinesis Streams for different use cases. For each consumer application, there is a separate DynamoDB table that maintains application state. Out of the many consumer applications, one application is experiencing provisioned throughput exception errors with it's particular DynamoDB table. Why is this happening? (Choose 2)

The application is not checkpointing enough.
The application is checkpointing too frequently.
The stream does not have enough shards.
The stream has too many shards.

Answer:
The stream has too many shards.
The application is checkpointing too frequently.

If your Amazon Kinesis Streams application receives provisioned-throughput exceptions, you should increase the provisioned throughput for the DynamoDB table. The KCL(Kinesis Client Library) creates the table with a provisioned throughput of 10 reads per second and 10 writes per second, but this might not be sufficient for your application. For example, if your Amazon Kinesis Streams application does frequent checkpointing or operates on a stream that is composed of many shards, you might need more throughput. Further information: http://docs.aws.amazon.com/streams/latest/dev/kinesis-record-processor-

A partition key is used to group data by shard within a stream. Kinesis Data Streams segregates the data records belonging to a stream into multiple shards. It uses the partition key that is associated with each data record to determine which shard a given data record belongs to.


The term checkpointing means recording the point in the stream up to the data records that have been consumed and processed thus far, so that if the application crashes, the stream is read from that point and not from the beginning of the stream. The subject of checkpointing and the various design patterns and best practices for it are outside the scope of this chapter. However, it is something you may encounter in production environments.

```

```
What are the max deliverables from one Dynamo DB Partition.

4,000 WCU, 1,000RCU, 10GB Data volume
3,000 WCU, 1,000RCU, 10GB Data volume
No maximums
1,000 WCU, 3,000RCU, 10GB Data volume

Answer:
1,000 WCU, 3,000RCU, 10GB Data volume
DynamoDB is capable of delivering 1,000 WCU, 3,000 RCU and 10GB of data from a single partition -- any more causes additional partitions to be created and data split between them. Further information: http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GuidelinesForTables.html#GuidelinesForTables.Partitions

```

```
In terms of data read-rate for data output, what is the capacity of a shard in a Kinesis stream?

4 MB/s
2 MB/s
1 MB/s
6 MB/s

Answer:
2 MB/s

Each shard in a Kinesis stream can support a maximum total data read rate of 2 MB per second.
```


```
Which of the following statements is true?

A shard supports up to 1000 transactions per second for reads, and 5 transactions per second for writes.
A shard supports up to 5 transactions per second for reads, and 100 records per second for writes.
A shard supports up to 5 transactions per second for reads, and 1000 records per second for writes.
A shard supports up to 5 transactions per second for reads, and 10 records per second for writes.

Answer:    
Each shard can support up to 5 transactions per second for reads, and up to 1,000 records per second for writes.

Kinesis enables you to ingest granular data into a stream and read batches of records to process the information. So the volume of megabytes you can read per second is much more important than the number of read transactions you get per shard. For example, you might have a busy website generating thousand of views per minute and an EMR cluster to process your access logs. In this scenario, you will have much more write events than read events. The same is valid for clickstreams, financial transactions, social media feeds, IT logs, and location-tracking events, etc.
```

```
Which operation/feature or service would you use to locate all items in a table with a particular sort key value? (Choose 2)

Scan against a table, with filters
Query with a global secondary index
Query
GetItem
Query with a local secondary index

Answer:    
Scan against a table, with filters
Query with a global secondary index

Local secondary indexes can't be used: they only allow an alternative sort key, and query can only work against 1 partition key, with a single or range of sort. Global secondary indexes will allow a new index with the sort key as a partition key, and query will work. Scan will allow it, but is very inefficient. GetItem wont work: it needs a single P-KEY and S-KEY.


```

```
A producer application has been designed to write thousands of events per second to Kinesis Streams by integrating the Kinesis Producer library into the application. The application takes data from logs on EC2 instances and ingests the data into Streams records. Which of the following solutions did the developer use to improve throughput when implementing the KPL with the application?

Collection
Aggregation
De-Aggregation
Re-Aggregation

Answer:    
Aggregation

Aggregation refers to the storage of multiple records in a Streams record. Aggregation allows customers to increase the number of records sent per API call, which effectively increases producer throughput. Further information: 

Kinesis Producer Library (KPL) 
```

