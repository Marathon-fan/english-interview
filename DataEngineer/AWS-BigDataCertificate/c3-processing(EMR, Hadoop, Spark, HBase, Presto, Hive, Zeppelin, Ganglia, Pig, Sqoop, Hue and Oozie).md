

# processing(EMR, Hadoop, Spark, HBase, Presto, Hive, Zeppelin, Ganglia, Pig, Sqoop, Hue and Oozie).md

```java
Use "emr-5.4.0"  

With the following components selected: 

Hadoop 2.7.3       -- Apache Hadoop is a collection of open-source software utilities that facilitate using a network of many computers to solve problems involving massive amounts of data and computation

Pig 0.16.0         -- Pig can execute its Hadoop jobs in MapReduce, Apache Tez, or Apache Spark.   

ZooKeeper 3.4.9    -- It is essentially a centralized service for distributed systems to a hierarchical key-value store, which is used to provide a distributed configuration service, synchronization service, and naming registry for large distributed systems

Hue 3.11.0         -- Open-source web interface for Apache Hadoop and other non-Hadoop applications    

Spark 2.1.0        -- a server-based workflow scheduling system to manage Hadoop jobs

Presto 0.166       --  an open-source software project to develop a database query engine using the standard Structured Query Language    

Hive 2.1.1         -- data warehouse software project built on top of Apache Hadoop for providing data query and analysis.

Sqoop 1.4.6        -- a tool designed for efficiently transferring bulk data between Apache Hadoop and structured datastores such as relational databases

HBase 1.3.0        -- Open-source NoSQL database. runs on top of HDFS, providing Bigtable-like(Google) capabilities for Hadoop.

Oozie 4.3.0        -- Apache Oozie is a server-based workflow scheduling system to manage Hadoop jobs.

Zeppelin 0.7.0     -- notebook   

Ganglia  2.7.2     -- monitoring system  

Avro               -- a remote procedure call and data serialization framework developed within Apache Hadoop project. It uses JSON for defining data types and protocols, and serializes data in a compact binary format.

```



## introduction   

```java
Hadoop, Spark, HBase
Presto
Zeppelin, Ganglia, Pig, Sqoop, Hue and Oozie
```

**Presto** is an open source distributed SQL query engine for running interactive analytic queries against data sources of all sizes ranging from gigabytes to petabytes.

Use cases
```java
Log processing / Analytics
Extract, Transform and Load(ETL)
Clickstream Analysis

Architecture
EMR in the AWS Ecosystem
Cluster Operations
Programming Frameworks and Projects
EMR and Hue (Hadoop User Experience)
```

## Apache Hadoop Part 1   

Hadoop(software library = framework) is not HDFS(file system)

What is Hadoop?   
```java
The Apache Hadoop software library is a framework
Distributed processing of large data sets across clusters of computers using simple programming models
Can scale up from single servers to thousands of machines, each offering local computation and storage
The library itself is designed to detect and handle failures at the application layer
```

Hadoop Architecture
```java
Modules
    Hadoop Common(Hadoop Core)
        Libraries and utilities needed by other Hadoop modules
        Considered as the core of Hadoop framework
        Abstraction of the underlying operating system and its file system
        Contens JAR files and scripts required to start Hadoop
    Hadoop Distributed File System(HDFS)
        Distributed File System(can scale across different machines and nodes)
        Fault-tolerant
        Data Replication
        High throughput to data
    Hadoop YARN
        Yet Another Resource Negotiator
        Cluster resource-management and job scheduling
    MapReduce
        Processing very large data sets
        Paralle, distributed algorithm on a cluster
        processing can occur on Filesystem(unstructured) or Database(structured)
        Data locality
        MapReduce Job Tasks
           Map
              Splits data into smaller chunks
              Processed in parallel
           Reduce 
              Outputs of map task become input for the reduce task
              Data is reduced to create an output file

```


## Apache Hadoop Part 2   

Architecture


![sparkArchitecture](./pics/c3-sparkArchitecture.jpg)

MapReduce Job Tasks
```java
Map and Reduce
    Input       ---     unstructured data
    Split       ---     each input will be processed into a (Key, Value) pair 
    Map         ---     intermediate  (Key, Value) pairs are generated 
    Shuffle     ---     moving intermediate (Key, Value) pairs  to the reducers, inside shuffle, also there is sorting   
    Reduce      ---     the data will be aggregated and filtered
    Result      ---     write the result to a file
```    


## EMR Architecture Part 1   


```
Master, Core and Task Nodes
Storage Options
Single Availability-Zone concept
```

![sparkArchitecture](./pics/c3-instanceGroup.jpg)

An EMR cluster can have 50 instance groups   
```
The Master Instance Group can have only Instance Group(node)
The Core Instance Group can have one or more Instance Groups
The Task Instance Group can have up to 48 Instance Groups
```

Master Node
```java
Single master node   
Manages the resources of a cluster
    Coordinates distribution and paralled execution of MapReduce executables
    Tracking and directing against HDFS
    ResourceManager
    Monitors health of the core and task nodes

Core Node
    Slave node
    Run tasks
    HDFS or EMRFS(EMR file system(derived from HDFS with S3 integration))
    DataNode
    NodeManager
    ApplicationMaster
    "Shrink" operation

Task Node 
    Slave node
    Optional
    No HDFS
    Added and removed from running clusters
    Extra capacity   

```



## EMR Architecture Part 2   


HDFS
```java
what is a distributed file system
    allow simultaneous access to data and files from many clients to aset of distributed machines

    share files and storage resources
    replication
    Typical DFS(distributed file system) would not work well for MapReduce, hence we have HDFS

```


![HDFS-fileReplication](./pics/c3-HDFS-fileReplication.jpg)  
Blocks are replicated across Slave Nodes


HDFS Block Sizes
```java
   Block sizes are files tend to be large
   64 MB is default
   No real limit on how large a block can be, generally a range from 64 MB to 256 MB
   Depends on the data
       For very large files you may use 256 MB
       For smaller files you may use 64 MB
       Understand your data, access patterns, cluster size

   Why are block sizes large in HDFS?
       Minimize random disk seeks and latency
       Disk seeks: time taken for a disk drive to locate the area on the disk 
       Data sequentially laid out on disk

   Block sizes can be set per file
   
   Replication Factor
       Set in the hdfs-site.xml file(the default value is 3)       
       can be changed per file 
```


```sh
hadoop fs -ls

hadoop fs -stat %r defaultblock.txt

hadoop fs -setrep -R -w 2 defaultblock.txt   # change the replication factor to 2

hadoop fs -stat %r defaultblock.txt
```

Storage Option
```java
Instance Store
    High I/O performance
    High IOPS at low cost
    D2 and I3

EBS for HDFS
    When using EBS with EBR,  volumes DO NOT persist after cluster termination

EMR File System(EMRFS)
    An implementation of HDFS which allows clusters to store data on S3
    Uses data directly on S3 without ingesting into HDFS
    Reliability, durability and scalability of S3    
    Resize and terminate EMR clusters without losing data
    Use EMRFS and HDFS together
        Copy data from S3 to HDFS using S3DistCp
        High I/O performance
        Processing the same dataset frequently

EMRFS and Consistent View
    S3 - Read after write consistency for new put requests
    S3 - Eventual consistency for overwrite of put and delete requests
    Listing after quick write may be incomplete
    For example an ETL pipeline that depends on list an input to subsequent steps       
    Consistent View: checks for list and read-after-read consistency for new S3 objects written or synched with EMRFS
    Retry logic if inconsistency is deleted
    Metadata in DynamoDB keeps track of S3 objects

```

Single Availability Zone Concept
```java
Block replication
Master, core and task node communication
Access to Metadata
Single master node
Launch replacement clusters easily(in single Single Availability Zone)
```

## EMR Architecture Part 1   

place holder    

## EMR Architecture Part 2   

place holder    


## EMR Architecture Part 3   

place holder    


## EMR Architecture Part 4    

place holder    



## Using Hue with EMR   

Hue = Hadoop User Experience     
```
Open-source web interface for Apache Hadoop and other non-Hadoop applications   
Browser-based
Easier to manage EMR clusters
```

![Hue1](./pics/c3-Hue1.jpg)  
```java
Use "emr-5.4.0"  

With the following components selected: 

Hadoop 2.7.3       -- Apache Hadoop is a collection of open-source software utilities that facilitate using a network of many computers to solve problems involving massive amounts of data and computation

Pig 0.16.0         -- Pig can execute its Hadoop jobs in MapReduce, Apache Tez, or Apache Spark.   

ZooKeeper 3.4.9    -- It is essentially a centralized service for distributed systems to a hierarchical key-value store, which is used to provide a distributed configuration service, synchronization service, and naming registry for large distributed systems

Hue 3.11.0         -- Open-source web interface for Apache Hadoop and other non-Hadoop applications    

Spark 2.1.0        -- a server-based workflow scheduling system to manage Hadoop jobs

Presto 0.166       --  an open-source software project to develop a database query engine using the standard Structured Query Language    

Hive 2.1.1         -- data warehouse software project built on top of Apache Hadoop for providing data query and analysis.

Sqoop 1.4.6        -- a tool designed for efficiently transferring bulk data between Apache Hadoop and structured datastores such as relational databases
                   -- batch data migration tool that allow you to transfer data between relational database and Hadoop

HBase 1.3.0        -- Open-source NoSQL database. runs on top of HDFS, providing Bigtable-like(Google) capabilities for Hadoop.

Oozie 4.3.0        -- Apache Oozie is a server-based workflow scheduling system to manage Hadoop jobs.

Zeppelin 0.7.0     -- notebook   

Ganglia  2.7.2     -- monitoring system  

```

demo
```java
After installing Amazon EMR
on AWS console -> Amazon EMR -> Cluster list -> Connections
click Hue(then we can see the Hue page on the browser)

after creating super user account,

With Hue, we can get access to the following browsers:   
S3 browser   
HDFS browser

With Hue, we can use Hive Editor and Pig Editor   
we can save scripts

With Hue, we can access Hive(Metastore Tables), HBase and Zookeeper

With Hue, we have a Job browser where we can see the status of our jobs

With Hue, we can access Server Logs(searchable logs)   

With Hue, we can access Oozie Editor(workflow scheduler system to manage Hadoop Job) 

With Hue, we can use a number of Authentication Providers: LDAP, PAM, SPNEGO, OpenID, OAuth, SAML2
on the EMR master node, run the following command:   
cd /etc/hue/conf
vi hue.ini # then we can see the ldap settings   
           # for security reasons, it is better to store LDAP settings on S3 rather on the EMR master node

```


For the exam
```java
Have an overall understanding of the benefits and advantages of Hue
```




## Hive on EMR   

What is Hive
```java
Data Warehouse infrastructure built on top of Hadoop    
Summarize, Query and Analyze very large data sets
Uses a SQL-like interface(HiveQL)

Hive is useful for non-Java programmers
Hadoop is implemented in Java
Hive provides SQL ABSTRACTION to integrate HiveQL queries into the underlying java API without having to write Java programs

Hive is a high-level programming language
- with strong abstraction from the details of the computers
- needs an interpreter
- easier to read, write and maintain

Use cases
- Process and analyze logs
- Join very large tables
- batch jobs
- ad-hoc interactive queries
```

Hive architecture  
![Hive architecture](./pics/c3-HiveArchitecture.jpg)

Hive and AWS Ecosystem
```java
Apache Hive and Hive on EMR differences
S3
DynamoDB
Kinesis Streams (EMR 3.2.0 has a connector)
```

s3
```java
Read and write data from and to S3   
EMRFS(extends HDFS and can access files from S3)   
Partitioning in Hive   
Partitioning supported with S3
Time based data or source based with date

```

DynamoDB
```java
Join Hive and DynamoDB tables
Query data in DynamoDb tables
Copy data from a DynamoDB table into HDFS and vice versa
Copy data from DynamoDB to S3
Copy data from S3 to DynamoDB

EMR DynamoDB connector
```

demo
```java
create a table(in s3)

create a table on HDFS(Hive table)

create a DynamoDB table

copy data from Hive table in HDFS to DynamoDB

```

Hive Serializer/Deserializer (SerDe)   

```java
Serializer/Deserializer 
    Read data and write it back to HDFS or EMRFS in any format and vice versa
    JSON SerDe   
    RegEx SerDe  

```



## HBase on EMR   




```java
HBase is a column-oriented database management system that runs on top of Hadoop Distributed File System (HDFS) or on top of Amazon S3(using EMRFS).
 
 It is built for random, strictly consistent realtime access for tables with billions of rows and millions of columns

 Apache HBase integrates with Apache Hadoop, Apache Hive and Apache Phoenix so you can combine massively paralled analytics with fast data access

 It is well suited for sparse data sets, which are common in many big data use cases. ... An HBase system comprises a set of tables. Each table contains rows and columns, much like a traditional database.

HBase is an open-source, non-relational, distributed database modeled after Google's Bigtable and written in Java. It is developed as part of Apache Software Foundation's Apache Hadoop project and runs on top of HDFS, providing Bigtable-like capabilities for Hadoop. 

```

Use Cases
```java
Ad Tech
Content (Facebook, Spotify)
Financial Data (FINRA)

```

When should you use HBase?
```java
Large amounts of data - 100s of GBs to PBs
High write throughput and update rates
NoSQL, flexible schema
Fast access to data, random and real-time
Fault-tolerance in a non-relational environment
```

When NOT to use HBase
```java
Transactional applications
Relational database type of features
Small amounts of data
```

HBase vs DynamoDB    
```java
HBase                             DynamoDB

Wide column store                 Key-value store
No row size restrictions          Item size restricted
Flexible row key data types       Scalar types
Index creation is more manual     Easier index creation
```

HBase vs Redshift
```java
HBase                                                Redshift
Column-oriented                                      Column-oriented
Write throughput and updates perform well            Batch writes (COPY recommended) and updates are slow (not recommended)
Near real-tiem lookups (over fast changing data)     OLAP (Large complex queries, JOINS, aggregations)    
```    

  
![HBaseArchitecture](./pics/c3-HBaseArchitecture.jpg)  
HBaseArchitecture  

![HBase&EMRFS](./pics/c3-HBaseAndEMRFS.jpg)  
HBase&EMRFS  

![HBaseIntegration](./pics/c3-HBaseIntegration.jpg)    
HBaseIntegration   



using Hive to query data from HBase
```sh
hbase shell
create 'epl', 'teams'
put 'epl', 'r1','teams:col1', 'Liverpool'
scan 'epl'
exit

hive
set hbase.Zookeeper.quorum=ec2-35-xx-xx-xx.compute-1.amazonaws.com;  # in this demo, zooker and hive are in the same node
hive> create external table epl_hive (key string, value string)
    > stored by 'org.apache.hadoop.hive.HBaseStorageHandler'
    > with serdeproperties ("hbase.columns.mapping" = ":key,teams:col1")
    > tblproperties ("hbase.table.name" = "epl");
hive> select * from epl_hive

```

For the exam
```java
What is HBase
When to use HBase and when NOT to
Compared to DynamoDB and Redshift
Hbase integration with Hadoop, Hive, Phoenix, HDFS and EMRFS

```



## EMR Presto   

What is Presto?   
```java
Open-source in-memory distributed fast SQL query engine
Run interactive analytic queries against a variety of data sources with sizes ranging form GBs to PBs
Faster than Hive
```   

Advantages of using Presto
```java
Query different types of data sources from relational databases, NoSQL databases, frameworks like Hive to stream precessing platforms like Kafka

High concurrency, run thousands of queries per day(sub-second to minutes)

In-memory processing helps avoid unnecessary I/O, leading to low latency

Does not need an interpreter layer like Hive does(Tez)
```

Don't use Presto for ...
```java
Not a database and not designed for OLTP
Joining very large(100M plus rows) requires optimization(use Hive instead)
Batch processing(with Presto, there is no disk IO. data from multiple datastores needs to be feeded into memory)
```



presto connectors
```java
hive
jmx
Kafka
mongodb
mysql
postgresql
redis
...
```


Presto architecture
![Presto architecture](./pics/c3-Presto.jpg)


Presto Demo
```sh
environment: Hive, Hue, Presto

$ hive
$ create a table on S3
# run the query
$ select sum(l_discount) from lineitem2;
# the demo(Hive) takes 70 s to be completed

$ presto-cli
presto> use hive.default
presto:default> select sum(l_discount) from lineitem2;
# in presto Demo, it takes 12 s to be completed

```

For the exam
```sh
Have a high-level understanding of Presto
Know where Presto should be used and not be used

```


## Spark on EMR part 1   
What is Spark?
```java
Fast engine for processing large amounts of data
Run in-memory
Run on disk
```

Use cases
```java
Interactive Analytics
    Faster than running queries in Hive
    Flexibility in terms of languages(Scala, Python, etc.)
    Run queries against live data (Spark 2.0)
        Structured Streaming

Stream Processing        
    Disparate data sources
    Data in small sizes

Machine Learning
    Repeated queries at scale against data sets
    Train machine learning algorithms
    Machine Learning Library (MLlib)
        Recommendation Engines
        Fraud Detection
        Customer Segmentation
        Security

Data integration
    Extract, transform and load(ETL)
    Reduce time and cost
```


avoid using spark when ...
```java
Not a database and not designed for OLTP
very large batch processing(use disk, as memory does not support the huge data size. in this situation, use Hive)
Avoid for large multi-user reporting environments with high concurrency...
   Run ETL in Spark and copying the data to a typical reporting database
   Run batch jobs in Hive instead(or perhaps even schedule them using Oozie)

```

![SparkStack](./pics/c3-SparkStack.jpg)    

Spark Core has four modules: **Spark SQL, Spark Streaming, MLlib, Graphx**

The Spark Stack runs on top of Cluster Managers, these includes: **Standalone Scheduler, YARN, Mesos**


Spark Core -- General execution engine
```java
    dispatch and scheduling of tasks
    Memory management
    Supports APIs for Scala, Python, SQL, R and Java
    Supports the following APIs to access data
        Resilient Distributed Datasets(RDDs) are a logical collection of data partitioned across machines
        DataSet is distributed collection of data
        DataFrame is a DataSet organized into named columns
```

Spark SQL
```java
    Run low-latency, interactive SQL queries against structured data
    RDD and DataFrame APIs to access variety of data sources using Scala, Python, Java, R or SQL
    Avro, Parquet, ORC and JSON
    Join across data sources
    Supports querying Hive tables using HiveQL
    JDBC/ODBC with existing databases

```

Spark Streaming
```java
an extension of the core Spark API that enables scalable, high-throughput, fault tolerant stream processing of live-data streams
```
![SparkStreaming](./pics/c3-SparkStreaming.jpg)


MLlib
```java
Spark scalable machine learning library
MLlib for out-of-the-box scalable, distributed machine learning algorithms
Read data from HDFS, HBase or any Hadoop data source
Write MLlib applications using Scala, Java, Python, or SparkR 

Classification: logistic regression, naive Bayes,...
Regression: generalized linear regression, survival regression,...
Decision trees, random forests, and gradient-boosted trees
Recommendation: alternating least squares (ALS)
Clustering: K-means, Gaussian mixtures (GMMs),...
Topic modeling: latent Dirichlet allocation (LDA)
Frequent itemsets, association rules, and sequential pattern mining

```

GraphX
```
Spark's API for grphs and graph-parallel computation
Interactively build and transform graph structured data at scale
Supports a number of graph algorithms

```

Cluster Managers
```
Driver Program(Spark Context) coordinates processes
Drive Program connects to Cluster Manager(it can be standalone scheduler, MESOS or YARN)
Spark acquires executors
Spark sends application code to executors
Driver Program sends taks to the executors to run
```

```
standalone Scheduler(here, Spark doesn't require hadoop as long as we have a shared file system)
Hadoop YARN
   Kerberos authentication
   Dynamic allocation of executors
   Dynamic sharing and central configuration of resources across various engines

Apache Mesos
   Hadoop, Spark
   Kafka, Elasticsearch
   Resource management across entire datacenters and cloud environments

```

![clusterManager](pics/c3-clusterManager.jpg)


File Format Benchmarks - **Avro, JSON, ORC, & Parquet**


## Spark on EMR part 2   


![SparkFrameworkReplacingMapReduceFramework](pics/c3-SparkFrameworkReplacingMapReduceFramework.jpg)

When running Spark on EMR, the Spark procsssing engine is deployed on each node of the cluster. The spark framework replaces MapReduce framework.

Spark and SparkSQL can still interact with HDFS(also with S3).


**EMR Ecosystem**
![EMREcosystem](pics/s3-EMREcosystem.jpg)


**Spark Streaming and Kinesis Streams**  
```
Spark Streaming provides High-level abstraction called DStreams (Distretized Streams), which are a continuous stream of data    
DStreans can be created from input data streams from sources like Kinesis Streams
DStreams are a collection of RDDs
Transformations are applied to the RDDs
Results published to HDFS, databases or dashboards   
```

![c3-KinesisAndSpark-EMR](pics/s3-c3-KinesisAndSpark-EMR.jpg)

AWS big data blogs for the exam
```
Querying Amazon Kinesis Streams Directly with SQL and Spark Streaming
Optimize Spark-Streaming to Effciently Process Amazon Kinesis Streams
Analyze Realtime Data from Amazon Kinesis Streams Using Zeppelin and Spark Streaming
Powering Amazon Redshift Analytics with Apache Spark and Amazon Machine Learning
Analyze Your Data on Amazon DynamoDB with Apache Spark 

```

**Spark and Redshift**  

```
Spark for extraction, transformation and loading (ETL) of large amounts of data
    Hive tables in HDFS or S3, text files (csv), parquet etc
    ETL in Spark gives you a performance benefit

Redshift for analysis
```

```
Databrick's Spark-Redshift library
   Reads data from Redshift and can write back to Redshift by loading data into Spark SQL DataFrames
       Executes the Redshift UNLOAD command to copy data to a S3 bucket
       Reads the files in the S3 bucket
       Spark SQL DataFrame is generated
       Queries can then be executed
   Spark-Redshift allows you to work with data in S3, Hive tables, text or Parquet files on HDFS    
```

![c3-SparkRedshift](pics/c3-SparkRedshift.jpg)



**Spark and DynamoDB**    
example
```sh
EMR-dynamoDB-connecter  lib 

# install spark-shell

$ spark-shell --jars /usr/.../emr-ddb-hadoop.jar 

$> scala   # paste the code and we will see the result

```

for the exam
```sh
Know how Spark Streaming and Kinesis Streams work together
High level understanding of how Spark integrates with Redshift and DynamoDB

```


![c3-SparkAndDynamoDB](pics/c3-SparkAndDynamoDB.jpg)


## EMR File Storage and Compression       

Files in HDFS and S3
```java
In HDFS data files are split into chunks automatically by Hadoop

If your data files are stored in S3, Hadoop will split the data on S3 by reading the files in multiple HTTP range requests
```

EMR Hadoop Reading S3 Files

![EMRHadoopReadingS3Files](./pics/c3-EMRHadoopReadingS3File1.jpg)


![EMRHadoopReadingS3Files](./pics/c3-EMRHadoopReadingS3File2.jpg)



![HDFSArchitecture](./pics/c3-HDFSArchitecture.jpg)


**Compression**
```
GZIP        ---Compression Ratio high,  compress/decompress speed slow            not splittable
bzip2       ---Compression Ratio high,  compress/decompress speed slow            splittable

LZO         ---Compression Ratio low,  compress/decompress speed fast             splittable
Snappy      ---Compression Ratio low,  compress/decompress speed fast             not splittable
```

![compressFormat](./pics/c3-compressFormat.jpg)


Compression
```
Better performance when less data is transferred between S3, mappters and reducers
Less network traffic between S3 and EMR
Reduce storage costs 
```


File Formats
```java
Text(csv, tsv)
Parquet - Columnar-oriented file format
ORC - Optimized Row Columnar file format
Sequence - Flat files consisting of binary key/value pairs
Avor - Data serialization framework
```

File Sizes
```java
GZIP files are not splittable, keep them in the 1-2 GB range
Avoid smaller files(100 MB or less), plan for fewer larger files
S3DistCp can be used to combine small files into larger files
    An extension of DistCp
    S3DistCp can be sued to copy data between S3 buckets or S3 to HDFS or HDFS to S3
    S3DistCp can be added as a step
```

For the exam
```
Study the compression algorithm table
```



## EMR Lab  



## Lambda in the AWS Big Data Ecosystem    

What is AWS Lambda?  
```java
    Function As A Service (Faas) product
    Billed for only the number of milli-seconds (ms) you use
    Isolated and stateless
    Presistence - your responsibility
    Event-driven
    Integates with other AWS Services
    Invoked via API Gateway(Lambda is not directly accessable. You need to map a url and the http method to trigger the lambda code)    
```

At the end of Lambda, it can write the result to its own destination, or generate its own event(interact Internet service over HTTPS, or another AWS service over SNS, SQS, or invoke another Lambda Function)    


![c3-Lambda_bigData](./pics/c3-Lambda_bigData.jpg)  
serverless framework for Big Data  

Lambda's limitations:

```java
Ephemeral disk capacity ("/tmp" space) - 512MB
Number of processes and threads - 1024
Maximum execution duration - 300 seconds
```


lambda and Kinesis Stream integration    
![c3-lambdaKinesisStream](./pics/c3-lambdaKinesisStream.jpg)  
Lambda polls the Kinesis Stream every second    


lambda and Redshift integration    
Redshift Database Loader is Lambda-based    
Files can be pushed to S3, and then be loaded automatically into a Redshift cluster   

For the Exam
```
Understanding of Lambda
Integration with AWS Big Data service 
References (AWS Big Data Blog and Documentation)     
...

```


## test    


```java

Which of the following are the 4 modules (libraries) of Spark? (Choose 4)

SparkSQL
MLlib
GraphX
YARN
Apache Mesos
Spark Streaming

answer:
Spark’s four modules are MLlib, SparkSQL, Spark Streaming, and GraphX.

```



```java
Which open-source Web interface provides you with a easy way to run scripts, manage the Hive metastore, and view HDFS?

Apache Zeppelin
Ganglia
Hue
YARN Resource Manager

answer:
Hue (Hadoop User Experience) is an open-source, web-based, graphical user interface for use with Amazon EMR and Apache Hadoop. Hue groups together several different Hadoop ecosystem projects into a configurable interface for your Amazon EMR cluster. Further information: http://docs.aws.amazon.com/emr/latest/ReleaseGuide/emr-hue.html

```

```java
Which of the following does Spark Streaming use to consume data from a Kinesis Stream?

Kinesis Producer Library
Kinesis Client Library
Kinesis Consumer Library
Kinesis Connector Library

answer:
Spark Streaming uses the Kinesis Client Library (KCL) to consume data from a Kinesis stream. KCL handles complex tasks like load balancing, failure recovery, and check-pointing. Further information: https://spark.apache.org/docs/latest/streaming-kinesis-integration.html

```

```java

How are EMR tasks nodes different from core nodes? (Choose 3)

Task nodes do not include HDFS.
Task nodes run the Resource Manager.
They are used for extra capacity when additional CPU and RAM are needed.
Task nodes are optional.
Task nodes run the NodeManager daemon.

answer:

Task nodes are used for extra capacity when additional CPU and RAM are needed. They are optional and do not include HDFS. Further information: http://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-plan-instances.html#w2ab1c18c25c17



```

```java
Your EMR cluster requires high I/O performance and at a low cost. In terms of storage, which of the following is your best option?

EMRFS
EBS volumes with PIOPS
Instance store volumes
EMRFS with consistent view

answer:
D2 and I3 instance types provide you with various options in terms of the amount of instance storage. This instance storage can be used for HDFS if the I/O requirements of the EMR cluster are high.

```

```java
When should you not use Spark? (Choose 2)

In multi-user environments with high concurrency
For batch processing
For ETL workloads
For interactive analytics

answer:

Do not use Spark for batch processing. With Spark, there is minimal disk I/O, and the data being queried from the multiple data stores needs to fit into memory. Queries that require a lot of memory can fail. For large batch jobs, consider Hive. 
Also avoid using Spark for multi-user reporting with many concurrent requests.
```

```java
True or False: Presto is a database.

False
True

answer:
Presto is an open source distributed SQL query engine for running interactive analytic queries against data sources of all sizes ranging from gigabytes to petabytes.

```

```

You have just joined a new company as an AWS Big Data Architect, replacing an architect who left to join a different company. As a data driven company, your company has started using several of AWS' Big Data services in the last 6 months. Your new manager is concerned that the AWS charges are too high, and she has asked you to review the monthly bills. After review, you determine that the EMR costs are unnecessarily high considering the company uses EMR to process new data within a 6 hour period that starts at midnight and ends between 5 AM and 7 AM, depending on the amount of data that needs to be processed. The data that needs to be processed is already in S3. However, it appears that the EMR cluster that processes the data is running 24 hours a day, 7 days a week. What type of cluster should your predecessor have configured in order to keep costs low and not unnecessarily waste resources?

Nothing. AWS announces frequent price reductions, and costs will balance-out over time.
He should have used auto-scaling to reduce the number of core nodes and task nodes running when no processing is taking place.
To reduce costs, your predecessor should have purchased reserved instances for the EMR cluster.
Your predecessor should have configured the cluster as a transient cluster.

answer:

With a transient cluster, the input data is loaded, processed, the output data is stored in S3 and the cluster is automatically terminated. Shutting-down the cluster automatically ensures that you are only billed for the time required to process your data.

Amazon EMR provides configuration options that control how your cluster is terminated—automatically or manually. If you configure your cluster to be automatically terminated, it is terminated after all the steps complete. This is referred to as a transient cluster. 
```

```java
You have just joined a company that has a petabyte of data stored in multiple data sources. The data sources include Hive, Cassandra, Redis, and MongoDB. The company has hundreds of employees all querying the data at a high concurrency rate. These queries take between a sub-second and several minutes to run. The queries are processed in-memory, and avoid high I/O and latency. A lot of your new colleagues are also happy they did not have to learn a new language when querying the multiple data sources. Which open-source tool do you think your new colleagues are using?

Hive
SparkSQL
Big Data Query Engine
Presto

answer:
Good Work!
Presto is a fast, open-source, in-memory, distributed SQL query engine. Since it uses ANSI SQL, you don’t have to learn another language to use it. It is used to run interactive analytic queries against a variety of data sources with sizes ranging from GBs to PBs. These data sources include Cassandra, Hive, Kafka, MongoDB, MtSQL, PostgresSql, Redis, and a number of others. Presto is also significantly faster than Hive. Further information: http://docs.aws.amazon.com/emr/latest/ReleaseGuide/emr-presto.html

```

```java

True or False: EBS volumes used with EMR persist after the cluster is terminated.
answer:

When EBS volumes are used with EMR, the volumes do not persist after cluster termination. Compare this to how EBS behaves when used with an ordinary RC2 instance: it is possible for the volume to persist after its instance is terminated.

Amazon Elastic Block Store (Amazon EBS) provides persistent block storage volumes for use with Amazon EC2 instances in the AWS Cloud. 
```

```java

You plan to use EMR to process a large amount of data that will eventually be stored in S3. The data is currently on-premise, and will be migrated to AWS using the Snowball service. The file sizes range from 300 MB to 500 MB. Over the next 6 months, your company will migrate over 2 PB of data to S3 and costs are a concern. Which compression algorithm provides you with the highest compression ratio, allowing you to both maximize performance minimize costs?

Snappy
bzip2
LZO
GZIP

answer:


```


Snowball is a petabyte-scale data transport solution that uses devices designed to be secure to transfer large amounts of data into and out of the AWS Cloud.


```java
You plan to use EMR to process a large amount of data that will eventually be stored in S3. The data is currently on-premise, and will be migrated to AWS using the Snowball service. The file sizes range from 300 MB to 500 MB. Over the next 6 months, your company will migrate over 2 PB of data to S3 and costs are a concern. Which compression algorithm provides you with the highest compression ratio, allowing you to both maximize performance minimize costs?

Snappy


bzip2


LZO


GZIP

answer:

Of the possible selections, the bzip2 compression algorithm has the highest compression ratio.

```

```java


answer:


```

