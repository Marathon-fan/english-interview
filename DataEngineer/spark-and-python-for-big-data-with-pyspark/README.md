


# pySpark -- Python and Spark   

https://www.udemy.com/spark-and-python-for-big-data-with-pyspark/learn/v4/content

Python and Spark



## install pySpark

target

```sh
Spark/PySpark    

Big Data   

PySpark DataFrames
Spark 2.0
Working with very large datasets
Spark specifics, like Spark Streaming


Spark and Big Data basics
Setting up Spark in various ways
Python Crash Course
Python and Spark 2.0 DataFrames
PySpark Project Exercise

Introduction to Machine Learning
Linear Regression
Logistic Regression
Decision Trees and Random Forests
Gradient Boosted Trees


```


```sh
install Java  
install anaconda   

download spark-2.3.2-bin-hadoop2.7.tgz
tar -zxvf spark-2.3.2-bin-hadoop2.7.tgz   
cd spark-2.3.2-bin-hadoop2.7

sudo vi ~/.bash_profile, then add the following lines:   
export SPARK_PATH=/Desktop/installedPrograms/spark-2.3.2-bin-hadoop2.7   
export PYSPARK_DRIVER_PYTHON="jupyter"    
export PYSPARK_DRIVER_PYTHON_OPTS="notebook"   

alias snotebook='$SPARK_PATH/bin/pyspark --master local[2]'      

#then
source .bash_profile



```

## RDD   

RDD = resilient distributed dataset(a collection of elements partitioned across the nodes of the cluster that can be operated on in parallel)   

There are two ways to create RDDs: parallelizing an existing collection in driver program, or referencing a dataset in an external storage system, such as a shared filesystem, HDFS, HBase, or any data source offering a Hadoop InputFormat


## cmd   

```sh
jupyter notebook   
# then we can have the 'jupyter notebook' open in the browser    



```


## hadoop   


## anaconda   

## commonly used data science libraries    

```sh
Core Libraries & Statistics
1. NumPy 
2. SciPy 
3. Pandas
4. StatsModels 

Visualization
5. Matplotlib 
6. Seaborn
7. Plotly 
8. Bokeh
9. Pydot 

Machine Learning
10. Scikit-learn
11. XGBoost / LightGBM / CatBoost
12. Eli5 

Deep Learning
13. TensorFlow 
14. PyTorch
15. Keras

Distributed Deep Learning
16. Dist-keras / elephas / spark-deep-learning

Natural Language Processing
17. NLTK
18. SpaCy
19. Gensim

Data Scraping
20. Scrapy
```







