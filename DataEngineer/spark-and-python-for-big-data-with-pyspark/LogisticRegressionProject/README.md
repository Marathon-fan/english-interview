


# pySpark -- Python and Spark   

https://www.udemy.com/spark-and-python-for-big-data-with-pyspark/learn/v4/content

data set address:  
https://github.com/clumdee/Python-and-Spark-for-Big-Data-master/tree/master/Data%20Set%20Generator%20(remove%20me%20the%20future!)/DataSets

download data
```sh
aws s3 ls s3://emr-data-sydney

sudo aws s3 cp s3://emr-data-sydney/customer_churn.csv /home/ec2-user/EMRData/customer_churn.csv

```

# Logistic Regression Theory  

## classification  

Not all labels are continuous, sometimes you need to predict categories, this is known as classification.

## Logistic Regression is one the basic ways to perform classification   

don't be confused by the word "regression"


## examples of classification problems   

examples of Binary Classification
```sh
Spam versus "Ham" emails
Loan default(yes/no)
Disease Diagnosis
```

the convention for binary classification is to have two classes 0 and 1

## theory   

Sigmoid Function  
The Sigmoid (aka Logistic) Functin takes in any value and outputs in to between 0 and 1   

Confusion Matrix   
True Positive(TP)         False Negative(FN)
False Positive(FP)        True Negative(TN)

**Model Evaluation**   
The main point to remember with the confusion matrix and the various calculated metrics is that they are all fundamentally ways of **comparing the predicted values versus the true values**   

**accuracy = (TP + TN) / total**   
**Misclassification rate(error rate) = (FP + FN) / total**   



# Logistic Regression Consulting Project    

```
You've been contacted by a top marketing agency to help them out with customer churn(predict customer churn)!

A marketing agency has many customers that use their service to produce ads for the client/customer websites
They've noticed that they have quite a bit of churn in clients
They currently randomly assign account managers, but want you to create a machine learning model that will help predict which customers will churn(stop buying their servie) so that they can correctly assign the customers most at risk to churn an account manager
Luckily they have some historical data, can you help them out?

Create a classification algorithm that will help classify whether or not a customer churned(create a model that can predict whether a customer will churn(0 or 1) based off the features)    
Then the company can test this against incoming data for future customers to predict which customers will churn and assign them an account manager

```  

data(customer_churn.csv)
```
Name: Name of the latest contact at Company  
Age: Customer Age(number of years the customer in business)   
Total_Purchase: Total Ads Purchased   
Account_Manager: Binary 0 = No manager, 1 = Account manager assigned
Years: Total Years as a customer  
Num_sites: Number of websites that use the service
Onboard_date: Data that the name of the lastest contact was onboarded
Location: Client HQ address
Company: Name of Client Company  

Churn: 0 or 1 indicating whether customer has churned
```

```python

import findspark
findspark.init('/home/ec2-user/spark-2.1.1-bin-hadoop2.7')
from pyspark.sql import SparkSession   
spark = SparkSession.builder.appName('logreconsult').getOrCreate()

csvSchema = StructType()
                     .add("Names", StringType(), True)
                     .add("Age", DoubleType(), True)        
                     .add("Total_Purchase", DoubleType(), True)
                     .add("Account_Manager", IntegerType(), True)        
                     .add("Years", DoubleType(), True)
                     .add("Num_Sites", DoubleType(), True)        
                     .add("Onboard_date", TimestampType(), True)
                     .add("Location", StringType(), True)        
                     .add("Company", StringType(), True)                                                                           
                     .add("Churn", IntegerType(), True)

# !!!!!!!!!!!!!!!!!!!!!!!! use python to load csv with specified schema
# !!!!!!!!!!!!!!!!!!!!!!!!

data = spark.read.csv('EMRData/customer_churn.csv', inferSchema=True, header=True)
data.printSchema()

df = spark.read.format("com.crealytics.spark.excel").
         schema(yourSchema).
         option("useHeader", "true").\
         load("D:\\Users\\ABC\\Desktop\\TmpData\\Input.xlsm")


data.describe().show()
data.columns

from pyspark.ml.feature import VectorAssembler
assembler = VectorAssembler(inputCols=[
 'Age',
 'Total_Purchase',
 'Account_Manager',
 'Years',
 'Num_Sites'], outputCol = 'features') 
output = assembler.transform(data)
final_data = output.select('features', 'churn')


train_churn, test_churn = final_data.randomSplit([0.7, 0.3])
from pyspark.ml.classification import LogisticRegression
lr_churn = LogisticRegression(labelCol='churn')
fitted_churn_model = lr_churn.fit(train_churn)
training_sum = fitted_churn_model.summary
training_sum.predictions.describe().show()

from pyspark.ml.evaluation import BinaryClassificationEvaluator
pred_and_labels = fitted_churn_model.evaluate(test_churn)
pred_and_labels.predictions.show()



```


