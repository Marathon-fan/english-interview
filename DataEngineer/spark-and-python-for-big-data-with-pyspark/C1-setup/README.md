


# pySpark -- Python and Spark   

https://www.udemy.com/spark-and-python-for-big-data-with-pyspark/learn/v4/content


# AWS EC2 PySpark set-up   


```sh
1 login to EC2 instance  

then 
$ aws configure
AWS Access Key ID [None]: AKIAIOSFODNN7EXAMPLE
AWS Secret Access Key [None]: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
Default region name [None]: us-west-2
Default output format [None]: json


2 

sudo apt-get update  
sudo yum update

sudo apt install python3-pip  
sudo yum install python36
sudo yum -y install python36-pip


sudo yum install -y python36-setuptools
sudo easy_install-3.6 pip
sudo pip install --upgrade pip    
# then tools like pip3 will be installed   

3
pip install --upgrade pip

alias sudo='sudo env PATH=$PATH'  
sudo pip3 install jupyter   

alias sudo='sudo env PATH=$PATH'  
sudo pip3 install numpy    

4 
sudo apt-get install default-jre    
sudo yum install default-jre    
sudo yum install java-1.8.0-openjdk     # install java 1.8 
sudo alternatives --config java         # configure so that we get the right java version   

java -version

# install sdk
curl -s "https://get.sdkman.io" | bash
source "/home/ec2-user/.sdkman/bin/sdkman-init.sh"

# install scala using sdk
sdk install scala
scala -version

# 
alias sudo='sudo env PATH=$PATH'
sudo pip3 install py4j


# 
wget http://archive.apache.org/dist/spark/spark-2.1.1/spark-2.1.1-bin-hadoop2.7.tgz

sudo tar -zxvf spark-2.1.1-bin-hadoop2.7.tgz    
cd spark-2.1.1-bin-hadoop2.7   
pwd
/home/ec2-user/spark-2.1.1-bin-hadoop2.7

cd 

sudo pip3 install findspark

jupyter notebook --generate-config   
# Writing default config to: /home/bitnami/.jupyter/jupyter_notebook_config.py

mkdir certs
cd certs

sudo openssl req -x509 -nodes -days 365 -newkey rsa:1024 -keyout mycert.pem -out mycert.pem    
# then enter  

cd ~/.jupyter/
vi jupyter_notebook_config.py 
# then add the following info and save(:wq)
c = get_config()
c.NotebookApp.certfile = u'/home/ec2-user/certs/mycert.pem'
c.NotebookApp.ip = '0.0.0.0'   # c.NotebookApp.ip = '*'
c.NotebookApp.open_browser = False
c.NotebookApp.port = 8888

# then
cd 
jupyter notebook

https://52.62.145.154:8888/?token=5b4760281f8d896cec5e925ba3b087610ef583eead63172d
# click advanced, and proceed to ec2 instance

inside the brower, new a python3 project(enter the following cotent)   

```

```python

  
import findspark
findspark.init('/home/ec2-user/spark-2.1.1-bin-hadoop2.7')
import pyspark


```

# Python Crash    

## Jupyter   

```
Jupyter Notebooks use a .ipynb file format but can be saved to a .py file    

```

# Spark DataFrame Basics

```
RDD (early implementation) = resilient distributed dataset

DataFrame  (from Spark 2.0  ) --- cleaner and easier than RDD



```

# Linear Regression   

```


```


# Databricks setup   

```
Databricks is a company that provides clusters that run on top of AWS and adds a convience of having a Notebook System already set up and the ability to quickly add files(either from S3 or local computer file system)

free community version  

it has its own storage format known as DBFS

Databricks -- quickest online setup   

https://databricks.com/try-databricks    


```

# AWS EMR setup   

```
What is Apache Zeppelin ---  Multi-purpose Notebook
- Data Ingestion
- Data Discovery
- Data Analytics
- Data Visualization & Collaboration



The Zeppelin Notebook is a farily new environment that mimics Jupyter Notebook's capabilities but was created specifically with Big Data(Spark, Hadoop, etc...)

zeppelin.apache.org
aws.amazon.com
```

## EMR(managed hadoop framework) setup   


```sh
AWS console -> EMR

click create cluster  # give it a name

on Software configuration, choose: Spark: Spark 2.3.2 on Hadoop 2.8.5 YARN with Ganglia 3.7.2 and Zeppelin 0.8.0

Hardware configuration, choose defualt(or m1.medium to save money),  Number of instances(choose 3)  

Security and access   

modify the PEM file so that it can allow us to log in directly to the master node of the running cluster
chmod og-rwx keypairFile.pem

click "Create cluster"  

then click "security groups for master", 
then tick "ElasticMapReduce-master"   
then edit Inbound, add Rule, then add the rule "ssh     TCP       22       My IP"
also add the rule "Custom TCP Rule     TCP       8890       My IP", the save

go back to EMR console, if the state of the cluster is "waiting", then it means it's ready 

we can use "Master public DNS:8890" in the browser to see the Zeppelin page    

then create a new note
choose spark(pyspark) as the interpreter


```

```python
%pyspark       # then all the code will be python Or the code should be written in Scala   
df = spark.read.csv("s3n://Myaccesskey:secretKey@bucketname/myfile.csv")

spark      # the spark session object    

sc         # spark context object

```



Python and Spark



## install pySpark

```sh
install Java  
install anaconda   

download spark-2.3.2-bin-hadoop2.7.tgz
tar -zxvf spark-2.3.2-bin-hadoop2.7.tgz   
cd spark-2.3.2-bin-hadoop2.7

sudo vi ~/.bash_profile, then add the following lines:   
export SPARK_PATH=/Desktop/installedPrograms/spark-2.3.2-bin-hadoop2.7   
export PYSPARK_DRIVER_PYTHON="jupyter"    
export PYSPARK_DRIVER_PYTHON_OPTS="notebook"   

alias snotebook='$SPARK_PATH/bin/pyspark --master local[2]'      

#then
source .bash_profile



```

## RDD   

RDD = resilient distributed dataset(a collection of elements partitioned across the nodes of the cluster that can be operated on in parallel)   

There are two ways to create RDDs: parallelizing an existing collection in driver program, or referencing a dataset in an external storage system, such as a shared filesystem, HDFS, HBase, or any data source offering a Hadoop InputFormat


## cmd   

```sh
jupyter notebook   
# then we can have the 'jupyter notebook' open in the browser    



```


## hadoop   


## anaconda   


