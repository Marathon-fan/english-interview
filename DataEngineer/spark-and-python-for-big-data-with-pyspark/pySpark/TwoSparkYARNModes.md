
# pySpark -- Python and Spark   


The following shows how you can run spark-shell in client mode:
```sh
$ ./bin/spark-shell \
    --master yarn   \
    --deploy-mode client
```

when to use client mode
```
The client mode is deployed with the Spark shell program, which offers an interactive Scala console. Use this mode when you want to run a query in real time and analyze online data.
```
```
run a query in real time and analyze online data.
```


The following shows how you can run spark-submit in cluster mode:
```sh
$ ./bin/spark-submit --class org.apache.spark.examples.SparkPi \
    --master yarn \
    --deploy-mode cluster \
    --driver-memory 4g \
    --executor-memory 2g \
    --executor-cores 1 \
    --queue thequeue \
    examples/jars/spark-examples*.jar \
    10
```    

when to use cluster mode
```
When submitting an application from a host that is far from the compute hosts (worker nodes); for example, locally from your laptop, use this mode to minimize network latency between the Drivers and the Executors.
```



Spark-submit example   
```sh
spark-submit --class wordcount.WordCount \
    --master yarn \
    sparkdemo_2.10-1.0.jar \
    prod /public/randomtextwriter/part-m-0000 /user/dgadiraju/wordcount 

hadoop fs -ls /public/randomtextwriter/part-m-0000

hadoop fs -ls /user/dgadiraju/wordcount 



```

################ 




