

```python
from pyspark.sql import SparkSession
sparkSession = SparkSession.builder.appName("johnSpark").getOrCreate() 
orcFile = sparkSession.read.option("inferSchema", True).orc("file:///pyspark/part-r-00000-a5cd888d-37af-4d95-b21f-9c8fc3c0b8d0.orc")
orcFile.printSchema()
orcFile.show()

# python
orcFile.select("contractorid", "supervisorcontact").show()
from pyspark.sql.functions import when 
orcFile = orcFile.withColumn("contractorcomments", when(orcFile.supervisorcontact.isNotNull(), "random contractorcomments").otherwise("random contractorcomments"));
orcFile = orcFile.withColumn("homephone", when(orcFile.supervisorcontact.isNotNull(), "0400000000").otherwise("0400000000"));
orcFile = orcFile.withColumn("district", when(orcFile.supervisorcontact.isNotNull(), "random district").otherwise("random district"));
orcFile = orcFile.withColumn("longitude", when(orcFile.supervisorcontact.isNotNull(), "0.00").otherwise("0.00"));
orcFile = orcFile.withColumn("contractornumber", when(orcFile.supervisorcontact.isNotNull(), "0400000000").otherwise("0400000000"));
orcFile = orcFile.withColumn("contactnumber", when(orcFile.supervisorcontact.isNotNull(), "0400000000").otherwise("0400000000"));
orcFile = orcFile.withColumn("contractoraddress", when(orcFile.supervisorcontact.isNotNull(), "random addr").otherwise("random addr"));
orcFile = orcFile.withColumn("id", when(orcFile.supervisorcontact.isNotNull(), "random id").otherwise("random id"));
orcFile = orcFile.withColumn("contractorname", when(orcFile.supervisorcontact.isNotNull(), "0400000000").otherwise("0400000000"));
orcFile = orcFile.withColumn("address", when(orcFile.supervisorcontact.isNotNull(), "random addr").otherwise("random addr"));
orcFile = orcFile.withColumn("name", when(orcFile.supervisorcontact.isNotNull(), "random name").otherwise("random name"));
orcFile = orcFile.withColumn("subdistrict", when(orcFile.supervisorcontact.isNotNull(), "random subdistrict").otherwise("random subdistrict"));
orcFile = orcFile.withColumn("guarenteedsubcontractor", when(orcFile.supervisorcontact.isNotNull(), "0400000000").otherwise("0400000000"));
orcFile = orcFile.withColumn("latitude", when(orcFile.supervisorcontact.isNotNull(), "0.00").otherwise("0.00"));
orcFile = orcFile.withColumn("supervisorname", when(orcFile.supervisorcontact.isNotNull(), "anonymous").otherwise("anonymous"));
orcFile = orcFile.withColumn("region", when(orcFile.supervisorcontact.isNotNull(), "random region").otherwise("random region"));
orcFile = orcFile.withColumn("contractorid", when(orcFile.supervisorcontact.isNotNull(), "0400000000").otherwise("0400000000"));
orcFile = orcFile.withColumn("supervisorcontact", when(orcFile.supervisorcontact.isNotNull(), "0400000000").otherwise("0400000000"));

orcFile.select("contractorcomments", "homephone","district","longitude","contractornumber","contactnumber","contractoraddress","id","contractorname","address","name", "subdistrict", "guarenteedsubcontractor", "latitude", "supervisorname","region", "contractorid", "supervisorcontact", ).show()

orcFile.write.orc("orcFolder");
orcFile.write.csv("csvFolder");


# python
cols = orcFile.columns

# scala
orcFile.select($"addresspostcode", ).show()

df.select($"name", $"age" + 1).show()

```

connect_task_stg   
```python
# start pyspark

$> sudo pyspark

from pyspark.sql import SparkSession
sparkSession = SparkSession.builder.appName("johnSpark").getOrCreate() 

// orc
orcFile = sparkSession.read.option("inferSchema", True).orc("file:///pyspark/part-r-00000-e0a87e0d-183d-4664-951c-0ca69db0b27d.orc")
orcFile.printSchema()
orcFile.show()

// csv
csvFile = sparkSession.read.format("csv").option("header", "true").option("mode", "DROPMALFORMED").load("file:///pyspark/taf0001_345919_20161031.csv")
csvFile.printSchema()
csvFile.show()
csvFile.select("esa_planner",\
                "adsl_capacity_planner").show()


# python
from pyspark.sql.functions import when 


district
customerid
longitude
w6key
subdistrict
latitude
hostordernumber
region
customer
callid
custaddress
crn
fnn
prod_skill_code
hostuserid
postcode
addresspostcode

// csv
csvFile = csvFile.withColumn("esa_planner", when(csvFile.esa_planner.isNotNull(), "random esa_planner").otherwise("random esa_planner"));
csvFile = csvFile.withColumn("adsl_capacity_planner", when(csvFile.adsl_capacity_planner.isNotNull(), "random adsl_capacity_planner").otherwise("random adsl_capacity_planner"));

// orc

orcFile = orcFile.withColumn("district", when(orcFile.district.isNotNull(), "random district").otherwise("random district"));
orcFile = orcFile.withColumn("customerid", when(orcFile.customerid.isNotNull(), "random customerid").otherwise("random customerid"));
orcFile = orcFile.withColumn("longitude", when(orcFile.longitude.isNotNull(), "0.00").otherwise("0.00"));
orcFile = orcFile.withColumn("w6key", when(orcFile.w6key.isNotNull(), "random w6key").otherwise("random w6key"));
orcFile = orcFile.withColumn("subdistrict", when(orcFile.subdistrict.isNotNull(), "random subdistrict").otherwise("random subdistrict"));
orcFile = orcFile.withColumn("latitude", when(orcFile.latitude.isNotNull(), "0.00").otherwise("0.00"));
orcFile = orcFile.withColumn("hostordernumber", when(orcFile.hostordernumber.isNotNull(), "random hostordernumber").otherwise("random hostordernumber"));
orcFile = orcFile.withColumn("region", when(orcFile.region.isNotNull(), "random region").otherwise("random region"));
orcFile = orcFile.withColumn("customer", when(orcFile.customer.isNotNull(), "random customer").otherwise("random customer"));
orcFile = orcFile.withColumn("callid", when(orcFile.callid.isNotNull(), "random callid").otherwise("random callid"));
orcFile = orcFile.withColumn("custaddress", when(orcFile.custaddress.isNotNull(), "random custaddress").otherwise("random custaddress"));
orcFile = orcFile.withColumn("crn", when(orcFile.crn.isNotNull(), 999).otherwise(999));
orcFile = orcFile.withColumn("fnn", when(orcFile.fnn.isNotNull(), "random fnn").otherwise("random fnn"));
orcFile = orcFile.withColumn("prod_skill_code", when(orcFile.prod_skill_code.isNotNull(), 999).otherwise(999));
orcFile = orcFile.withColumn("hostuserid", when(orcFile.hostuserid.isNotNull(), "random hostuserid").otherwise("random hostuserid"));
orcFile = orcFile.withColumn("postcode", when(orcFile.postcode.isNotNull(), "random postcode").otherwise("random postcode"));
orcFile = orcFile.withColumn("addresspostcode", when(orcFile.addresspostcode.isNotNull(), "random addresspostcode").otherwise("random addresspostcode"));

orcFile.select("district",\
                "customerid",\
                "longitude",\
                "w6key",\
                "subdistrict",\
                "latitude",\
                "hostordernumber",\
                "region",\
                "customer",\
                "callid",\
                "custaddress",\
                "crn",\
                "fnn",\
                "prod_skill_code",\
                "postcode",\
                "addresspostcode").show()


orcFile.select("confirmationdate",\
               "contactdate").show()

csvFile.write.csv("csvFolder");
csvFile.write.csv("orcFolder");

orcFile.write.orc("orcFolder");
orcFile.write.csv("csvFolder");



# python
cols = orcFile.columns

# scala
orcFile.select($"addresspostcode", ).show()

df.select($"name", $"age" + 1).show()

```


```sh
sudo cp -r csvFolder /vagrant/src/test/resources/sources/sampledata/r/nonsp/isdw/connect_task_stg/
sudo cp -r orcFolder /vagrant/src/test/resources/sources/sampledata/r/nonsp/isdw/connect_task_stg/
```

 |-- tasktype: string (nullable = true)
 |-- postcode: string (nullable = true)
 |-- fasttrack: long (nullable = true)
 |-- subproject: string (nullable = true)
 |-- projectno: string (nullable = true)
 |-- esacode: string (nullable = true)
 |-- addresspostcode: string (nullable = true)
 |-- src_date: date (nullable = true)



**load local files to pySpark**
```
You can't load local file unless you have same file in all workers under same path. For example if you want to read data.csv file in spark, copy this file to all workers under same path(say /tmp/data.csv). Now you can use sc.textFile("file:///tmp/data.csv") to create RDD.

Current working directory is the folder from where you have started pyspark. You can start pyspark using ipython and run pwd command to check working directory. [Set PYSPARK_DRIVER_PYTHON=/path/to/ipython in spark-env.sh to use ipython

```

**sparkContext vs SparkSession**
```
sparkContext was used as a channel to access all spark functionality. ... SparkSession provides a single point of entry to interact with underlying Spark functionality and allows programming Spark with Dataframe and Dataset APIs. All the functionality available with sparkContext are also available in sparkSession.
```



What does the 'Z' stand for in timestamps?
```

What does the 'Z' stand for in timestamps?
Ask Question
up vote
9
down vote
favorite
Whenever I see the time of a question on the interface, something like this I see the time shared as 2017-02-11 12:46:00Z which is the current time. From calculations it seems the time is in UTC but then what does 'Z' stand for here? Is there a special meaning to Z, some term?

"Zulu", the military time zone which is identical to UTC.

From Wikipedia:

The time zone using UTC is sometimes denoted UTC�00:00 or by the letter Z�a reference to the equivalent nautical time zone (GMT), which has been denoted by a Z since about 1950. Time zones were identified by successive letters of the alphabet and the Greenwich time zone was marked by a Z as it was the point of origin. The letter also refers to the "zone description" of zero hours, which has been used since 1920 (see time zone history). Since the NATO phonetic alphabet word for Z is "Zulu", UTC is sometimes known as "Zulu time". This is especially true in aviation, where "Zulu" is the universal standard. This ensures all pilots regardless of location are using the same 24-hour clock, thus avoiding confusion when flying between time zones. See the list of military time zones for letters used in addition to Z in qualifying time zones other than Greenwich.
```

timestamp
```
https://help.sumologic.com/03Send-Data/Sources/04Reference-Information-for-Sources/Timestamps%2C-Time-Zones%2C-Time-Ranges%2C-and-Date-Formats
```


Avoid writing carriage return when writing line feed with Python
You can do this by passing '' to the newline parameter when opening the text file.
```
# in python3
f = open('test.txt', 'w', newline='')
f.write('Only LF\n')
f.write('CR + LF\r\n')
f.write('Only CR\r')
f.write('Nothing')
f.close()
```