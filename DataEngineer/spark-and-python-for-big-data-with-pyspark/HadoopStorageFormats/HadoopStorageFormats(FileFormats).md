
**to Read**
```
https://spark.apache.org/docs/2.2.0/sql-programming-guide.html
```

**MapReduce, Spark, and Hive are three primary ways that you will interact with files stored on Hadoop.**

```
tab delimited files                              the default format of Hive
Plain text storage (eg, CSV, TSV files)          
Sequence Files                                   
Avro                                             
Parquet                                          
```


In MapReduce file format support is provided by the InputFormat and OutputFormat classes. Here is an example configuration for a simple MapReduce job that reads and writes to text files:

```
Job job = new Job(getConf());
...
job.setInputFormatClass(TextInputFormat.class);
job.setOutputFormatClass(TextOutputFormat.class);
```

Both Hive and Spark have similar mechanisms for reading and writing custom file formats which wrap the Hadoop InputFormat described above, so the InputFormat is truly the gateway to file formats on Hadoop.

**Different storage formats are set up to provide different types of data to their consumers.**

For example the `TextInputFormat` gives you a string that represents a single line of the file it reads, whereas the `AVRO` file format is designed to provide you with structured data that can be deserialized to a java object.


To illustrate, take a look at this example of a MapReduce map class from the AVRO documentation ( I simplified it a little ). Notice how the Map task transparently receives an instance of the `User` class?
```java
 public static class ColorCountMapper extends Mapper<AvroKey<User>, NullWritable, Text, IntWritable> {

    @Override
    public void map(AvroKey<User> key, NullWritable value, Context context) {

      CharSequence color = key.datum().getFavoriteColor();
      if (color == null) {
        color = "none";
      }

      context.write(new Text(color.toString()), new IntWritable(1));
    }
  }
```


Contrast that with how you’d accomplish it with a vanilla text input workflow:
```java
  public static class ColorCountMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    @Override
    public void map(LongWritable key, Text value, Context context) {

      String[] user = value.getString().split("\\n");

      String color = "none";

      if(user.length >= 2) {
        color = user[1];
      }
      context.write(new Text(color), new IntWritable(1));
    }
  }
```

**TEXT FILES (E.G. CSV, TSV)**

Simple text-based files are common in the non-Hadoop world, and they’re super common in the Hadoop world too. Data is laid out in lines, with each line being a record. Lines are terminated by a newline character \n in the typical unix fashion.

Text-files are inherently splittable (just split on \n characters!), but if you want to compress them you’ll have to use a file-level compression codec that support splitting, such as BZIP2

Because these files are just text files you can encode anything you like in a line of the file. One common example is to make each line a JSON document to add some structure. While this can waste space with needless column headers, it is a simple way to start using structured data in HDFS.

**SEQUENCE FILES**   
```
Sequence files were originally designed for MapReduce, so the integration is smooth. They encode a key and a value for each record and nothing more. Records are stored in a binary format that is smaller than a text-based format would be. Like text files, the format does not encode the structure of the keys and values, so if you make schema migrations they must be additive.
```

**AVRO FILES**   
```
Avro is an opinionated format which understands that data stored in HDFS is usually not a simple key/value combo like Int/String. The format encodes the schema of its contents directly in the file which allows you to store complex objects natively.

Honestly, Avro is not really a file format, it’s a file format plus a serialization and deserialization framework. With regular old sequence files you can store complex objects but you have to manage the process. Avro handles this complexity whilst providing other tools to help manage data over time.

Avro is a well thought out format which defines file data schemas in JSON (for interoperability), allows for schema evolutions (remove a column, add a column), and multiple serialization/deserialization use cases. It also supports block-level compression. For most Hadoop-based use cases Avro is a really good choice.
```

**COLUMNAR FILE FORMATS (PARQUET, RCFILE)**

```
The latest hotness in file formats for Hadoop is columnar file storage. Basically this means that instead of just storing rows of data adjacent to one another you also store column values adjacent to each other. So datasets are partitioned both horizontally and vertically. This is particularly useful if your data processing framework just needs access to a subset of data that is stored on disk as it can access all values of a single column very quickly without reading whole records.

One huge benefit of columnar oriented file formats is that data in the same column tends to be compressed together which can yield some massive storage optimizations (as data in the same column tends to be similar).

If you’re chopping and cutting up datasets regularly then these formats can be very beneficial to the speed of your application, but frankly if you have an application that usually needs entire rows of data then the columnar formats may actually be a detriment to performance due to the increased network activity required.

Overall these formats can drastically optimize workloads, especially for Hive and Spark which tend to just read segments of records rather than the whole thing (which is more common in MapReduce).
```

**Compression Codecs**

there are two ways you can compress data in Hadoop.
```
File-Level Compression
Block-Level Compression
```

File-level compression means you compress entire files regardless of the file format, the same way you would compress a file in Linux. Some of these formats are splittable (e.g. bzip2, or LZO if indexed).

So you’d end up with a file called user-data.csv.gzip for example.

Block-level compression is internal to the file format, so individual blocks of data within the file are compressed. This means that the file remains splittable even if you use a non-splittable compression codec like Snappy. However, this is only an option if the specific file format supports it.

So you’d still have a file called user-data.sequence, which would include a header noting the compression codec needed to read the remaining file contents.

Data Compression Supported by Hadoop
```
Gzip
BZIP2
LZO
LZ4
Snappy
```

Z File Format
```
Unix file Compressed

Z is a file extension for a compressed archive file used with Unix-based systems. Z file archives were commonly used when creating a compressed archive to magnetic tape for backup purposes. Z files can be opened by most current compression software, including Winzip (Windows) and Tar (Unix/Linux).
```

```
CSV        Comma Separated Values 
ORC        Optimized Row Compressed 
TSV        Tab Separated Value 
text Format
XML        Extensible Markup Language
JSON       JavaScript Object Notation 
ARFF       Attribute-Relation File Format
Sequence File Format
Zip file Format 
SQL Database Table


```


Spark DataFrame features
```
Scalability – Allows processing Petabytes of data at once.
Flexibility – Supports a broad array of data formats (csv, elasticsearch, Avro, etc.) and storage systems (HDFS, Hive tables, etc.)
Custom Memory Management – Data is stored off-heap in binary format that saves memory and removes garbage collection. Also, Java serialization is avoided as the schema is already known.
Optimized Execution Plans – Spark catalyst optimizer executes the query plans and executes the queries on RDDs.
```


**Spark Data Source**
https://spark.apache.org/docs/latest/sql-data-sources.html


For the jdbc source of tdcprdc_base_isdw.connect_task_stg and tdcprdc_base_isdw.connect_ppl_stg
the databasename is tdcprdc_base_isdw, as specified by SPD.xls
and the table names are connect_task_stg and connect_ppl_stg respectively

Then how to find the files that are used by jdbc_spark_properties in properties.json? 

for example, in main/sources/headspin/json, we can see 
```
                "sqlFile": "/oanapps/headspin/oozie/spark/distributed-cache/file/sp_dp_i_headspin_commbank_tests.sql"
```
but I cannot see the file in OA, or in HDFS. It just says: No such file or directory
The sql files it specifies are not those files in main/sources/headspin/sql.
So how to find those files？ Or do we need to write those sql files and store them somewhere?
