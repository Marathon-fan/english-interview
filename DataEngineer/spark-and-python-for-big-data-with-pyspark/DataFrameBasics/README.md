


# DataFrame Basics   

https://www.udemy.com/spark-and-python-for-big-data-with-pyspark/learn/v4/t/lecture/6674914?start=0


Spark DataFrame Basics1
```python
from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('Basics').getOrCreate()
df = spark.read.json('people.json')
df.show()

df.printSchema()

df.columns

df.describe()

df.describe().show()

from pyspark.sql.types import (StructField,StringType,
	                           IntegerType,StructType)


data_schema = [StructField('age', IntegerType(), True),
               StructField('name', StringType(), True)]
# True means that the filed can be null  

final_struc = StructType(fields = data_schema)

df = spark.read.json('people.json', schema = final_struc)

df.printSchema()


df['age']

type(df['age'])

df.select('age')

df.select('age').show()

type(df.select('age'))  # we will get DataFrame type

df.head(2)[0]

type(df.head(2)[0])  # we get pyspark.sql.types.Row

df.select(['age', 'name']).show()

df.withColumn('newage', df['age']).show()

df.withColumn('double_age', df['age'] * 2).show()

df.withColumnRename('age', 'my_new_age')

df.createOrReplaceTempView('people')

# spark SQL example
results = spark.sql("SELECT * FROM people")

results.show()

new_results = spark.sql("SELECT * FROM people WHERE age=30")

new_results.show()

```

Spark DataFrame Basics2
```python

from pyspark.sql import SparkSession
spark = SparkSession.builder.appName('ops').getOrCreate()
df = spark.read.csv('appl_stock.csv', inferSchema=True, header=True)
df.printSchema()
df.show()
df.head(3)[0]
df.filter("Close < 500").show()
df.filter("Close < 500").select(['Open', 'Close']).show()
df.filter(df['Close'] < 500).select('Volume').show()
df.filter( (df['Close'] < 200) & (df['Open'] > 200)).show()
df.filter( (df['Close'] < 200) & ~(df['Open'] > 200)).show()
df.filter(df['Low'] == 197.16).show()
result = df.filter(df['Low'] == 197.16).collect()
row = result[0]
result
row.asDict()
row.asDict()['Volume']


```

Groupby and Aggregate Operations
```python
from pyspark.sql import SparkSession
spark = SparkSession.builder.appName('aggs').getOrCreate()
df = spark.read.csv('sales_info.csv', inferSchema = True, header = True)
df.show()
df.printSchema()
df.groupBy("Company")
df.groupBy('Company').mean().show()
df.groupBy('Company').sum().show()
df.groupBy('Company').max().show()
df.groupBy('Company').min().show()
df.groupBy('Company').count().show()
df.agg({'Sales':'sum'}).show()    # we pass the column and the function
df.agg({'Sales':'max'}).show()   
group_data = df.groupBy("Company")
group_data.agg({'Sales':'max'}).show()

# import functions from Spark
from pyspark.sql.functions import countDistinct, avg, stddev
df.select(countDistinct('Sales')).show()
df.select(avg('Sales')).show()
df.select(avg('Sales').alias('Average Sales')).show()
df.select(stddev('Sales')).show()
from pyspark.sql.functions import format_number
sales_std = df.select(stddev("Sales").alias('std'))
sales_std.select(format_number('std', 2).alias('std')).show() # only show 2 decimals

# sort and order things
df.orderBy("Sales").show()
df.orderBy(df['Sales'].desc()).show()


```

missing data
```python
from pyspark.sql import SparkSession
spark = SparkSession.builder.appNmae('miss').getOrCreate()
df = spark.read.csv('ContainsNull.csv', header = True, inferSchema = True)
df.show()

# dropping the missing data
df.na.drop().show()   # drop any row that contains null data
df.na.drop(thresh = 2).show()   # drop any row that contains at least 2 null values
df.na.drop(how = 'all').show()   # drop the row with all fields are null
df.na.drop(subset = ['Sales']).show()   # drop the row with all null Sales field

# fill value
df.na.fill('FILL VALUE').show()   # fill string columns that are null with 'FILL VALUE'
df.na.fill(0).show()   # fill numberic columns that are null with 0
df.na.fill('No Name', subset=['Name']).show()   # fill the Name column that are null with 'No Name'

from pyspark.sql.functions import mean
mean_val = df.select(mean(df['Sales'])).collect()
mean_sales = mean_val[0][0]
df.na.fill(mean_sales, ['Sales']).show()
df.na.fill(df.select(mean(df['Sales'])).collect()[0][0], ['Sales']).show()

```

Dates and Timestamps
```python
from pyspark.sql import SparkSession
spark = SparkSession.builder.appName('dates').getOrCreate()
df = spark.read.csv('appl_stock.csv', header = True, inferSchema = True)
df.show()
df.select(['Date', 'Open']).show()

from pyspark.sql.functions import (dayofmonth, hour, 
	                               dayofyear, month, 
	                               year, weekofyear, 
	                               format_number, date_format)
df.select(dayofmonth(df['Date'])).show()
df.select(hour(df['Date'])).show()
df.select(month(df['Date'])).show()

df.select(year(df['Date'])).show()
newdf = df.withColumn("Year", year(df['Date']))
result = newdf.groupBy("Year").mean().select(["Year", "avg(Close)"])
result.show()
new_result = result.withColumnRenamed("ave(Close)", "Average Closing Price")
new_result.show()
new_result.select(['Year', format_number('Average Closing Price', 2)])
new_result.select(['Year', format_number('Average Closing Price', 2).alias("Avg Close")])


```

