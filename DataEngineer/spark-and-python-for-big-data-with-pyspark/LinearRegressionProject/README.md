


# pySpark -- Python and Spark   

https://www.udemy.com/spark-and-python-for-big-data-with-pyspark/learn/v4/content


# Linear Regression Theory  

![Linear Regression Theory](./pics/linearRegression.jpg)

**The lalels in Linear Regression are continuous** 

# Linear Regression Consulting Project     

## creating the models   

How many crew members a ship will require    

```sh
features

1 ship Name
2 Cruise Line
3 Age(as of 2013)
4 Tonnage (1000s of tons)
5 passengers (100s)
6 Length (100s of feet)
7 Cabins (100s)
8 Passenger Density
9 Crew (100s)

```

StringIndexer --- convert strings to numbers   




## scripts   

```sh

aws s3 ls s3://emr-data-sydney
mkdir EMRData
sudo aws s3 cp s3://emr-data-sydney/cruise_ship_info.csv /home/ec2-user/EMRData/cruise_ship_info.csv 

```




```python

import findspark
findspark.init('/home/ec2-user/spark-2.1.1-bin-hadoop2.7')
import pyspark

import os
cwd = os.getcwd()
print(cwd)

from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('cruise').getOrCreate()


df = spark.read.csv('EMRData/cruise_ship_info.csv', inferSchema=True, header=True)
df.printSchema()


for ship in df.head(5):
	print(ship)
	print('\n')

df.groupBy('Cruise_line').count().show()

# use String indexer
from pyspark.ml.feature import StringIndexer
indexer = StringIndexer(inputCol = 'Cruise_line', outputCol = 'cruise_cat')
indexed = indexer.fit(df).transform(df)
indexed.head(3)

from pyspark.ml.linalg import Vectors
from pyspark.ml.feature import VectorAssembler

indexed.columns

assembler = VectorAssembler(inputCols=[
 'Age',
 'Tonnage',
 'passengers',
 'length',
 'cabins',
 'passenger_density',
 'cruise_cat'], outputCol = 'features')

output = assembler.transform(indexed)
output.select('features', 'crew').show()    # the label column here is just crew   

final_data = output.select(['features', 'crew'])   

train_data, test_data = final_data.randomSplit([0.7, 0.3])
train_data.describe().show()
test_data.describe().show()

# build linear regression model
from pyspark.ml.regression import LinearRegression
ship_lr = LinearRegression(labelCol='crew')   # this is the linear regression model
trained_ship_model = ship_lr.fit(train_data)
ship_results = trained_ship_model.evaluate(test_data)
ship_results.rootMeanSquaredError

train_data = mean.describe().show()
# rootMeanSquaredError 0.67
# train_data   mean = 7.7  stddev = 3.65     count = 103    means that the result is pretty good

ship_results.r2

ship_results.meanSquaredError

ship_results.meanAbsoluteError

from pyspark.sql.functions import corr  # co-relation
df.select(corr('crew', 'passengers')).show()
# if crew is highly correlated to passengers, then the model is good 
# the result is 0.915  (highly correlated )

df.select(corr('crew', 'cabins')).show()  
# if crew is highly correlated to cabins, then the model is good 
# the result is 0.950  (highly correlated )

```


