


```sh
hadoop fs -ls
hadoop fs -mkdir oozie
hadoop fs -mkdir oozie/map-reduce
hadoop fs -put /pathXXX/Oozie/Workflows/MapReduce/* oozie/map-reduce
hadoop fs -ls oozie/map-reduce  
# then we see 1) input-data 2) job.properties 3) lib 4) workflow.xml
hadoop fs -ls oozie/map-reduce/lib    # see the jar file
hadoop fs -ls oozie/map-reduce/input-data  # see the data file

oozie job -oozie http://localhost:11000/oozie -config /pathXXX/Oozie/Workflows/MapReduce/job.properties -run
# job.properties is in our local machine   
oozie job -oozie http://localhost:11000/oozie -info jobID
# inspect the job
oozie job -oozie http://localhost:11000/oozie -log jobID

hadoop fs -ls ozie/map-reduce
# then we can see the output
hadoop fs -ls oozie/map-reduce/output-data
hadoop fs -ls oozie/map-reduce/output-data/part-00000

# navigate to localhost:8088/cluster to see the result on UI

```