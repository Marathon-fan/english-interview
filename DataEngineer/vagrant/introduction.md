
Gradle allows "key - value" pairs in a 'gradle.properties' file. Available to scripts in the settings.gradle & buuild.gradle files

```sh
gradle.properties in project root directory

gradle.properties in GRADLE_USER_HOME directory

system properties, e.g. when -Dgradle.user.home is set on the command line
```



