
# Introduction to 'Plugins'    


**A plugin is nothing but a set of tasks**, almost all useful tasks such as compiling tasks, setting domain objects, setting up source files, etc. are handled by plugins. Applying a plugin to a project, allows the plugin to extend the project’s capabilities. Plugins can −

```
Extend the basic Gradle model (e.g. add new DSL elements that can be configured).
Configure the project according to conversions (e.g. add new tasks or configure sensible defaults).
Apply specific configuration (e.g. add organizational repositories or enforce standards).
```

**Types of Plugins**  
There are two types of plugins in Gradle, script plugins and binary plugins.   
`Script plugins` is an additional build script that gives a declarative approach to manipulating the build. This is typically used within a build. Binary plugins are the classes that implement the plugin interface and adopt a programmatic approach to manipulating the build.   
`Binary plugins` can reside with a build script, with the project hierarchy or externally in a plugin JAR.

plugin can extend Gradle model   

```
java
java-library
other plugins
```

build.gradle
```
plugins {
	id 'java'
}
```

# Applying 'Plugins'   

DSL: domain specific language    

conventions: unwritten rules   


