

# Introduction to the Gradle Object Model

Gradle Interfaces

```
Script <Interface>

Project <Interface>

Gradle <Interface>

Settings <Interface>

Task <Interface>

Action <Interface>
```

build.gradle
```
logger.info "Hello!"
apply {
	println "Hello again!!"
}
```


# the Gradle Lifecycle   

```
1 Initialization        ---    init.gradle, setting.gradle   
2 Configuration         ---    build.gradle 
3 Execution             ---    build.gradle 
```

# the Gradle Initialization Phase  

phase 1 Initialization   
```
init.gradle + <XXX>.gradle


settings.gradle

```

**.gradle/init.d**

another.gradle
```java
import java.text.*
gradle.ext.timestamp = {
	def df = new SimpleDataFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
	df.setTimeZone(TimeZone.getTimeZone("UTC"))
	return df.format(new Date())
}

println "another.gradle --> This is executed during the initialization phase- timestamp @ ${gradle.timestamp()}"

```


phase 2 Configuration
```
build.gradle
```

phase 3 Execution
```
build.gradle
```

# Gradle Object Model   









