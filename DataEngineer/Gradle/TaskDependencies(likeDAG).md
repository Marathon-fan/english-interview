
**define the dependencies**
Almost all the projects are not self-contained. They need files build by other projects to compile and test the source files. For example, in order to use Hibernate in the project, you need to include some Hibernate JARs in the classpath. Gradle uses some special script to define the dependencies, which needs to be downloaded.


**publication**
Gradle takes care of building and publishing the outcomes somewhere. Publishing is based on the task that you define. You might want to copy the files to the local directory, or upload them to a remote Maven or lvy repository, or you might use the files from another project in the same multi-project build. The process of publishing is called as publication.

**Declaring Your Dependencies**
Gradle follows some special syntax to define dependencies. The following script defines two dependencies, one is Hibernate core 3.6.7 and second one is Junit with the version 4.0 and later. Take a look at the following code. Use this code in build.gradle file.

```
apply plugin: 'java'

repositories {
   mavenCentral()
}

dependencies {
   compile group: 'org.hibernate', name: 'hibernate-core', version: '3.6.7.Final'
   testCompile group: 'junit', name: 'junit', version: '4.+'
}
```

**Dependency Configurations**  

Dependency configuration is nothing but defines a set of dependencies. You can use this feature to declare external dependencies, which you want to download from the web. This defines the following different standard configurations.
```
Compile − The dependencies required to compile the production source of the project.

Runtime − The dependencies required by the production classes at runtime. By default, also includes the compile time dependencies.

Test Compile − The dependencies required to compile the test source of the project. By default, it includes compiled production classes and the compile time dependencies.

Test Runtime − The dependencies required to run the tests. By default, it includes runtime and test compile dependencies.
```


**Publishing Artifacts**  
These published files are called **artifacts**. 
Dependency configurations are also used to publish files. These published files are called artifacts. Usually, we use plug-ins to define artifacts. However, you do need to tell Gradle where to publish the artifacts. You can achieve this by attaching repositories to the upload archives task. Take a look at the following syntax for publishing Maven repository. While executing, Gradle will generate and upload a Pom.xml as per the project requirements. Use this code in build.gradle file.
```
apply plugin: 'maven'

uploadArchives {
   repositories {
      mavenDeployer {
         repository(url: "file://localhost/tmp/myRepo/")
      }
   }
}
```


How to 'Hook' into the Task Dependency Graph


```SH

...

project.gradle.taskGraph.whenReady { taskGraph ->
	if (taskGraph.hasTask(doStep2)) {
		project.version = '1.0'
	} else {
		project.version = '1.0-SNAPSHOT'
	}
}

project.gradle.taskGraph.beforeTask { task ->
    logger.info "+++ Before task $task.name"
}

project.gradle.taskGraph.afterTask { task ->
    logger.info "+++ After task $task.name"
}


```