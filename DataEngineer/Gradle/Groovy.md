

https://www.udemy.com/gradle-masterclass/learn/v4/t/lecture/11724110?start=0


# Starting a Gradle Build Script

build.gradle
```java
println "Hello, world!"

System.out.println "Hello, world" // 100% percent compatible with Java

import java.text.*

SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")

sdf.setTimeZone(TimeZone.getTimeZone("UTC"))

System.out.println("Hello World now!" + sdf.format(new Date))

println "Hello World now!" + sdf.format(new Date)

sayHelloWorld()

void sayHelloWorld() {
	println "hello world!"
}

sayHelloWorld()
```



