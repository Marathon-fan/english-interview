

everytime this app is renewed, also renew the following address:  
https://bitbucket.org/Marathon-fan/neo4j_demo/src/master/

# introduction

A simple app using Neo4j that can peron, location and the relations(like a is b's friend or a was born in b location) among them.

# files strucutre
```sh
app.js         # routes and restful api

/views
    index.ejs  # main page
    person.ejs # person page
    /partials
        footer.ejs  # common footer
        header.ejs  # common header
```


# how to run it   

**step 1 run neo4j through docker**  

```sh
docker run \
    --publish=7474:7474 --publish=7687:7687 \
    --volume=$HOME/neo4j/data:/data \
    --env=NEO4J_ACCEPT_LICENSE_AGREEMENT=yes \
    --env=NEO4J_AUTH=none \
    neo4j:3.3.7
```


**step 2 install node dependencies**

```sh
npm install
```

**step 3 run the app**     

```
node app
```

navigate to the address http://localhost:3000/
(you can also navigate into neo4j browswer to see the graph nodes(localhost:7474))


# restful APIs

```sh
GET /                       # the main page

POST /person

POST /location

POST /friends/connect

POST /person/born

GET /person/:id
```

# demo pics  

graph-data1  
![graph-data1](./pics/graph-data1.jpg)

graph-data2(in table)  
![graph-data2](./pics/graph-data2.jpg)

simpleUI   
![simpleUI](./pics/simpleUI.jpg)


==============================================================================================

2 setting up the website dependencies   

add the following dependencies:
```js
  "dependencies": {
    "body-parser":"*",           //
    "cookie-parser":"*",         //
    "express":"*",               // web framework, routes, url, etc
    "ejs":"^2.4.2",              // template engine for html, dynamic variables
    "morgan":"*"                 // logger
  },
```



npm install

3 setting up the neo4j   

```sh
npm install neo4j-driver
```




==============================================================================================

```
:play movie graph

click >

then we can see the scripts in dbscripts/readme.md

then click >(play)

and we see the graphDB in the graph form

```

run the following command
```sql
# Find the actor named "Tom Hanks"...

MATCH (tom {name: "Tom Hanks"}) RETURN tom


# Find the movie with title "Cloud Atlas"...

MATCH (cloudAtlas {title: "Cloud Atlas"}) RETURN cloudAtlas

# Find 10 people...
MATCH (people:Person) RETURN people.name LIMIT 10

MATCH (people:Person) RETURN people LIMIT 10


# Find movies released in the 1990s...
MATCH (nineties:Movie) WHERE nineties.released >= 1990 AND nineties.released < 2000 RETURN nineties.title
MATCH (nineties:Movie) WHERE nineties.released >= 1990 AND nineties.released < 2000 RETURN nineties

# List all Tom Hanks movies...
MATCH (tom:Person {name: "Tom Hanks"})-[:ACTED_IN]->(tomHanksMovies) RETURN tom,tomHanksMovies

# Who directed "Cloud Atlas"?
MATCH (cloudAtlas {title: "Cloud Atlas"})<-[:DIRECTED]-(directors) RETURN directors.name

# Tom Hanks' co-actors...
MATCH (tom:Person {name:"Tom Hanks"})-[:ACTED_IN]->(m)<-[:ACTED_IN]-(coActors) RETURN coActors.name

# How people are related to "Cloud Atlas"...
MATCH (people:Person)-[relatedTo]-(:Movie {title: "Cloud Atlas"}) RETURN people.name, Type(relatedTo), relatedTo

# Movies and actors up to 4 "hops" away from Kevin Bacon
MATCH (bacon:Person {name:"Kevin Bacon"})-[*1..4]-(hollywood)
RETURN DISTINCT hollywood

# Bacon path, the shortest path of any relationships to Meg Ryan
MATCH p=shortestPath(
  (bacon:Person {name:"Kevin Bacon"})-[*]-(meg:Person {name:"Meg Ryan"})
)
RETURN p

# Extend Tom Hanks co-actors, to find co-co-actors who haven't worked with Tom Hanks...
MATCH (tom:Person {name:"Tom Hanks"})-[:ACTED_IN]->(m)<-[:ACTED_IN]-(coActors),
      (coActors)-[:ACTED_IN]->(m2)<-[:ACTED_IN]-(cocoActors)
WHERE NOT (tom)-[:ACTED_IN]->()<-[:ACTED_IN]-(cocoActors) AND tom <> cocoActors
RETURN cocoActors.name AS Recommended, count(*) AS Strength ORDER BY Strength DESC

# Find someone to introduce Tom Hanks to Tom Cruise
MATCH (tom:Person {name:"Tom Hanks"})-[:ACTED_IN]->(m)<-[:ACTED_IN]-(coActors),
      (coActors)-[:ACTED_IN]->(m2)<-[:ACTED_IN]-(cruise:Person {name:"Tom Cruise"})
RETURN tom, m, coActors, m2, cruise


### clean up

# Delete all Movie and Person nodes, and their relationships
MATCH (n) DETACH DELETE n
# Note you only need to compare property values like this when first creating relationships

# Prove that the Movie Graph is gone
MATCH (n) RETURN n

```