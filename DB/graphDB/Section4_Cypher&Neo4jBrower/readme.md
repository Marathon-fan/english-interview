

## what is Neo4j   


## popular use cases 

Cypher Query Language(CQL)

Nodes, Relationships, Labels, Properties

CREATE, MATCH

SET, REMOVE

DELETE

##  understanding Cypher Query Language    
Cypher
```
Cypher uses a variety of clauses
MATCH and WHERE are the most common
MATCH is used for describing the structure of a pattern searched for
WHERE is used to add additional constraints for patterns
```

example
```
MATCH (charlie:Person { name: 'Charlie Sheen'})-[:ACTED_IN]-(movie:Movie) RETURN move

[:XXXX]         relationships            
```

Cypher Clauses
```
MATCH			to match/find/select patterns
WHERE		
CREATE
DELETE
SET & REMOVE
RETURN
MERGE
```

Nodes
```
use parenthesis() which look like circles
to refer to the node, use an identifier like (n), (p) or (person)


example:
MATCH (n) RETURN n
MTCH (person) RETURN person
```

Labels
```
Labels let us group our nodes
short identifiers are usually used when labels are used (n, a, b)

example:
MATCH (node:Label) RETURN node
MATCH (n:Person) RETURN n            # n is the short identifier for Person
```

Relationships
```
Nodes can be linked by relationships using -->
Brackets and a colon [:] are used for relationship names

example

MATCH (a)-->(b)                  # all of the a nodes are ralted to b nodes  
MATCH (a)-->()                   # all of the a nodes that are related to anything  
MATCH (a)-[:REL_TYPE]->(b)       # all of the a nodes that has REL_TYPE relationship with b nodes   

MATCH (a:Person)-[:KNOWS]->(b:Person) RETURN a, b
                                 # match a person that has relation(KNOWS) with b person, then return a, b 

```


Direction

```
Only one way direction is supported when using CREATE
CREATE (a:Person {name:'Sarah'})-[r:FRIENDS]->(b:Person {name:'Frank'})   # CREATE a node person that has FRIENDS relationship with b Person node

Two way is supported when using MATCH

MATCH (a:Person)-[:FRIENDS]->(b:Person) RETURN a,b     // left to right         get all the friends of a
MATCH (a:Person)<-[:FRIENDS]-(b:Person) RETURN a,b     // right to left         get all the friends of b
MATCH (a:Person)-[:FRIENDS]-(b:Person) RETURN a,b      // Both Ways             get all the friends of a and b

```

Properties
```
Instead of returning the whole node itself, we can return any of the properity values

MATCH (a)  RETURN a.property
MATCH (a:Label {property="propvalue"}) RETURN a      # when the property is propvalue, then return a node
MATCH (a:Label {property="propvalue"}) RETURN a.property

MATCH (a:Person) RETURN a.name                       # for every node that matches Person, return a.name
MATCH (a:Person {name='Brad'}) RETURN a              # when the Person's name is Brad, return the node a
MATCH (a:Person {name='Brad'}) RETURN a.name
```


