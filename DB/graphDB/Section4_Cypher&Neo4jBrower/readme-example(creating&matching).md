

## dependencies   

1 run neo4j through docker  

docker run \
    --publish=7474:7474 --publish=7687:7687 \
    --volume=$HOME/neo4j/data:/data \
    --env=NEO4J_ACCEPT_LICENSE_AGREEMENT=yes \
    --env=NEO4J_AUTH=none \
    neo4j:3.3.7

then navigate into neo4j browswer:  
localhost:7474


## createing nodes and matching nodes

```sql
CREATE (n:Person {name:'John'}) RETURN n;
```

```sql
CREATE (n:Person {name:'Sally'}) RETURN n;
```

```sql
CREATE (n1:Person {name:'Steve'})
CREATE (n2:Person {name:'Mike'})
CREATE (n1:Person {name:'Liz'})
CREATE (n2:Person {name:'Shawn'})

```


```sql
CREATE (l1:Location {city:'Miami', state:'FL'})

CREATE (l2:Location {city:'Boston', state:'MA'})

CREATE (l3:Location {city:'Lynn', state:'ML'})

CREATE (l4:Location {city:'Portland', state:'ME'})

CREATE (l5:Location {city:'San Francisco', state:'CA'})

```


query
```sql
MATCH (n) RETURN (n)

MATCH (n:Person) RETURN (n)

MATCH (n {name:'Mike'}) RETURN (n)

MATCH (n) WHERE n.name = 'Mike' RETURN n

MATCH (n) WHERE n.name = 'Mike' or n.name = 'John' RETURN n

MATCH (n) WHERE n.name = 'Mike' or n.name = 'John' RETURN n.name

```


## createing relationships    


ADD relationship between Liz and Mike
```SQL
MATCH (a:Person {name:'Liz'}),
(b:Person {name:'Mike'})
MERGE (a)-[:FRIENDS]->(b)
```

ADD relationship(with property) between Shawn and Sally
```SQL
MATCH (a:Person {name:'Shawn'}),
(b:Person {name:'Sally'})
MERGE (a)-[:FRIENDS {since:2001}]->(b)
```


```SQL
MATCH (a:Person {name:'Shawn'}),
(b:Person {name:'John'})
MERGE (a)-[:FRIENDS {since:2012}]->(b)
```


```SQL
MATCH (a:Person {name:'Shawn'}),
(b:Person {name:'Mike'})
MERGE (a)-[:FRIENDS {since:2006}]->(b)
```


```SQL
MATCH (a:Person {name:'Sally'}),
(b:Person {name:'Steve'})
MERGE (a)-[:FRIENDS {since:2006}]->(b)
```


```SQL
MATCH (a:Person {name:'Liz'}),
(b:Person {name:'John'})
MERGE (a)-[:MARRIED {since:1998}]->(b)
```


```SQL
MATCH (a:Person {name:'John'}),
(b:Location {city:'Boston'})
MERGE (a)-[:BORN_IN {Year:1978}]->(b)
```


```SQL
MATCH (a:Person {name:'Liz'}),
(b:Location {city:'Boston'})
MERGE (a)-[:BORN_IN {Year:1981}]->(b)
```

```SQL
MATCH (a:Person {name:'Mike'}),
(b:Location {city:'San Francisco'})
MERGE (a)-[:BORN_IN {Year:1960}]->(b)
```

```SQL
MATCH (a:Person {name:'Shawn'}),
(b:Location {city:'Miami'})
MERGE (a)-[:BORN_IN {Year:1960}]->(b)
```

```SQL
MATCH (a:Person {name:'Steve'}),
(b:Location {city:'Lynn'})
MERGE (a)-[:BORN_IN {Year:1970}]->(b)
```

## query   

```SQL
MATCH (a:Person)-[:BORN_IN] -> (b:Location {city:'Boston'}) RETURN a, b
MATCH (a:Person)-[:BORN_IN] -> (b:Location {city:'Boston'}) RETURN a

```

return all nodes that relate to anything
```SQL
MATCH (a)-->() RETURN a

MATCH (a)--() RETURN a 
# return both way

# return all the names of the relationship
MATCH (a)-[r]-() RETURN a.name, type (r)
```

```SQL
MATCH (a)-[r]->() RETURN type(r)

MATCH (a)-[r]->() RETURN DISTINCT type(r)

MATCH (n)-[:MARRIED]->() RETURN n.name
MATCH (n)-[:MARRIED]-() RETURN n.name

```

```SQL
CREATE (a:Person {name:'Todd'})-[r:FRIENDS]->(b:Person {name:'Carlos'})

```

```SQL
MATCH(a:Person {name:'Mike'})-[r1:FRIENDS]-()-[r2:FRIENDS]-(friend_of_a_friend) RETURN friend_of_a_friend.name  AS fofName  
```

## updating and deleting nodes & properties      

```SQL
MATCH(a:Person {name:'Liz'}) SET a.age = 34

MATCH(a1:Person {name:'Shawn'}) SET a.age = 32

MATCH(a2:Person {name:'John'}) SET a2.age = 44

MATCH(a3:Person {name:'Mike'}) SET a3.age = 25

MATCH(a:Person {name:'Mike'}) SET a.test = 'TEST'



```

ADD, REMOVE, UPDATE PROPERTY
```SQL

MATCH(a:Person {name:'Mike'}) SET a.test = 'TEST'

MATCH(a:Person {name:'Mike'}) REMOVE a.test

# update
MATCH(a:Person {name:'Liz'}) SET a.age = 35
```

DELETE A NODE
```SQL
MATCH(a:Location {city:'Portland'}) DELETE a;

# remove all the corresponding relationships before deleting a node
# Neo.ClientError.Schema.ConstraintValidationFailed: Cannot delete node<160>, because it still has relationships. To delete this node, you must first delete its relationships.

MATCH(a:Person {name:'Todd'}) DELETE a;

# 
MATCH(a:Person {name:'Todd'})-[rel]-(b:Person) DELETE a, b, rel

# DELETE ALL NODES AND ALL RELATIONSHIPS
MATCH (n) OPTIONAL MATCH (n)-(r)-() DELETE N, R
```





