
## topics covered   

```
1 cypher queries & clauses
2 Lists / IN
3 WITH, UNWIND, UNION
4 HTTP RESTful API
5 Language Drivers
```



## dependencies   

1 run neo4j through docker  

```sh
docker run \
    --publish=7474:7474 --publish=7687:7687 \
    --volume=$HOME/neo4j/data:/data \
    --env=NEO4J_ACCEPT_LICENSE_AGREEMENT=yes \
    --env=NEO4J_AUTH=none \
    neo4j:3.3.7
```



then navigate into neo4j browswer:  
localhost:7474


## other Cypher Queries & Clauses   

**Node Patterns**   
(n:Person)                   Node with Person label    
(n:Person:Member)            Node with Person & Member labels   
(n:Person {name:'Jeff'})     Node with properties


**Relationship Patterns**   
```
(a)-->(b)                   		Relationship from a to b   
(a)--(b)							Relationship in any direction between a and b  
(a:Person)-->(b)					Node labeled Person with relationship to b  
(a)-[:KNOWS]->(b)					Relationship of type "KNOWS" from a to b
(a)-[:KNOWS |:LOVES]->(b)			Relationship of type "KNOWS" OR 'LOVES' from a to b	 
(a)-[r]->(b)						Bind the relationship to variable r         # there is no : here
(a)-[:KNOWS]->(b {property:value})	Relationship of the type 'KNOWS' with a declared property  	
```

**Variable Length Patterns**   
```
(a)-[*1..5]->(b)                   	Variable length path of between 1 and 5 relationships from a to b  
(a)-[*]->(b)						Variable length of any number of relationships from a to b  
size((a)-->()-->()) 				Count paths that match pattern
```


**Lists and the IN Operator**    
If Cypher knows something exists in a list, the result will be true   
Any list that contains a NULL and doesn't have a matching event will return NULL  

```
Expression 					Result
2 IN [1, 2, 3]				TRUE
2 IN [1, NULL, 3]			NULL
2 IN [1, 2, NULL]			TRUE
2 IN [1]					FLASE
2 IN []						FLASE
NULL IN [1, 2, 3]			NULL
NULL IN [1, NULL, 3]		NULL
NULL IN []					FALSE
```

example
```
MATCH (n)
WHERE id(n) in [0, 3, 5]
RETURN n
```

**OPTIONAL MATCH**    
Works like MATCH expect if no matches are found, OPTIONAL MATCH will use NULLs for missing parts of the pattern   
Similar to Outer join in SQL   

```SQL
MATCH (a:Movie {title: 'Some Movie'})
OPTIONAL MATCH (a)-->(x)
RETURN x
```

**Aliases**    

```
MATCH (A {NAME:'Bob'})
Return a.birth_date AS Birthday
```

**Order By**    

```SQL
MATCH (n)
RETURN n   
ORDER BY n.last_name DESC
```

```SQL
MATCH (n)
RETURN n   
ORDER BY n.last_name, n.first_name DESC
```

**Limit Results**    
```SQL
MATCH (n)
RETURN n   
ORDER BY n.name
LIMIT 5
```

**skip**    
```SQL
MATCH (n)
RETURN n   
ORDER BY n.name
SKIP 3
```

(This will start at the forth result)

**WITH**    
Allows queries to be chained together, piping the results from one to be used as starting points or criteria in the next   

```SQL
MATCH (david {name:"David"})--(otherPerson)-->()
WITH otherPerson, count(*) AS    # friend of a friend  
WHERE foaf > 1
RETURN otherPerson

```

**UNWIND**    
Expands a list into a sequence of rows    

```SQL
UNWIND [1, 2, 3] AS x
RETURN x
```

**UNION**    
Used to combine the result of multiple queries    

```SQL
MATCH (n:Actor)   
RETURN n.name AS name   
UNION ALL MATCH (n:Movie)   
RETURN n.title AS name
```

**String Match Negation**    
You can use NOT to exclude all matches on a given string   

```SQL
MATCH (n)
WHERE NOT n.name ENDS WITH 'r'
RETURN n
```

**Operators**    

```SQL
Mathematical      	+, -, *, /, %, ^   
Comparison 			=, <>, <, >, <=, >=, IS NULL, IS NOT NULL   
Special  			STARTS WITH, ENDS WITH, CONTAINS
Boolean 			AND, OR, XOR, NOT
```


**Aggregation**    

```
Aggregate or group data while traversing patterns   
Happens in the RETURN clause  
Most common aggregation functions are available
count, sum, avg, min, max
NULL values are skipped during aggregation

```

COUNT example(count the number of all Person nodes)
```SQL
MATCH (:Person)
RETURN COUNT(*) AS people
```

SUM & AVG example
```SQL
MATCH (e:Employee)
RETURN SUM(e.sal), AVG(e.sal)
```


## Neo4j HTTP REST API   

```
install RestEasy to chrome
```

```
launch RestEasy

use RestEasy to GET the following address:  
http://localhost:7474/db/data

and we get something a list of end points

POST data to the following address:    
http://localhost:7474/db/data/cypher    

query1

{
	"query":"MATCH(n) RETURN n"
}

query2

{
	"query":"MATCH(n) RETURN n.name"
}

query3

{
	"query":"MATCH(n:Person) RETURN n.name"
}

query4

{
	"query":"MATCH(n:Person) WHERE n.name={name} RETURN n.name",
	  "params": {
	  	"name":"Liz"
	  }
}

query5

{
	"query":"MATCH(n:Person) WHERE n.name={name} RETURN n.name, n.age",
	  "params": {
	  	"name":"Liz"
	  }
}


query6

{
	"query":"MATCH(a:Person)-[:FRIENDS]->(b:Person) WHERE a.name={name} RETURN b.name",
	  "params": {
	  	"name":"Liz"
	  }
}

query7

{
	"query":"MATCH(a:Person)-[:FRIENDS]-(b:Person) WHERE a.name={name} RETURN b.name",
	  "params": {
	  	"name":"Liz"
	  }
}

query8

{
	"query":"MATCH(a:Person)-[:FRIENDS]-(b:Person) WHERE a.name={name} RETURN b.name",
	  "params": {
	  	"name":"Mike"
	  }
}


query9 create data

{
	"query":"CREATE (n:Person {name:{name}})  RETURN n",
	  "params": {
	  	"name":"Peter"
	  }
}

query10 create a new relationship

{
	"query":"MATCH(a:Person {name:{name1}}), (b:Person {name:{name2}}) MERGE (a)-[:FRIENDS {since:{since}}]-(b)",
	  "params": {
	  	"name1":"Peter",
	  	"name2":"John",
	  	"since":1990
	  }
}



query10 find

{
	"query":"MATCH(a:Person)-[:BORN_IN]-(b:Location {city:{city}}) RETURN a.name",
	  "params": {
	  	"city":"Boston"
	  }
}



```


## Neo4j -- language Drivers   

```
official Drivers:
.Net  
Java
JavaScript/Node.js
Python

Community Drviers:
Ruby
PHP
Spring
Go
C/C++
```

Bolt Protocol: Connection-oriented protocol that uses compact binary encoding over TCP or web sockets(before 3.0, HTTP/REST was used)   




