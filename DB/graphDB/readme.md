

## what is Neo4j   

Neo4j is a high scalable, native graph database purpose- built to leverage not only data but also its relationships

1 not just about data, but also the connction between them  
2 native graph storage and processing  
3 uses Cypher Query language  

## ACID - compliant   

set of properties of database transactions
```
atomacity
consistency
isolation
durability
```

## the graph model

```
node a: person(the property is name: Tom Hanks)
a ACTED_IN(the property is role: Robert Langdon) c
a KNOWS(the property is since: 1987) B

b: person(the property is name: Ron Howard)
b DIRECTED c

c: movie(the property is title: The DaVinci Code)
```
![the graph model: Movie example](./pics/TheGraphModelExample.jpg)

## Cypher Query Language  

declarative graph query language for matching patterns  
efficient querying and updating of graph stores  
simple but powerful  
complicated queries can be expressed easily  
SQL for graphs  


example: match all the people a person knows
```
MATCH (me:Person)-[:KNOWS]->(you:Friend)
```

## scale & performance   

scales across volumes with solid data integrity   
support for replication with failover   
monitor clusters with Neo4j Metrics and other tools   

## Editions of Neo4j    
Community - free but limited to 1 node(lack of clustering and no hot backups)   
Enterpirse - Must buy a license unless the app built on top is open source  
Government - Extends Enterprise with additional government service including FISMA-related certification and support  


## popular use cases 

```
matchmaking
network management
software analytics
scientific research
project management
recommendations
social networks
```

## cmds  
MATCH (n) RETURN (n)



## questions  
All graph databases use native graph storage     ---   wrong   

What other version of Neo4j is available aside from Community?    ---   Enterprise    


Neo4j Browser is located on which port by default?    ----  7474    


Which driver is not an official but is a contributed driver?    ---- PHP  











