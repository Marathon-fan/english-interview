

# exam   

## 1 Sharding   
Sharding is a method for distributing data across multiple machines.  

MongoDB uses sharding to support deployments with very large data sets and high throughput operations.

There are two methods for addressing system growth: vertical and horizontal scaling.

**Vertical Scaling** involves increasing the capacity of a single server, such as using a more powerful CPU, adding more RAM, or increasing the amount of storage space.
As a result, there is a practical maximum for vertical scaling.
---simple

**Horizontal Scaling** involves dividing the system dataset and load over multiple servers, adding additional servers to increase capacity as required.  
Expanding the capacity of the deployment only requires adding additional servers as needed, which can be a lower overall cost than high-end hardware for a single machine. 
----The trade off is increased complexity in infrastructure and maintenance for the deployment.


**Sharded Cluster**     
A MongoDB sharded cluster consists of the following components:

shard: Each shard contains a subset of the sharded data. Each shard can be deployed as a replica set.  
mongos: The mongos acts as a query router, providing an interface between client applications and the sharded cluster.  
config servers: Config servers store metadata and configuration settings for the cluster. As of MongoDB 3.4, config servers must be deployed as a replica set (CSRS).   

![shardedCluster](./pics/shardedCluster.jpg)


**Shard Keys**  
To distribute the documents in a collection, MongoDB partitions the collection using the shard key. The shard key consists of an immutable field or fields that exist in every document in the target collection.

The shard key consists of an immutable field or fields that exist in every document in the target collection.

In the following example, collection1 is sharded while shard2 is not sharded.  
![shardExample1](./pics/shardExample1.jpg)

You must connect to a mongos router to interact with any collection in the sharded cluster. 

**The shard key** is either an indexed field or indexed compound fields that exists in every document in the collection.

The ideal shard key allows MongoDB to distribute documents evenly throughout the cluster.

Combination of fields from that query would make the best shard key.



two sharding strategies
```
Hashed Sharding
Hashed Sharding involves computing a hash of the shard key field’s value. Each chunk is then assigned a range based on the hashed shard key values.


Ranged Sharding
Ranged sharding involves dividing data into ranges based on the shard key values. Each chunk is then assigned a range based on the shard key values.
```

jumbo chunks --- chunks too big to be splitted


```
question1 In a sharded cluster, which of the following indexes must contain only unique values? Check all that apply.
```
The _id must be unique in a replica set, as those values are used in the Oplog to reference documents to update. This characteristic of uniqueness is enforced by the system.

In a sharded cluster, _id must also be unique across the sharded collection because documents may migrate to another shard, and identical values would prevent the document to be inserted in the receiver shard, failing the migration of the chunk. It is the responsibility of the application to ensure uniqueness on _id for a given collection in a sharded cluster if it is not the shard key.

Note that if _id is used as the shard key, the system will automatically enforce the uniqueness of the values, as chunk ranges are assigned to a single shard, and the shard can ensure uniqueness on the values in that range.

As for the shard key index, if it's not _id, it is perfectly acceptable to have identical values for different documents. However, beware of having too many documents with the same values, as this will lead to jumbo chunks.

```
When a chunk is in flight from one shard to another during a migration process, where are reads to that chunk directed?
```
To the shard from which it is being migrated


## Replication   

Hidden members are part of a replica set but cannot become primary and are invisible to client applications. Hidden members may vote in elections. 


```
What is the principal advantage of having a delayed replica set member?
```
It provides a window of time to recover from an operator error.

If you have a delayed member in your replica set, for example, a delay of one hour, it will take one hour before changes on the Primary are replicated to this member.

If a user were to drop a collection or database on the Primary, you would have one hour to go to this delayed member to retrieve the destroyed data.

You can also query older versions of your documents, however, you can't choose a historical version to retrieve as you only get the one that existed one hour ago.


```
What settings can be controlled by rs.reconfig()? Check all that apply.
```
Priority for each replica set member
Which replica set members are hidden
Which version of MongoDB each replica set member is running


You can not set the version of MongoDB a given replica is using. This would be a little difficult to control, as nodes may not have the desired version installed yet.

The other options make more sense to be controlled in a global configuration, as you want to be able to change them from one location (the Primary), and have the changes being effective without having to restart the mongod processes.

## aggregation   

```
$sample in aggregate
```
The $sample stage in the Aggregation Framework returns a subset of documents in a random fashion. 


## CRUD   


```
Consider the following example document from the sample collection. All documents in this collection have the same schema.
{
    "_id" : 3, 
    "a" : 7,
    "b" : 4
}
Which of the following queries will replace this with the document,
{
    "_id" : 7, 
    "c" : 4
}
```
The correct answer is that this operation cannot be done in a single query.  

To understand why, recall that the _id field of a document is immutable.  

In fact, trying this operation with:  

updateOne({_id: 3}, {$set: { _id: 7, c: 4 }, $unset: { a: "", b: "" }})  
produces the following error:  
"Performing an update on the path '_id' would modify the immutable field '_id'"  


```
$search 

For example, you could use the following query to find all stores containing any terms from the list “coffee”, “shop”, and “java”:

db.stores.find( { $text: { $search: "java coffee shop" } } )


You can also search for exact phrases by wrapping them in double-quotes. For example, the following will find all documents containing “java” or “coffee shop”:

db.stores.find( { $text: { $search: "java \"coffee shop\"" } } )
```


## Data Modeling    

```
Which of the following are good reasons to denormalize and create copies of your data in different collections? Check all that apply.
When you want to optimize for your most common read use case(s).
There is never a good reason to denormalize.
To avoid having to join data in the application layer.
```
If one of the main queries of the system is pulling related information from different collections, it usually gives better performance to group those information in a single document.

Keeping the information together will remove the need to do the corresponding joins in the application. The single document may also be a better match to the representation of that object in the application.

If you use MongoDB with a direct mapping of each collection to a table in the Relational model, you are not making use of some benefits brought by the document model. So, yes using a denormalized model is encouraged.

As for duplicating information in different collections (think of an address in an order for example), this is perfectly acceptable if you need to do it to get good performance.


## indexes and performance     

sort order of compound indexes 1
```
The following operation creates an ascending index on the item and stock fields:

db.products.createIndex( { "item": 1, "stock": 1 } )

The order of the fields listed in a compound index is important. The index will contain references to documents sorted first by the values of the item field and, within each value of the item field, sorted by values of the stock field. See Sort Order for more information.
```

sort order of compound indexes 2
```
Consider a collection events that contains documents with the fields username and date. Applications can issue queries that return results sorted first by ascending username values and then by descending (i.e. more recent to last) date values, such as:

db.events.find().sort( { username: 1, date: -1 } )
or queries that return results sorted first by descending username values and then by ascending date values, such as:

db.events.find().sort( { username: -1, date: 1 } )
The following index can support both these sort operations:

db.events.createIndex( { "username" : 1, "date" : -1 } )
However, the above index cannot support sorting by ascending username values and then by ascending date values, such as the following:

db.events.find().sort( { username: 1, date: 1 } )

```

sort order of compound indexes 3(only the prefixes for that index array will be OK for fully using the index. If not, then mongoDB still need to use in-memory sort as said by the following link offered by mongoDB)
```
Sort and Index Prefix
https://docs.mongodb.com/manual/tutorial/sort-results-with-indexes/
```


```
WiredTiger storage engine 

The WiredTiger storage engine supports document-level concurrency, allowing multiple documents from the same collection to be written to, simultaneously.

```

```
You have created the following index on the foo collection:

> db.foo.createIndex( { a : 1, b : -1, c : -1, d: 1 } )
Which of the following queries will be able to fulfill the query without an in-memory sort (i.e., it's able to use the index to sort)? Check all that apply.


db.foo.find( { a : { $gt : 100 } } ).sort( { c : -1 } )
db.foo.find( { a : 100 } ).sort( { b : 1, c : 1 } )
db.foo.find( { a : 200, b : { $lt : 100 } } ).sort( { b : 1 } )
Detailed Answer

Recall that compound indexes should be built in the order of equality, range, sort for common operational query patterns.

Also recall that as long as the query uses all keys of a compound index or a combination of index prefixes, it will make use of the existing index.

```


**The query on date, name will also use an index, the name_1_date_1_phone_1 index, because a prefix is specified (name, date).**

```
You have the following indexes on your collection:

[
	{
		"v" : 1,
		"key" : {
			"_id" : 1
		},
		"name" : "_id_",
		"ns" : "test.sample"
	},
	{
		"v" : 1,
		"key" : {
			"name" : 1,
			"date" : 1,
			"phone" : 1
		},
		"name" : "name_1_date_1_phone_1",
		"ns" : "test.sample"
	}
]
Which of the following queries will use an index? Check all that apply.



db.sample.find( { _id : 22, date: ISODate("2012-07-04" } )
db.sample.find( { date: ISODate("2011-07-04"), name : "Alice" } )
db.sample.find( { title : "DBA" } )
db.sample.find( { phone : "123-456-7890"), info : "201-555-5792" } )
Detailed Answer

There are 2 indexes:

{ _id: 1 }
{ "name" : 1, "date" : 1, "phone" : 1 }
The order of the fields in the index is important, however, the order of the fields in the query are not significant as the query planner will "reorder" the query terms to match a prefix of, or the full compound index.

The query on _id will use the first index. Because _id is guaranteed to be unique, it's possible for the planner to make this optimization. To be sure, there will still be a FETCH stage to get the document and ensure the date predicate is fulfilled.

The query on date, name will also use an index, the name_1_date_1_phone_1 index, because a prefix is specified (name, date).

The query on title is using a field for which there is no index.

As for the query on phone and info, phone is indexed, however as the third member of the compound index so won't be used.

```


**Because the field user.login is indexed and the regex beginning of line operator is being used (^), the index myIndex will be used for this query.**

```
INDEXES AND PERFORMANCE: Question 7 

Given the following example document:

{
  "_id": ObjectId("5360c0a0a655a60674680bbe"),

  "user": {
    "login": "ir0n",
    "description": "Made of metal"
    "date": ISODate("2014-04-30T09:16:45.836Z"),
  }
}
and the following index:
db.users.createIndex( { "user.login": 1, "user.date": -1 }, "myIndex" )
When performing the following query:
db.users.find( { "user.login": /^ir.*/ }, { "user":1, "_id":0 } ).sort( { "user.date":1 } )
Which of the following statements correctly describes how MongoDB will handle the query?


As a covered query using "myIndex" because we are filtering out "_id" and only returning "user.login"
As an index scan that uses "myIndex" because field "user.login" is indexed
As an optimized sort query (no explicit sort stage) using "myIndex" because we are sorting on an indexed field
MongoDB will need to do a table/collection scan to find matching documents
None of the above
Detailed Answer

Because the field user.login is indexed and the regex beginning of line operator is being used (^), the index myIndex will be used for this query.

To learn more, visit the following MongoDB Documentation page:
```

**Covered Query**
```
A covered query is a query that can be satisfied entirely using an index and does not have to examine any documents. An index covers a query when both of the following apply:

all the fields in the query are part of an index, and
all the fields returned in the results are in the same index.
```

```
Which of the following must be true for a query to be a covered query? Check all that apply.


All fields used in the selection filter of the query must be in the index that the query uses
All fields returned in the results must be in the index that the query uses
All fields returned in the results must be fields in the selection filter of the query
Detailed Answer

All fields used in the selection filter of the query must be in the index, so the system can find the documents that satisfy the selection filter without having to retrieve the document from the collection.

All fields returned in the results must be in the index, so again there is no need to retrieve the full document. A common mistake is not to provide a projection that filters out the field _id, which is returned by default. If the _id field is not a field in the index definition, it is not available, and the query system will need to fetch the full document to retrieve the value.

On the other hand, it is OK to ask for more fields than the ones provided in the selection filter, as long as those are in the index values, the system has all the information needed to avoid fetching the full document from the collection.

To learn more, visit the following MongoDB Documentation page:

```


**unique indexes**  
A unique index ensures that the indexed fields do not store duplicate values; i.e. enforces uniqueness for the indexed fields. By default, MongoDB creates a unique index on the _id field during the creation of a collection.

You may not specify a unique constraint on a hashed index.

```
Which of the following statements are true of unique indexes? Check all that apply.


The only possible unique index is the "_id" field.
The "unique" constraint on an index ensures that no two (or more) documents can share a value for that field in a collection
Hashed indexes cannot be unique.
Detailed Answer

Unique indexes have certain properties and restrictions that you should be familiar with.

For example, they ensure that no documents have the same data at the same key that carries a unique index, and you may not specify a unique constraint on a field that is specified as a hashed index.
```




## philosophy & Features   

```
MongoDB 4.0 introduced updates to multiple documents in a replica set through transactions.

In MongoDB 3.6, updates to single documents in replica sets or sharded clusters were atomic.
```


JSON can only represent a subset of the types supported by BSON. 

```

Why does MongoDB use BSON rather than JSON? Check all that apply.


BSON includes metadata to describe a document/object
BSON supports more data types than JSON
BSON is more human readable than JSON
Detailed Answer

BSON extends the JSON model to provide additional data types, ordered fields, and to be efficient for encoding and decoding within different languages.

```



```

Which of the following are valid types in BSON? Check all that apply.


Int64
ObjectId
Decimal128


Detailed Answer

These are all valid BSON types.

To learn more, visit the following MongoDB Documentation page:

BSON Types

```


**Collections** are analogous to tables in relational databases.

**chunks**
MongoDB uses the shard key associated to the collection to partition the data into chunks. A chunk consists of a subset of sharded data. Each chunk has a inclusive lower and exclusive upper range based on the shard key.


**balancer**
The MongoDB balancer is a background process that monitors the number of chunks on each shard. When the number of chunks on a given shard reaches specific migration thresholds, the balancer attempts to automatically migrate chunks between shards and reach an equal number of chunks per shard.

The balancer process is responsible for redistributing the chunks of a sharded collection evenly among the shards for every sharded collection. By default, the balancer process is always enabled.


**Config servers** store the metadata for a sharded cluster. The metadata reflects state and organization for all data and components within the sharded cluster. The metadata includes the list of chunks on every shard and the ranges that define the chunks.

The mongos instances cache this data and use it to route read and write operations to the correct shards. mongos updates the cache when there are metadata changes for the cluster, such as Chunk Splits or adding a shard. Shards also read chunk metadata from the config servers.

The config servers also store Authentication configuration information such as Role-Based Access Control or internal authentication settings for the cluster.

MongoDB also uses the config servers to manage distributed locks.

Each sharded cluster must have its own config servers. Do not use the same config servers for different sharded clusters.

