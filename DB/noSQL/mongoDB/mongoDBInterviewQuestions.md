


Atomicity
In MongoDB, a write operation is atomic on the level of a single document, even if the operation modifies multiple embedded documents within a single document.

When a single write operation (e.g. db.collection.updateMany()) modifies multiple documents, the modification of each document is atomic, but the operation as a whole is not atomic.


```
Starting in version 4.0, for situations that require atomicity for updates to multiple documents or consistency between reads to multiple documents, MongoDB provides multi-document transactions for replica sets.

Transactions
Starting in MongoDB 4.0, multi-document transactions are available for replica sets.
```


```
A replica set in MongoDB is a group of mongod processes that maintain the same data set. Replica sets provide redundancy and high availability, and are the basis for all production deployments. This section introduces replication in MongoDB as well as the components and architecture of replica sets.
```



MongoDB​ ​Interview​ ​Questions
set-1
1. How would you restrict the size of a collection in MongoDB?

2. What is meant by Covered Query in MongoDB?

3. How mongoDB can be used for Cache Management?

4. What is the difference between Enterprise and community version of MongoDB?

5. Have you worked with in premises or cloud hosting of MongoDB?

6. Have you used Atlas Service?

7. Why mongoDB?

8. What are the most powerful features of MongoDB?

9. How will you update all documents in a Collection?

10. How will add a field to all documents in a collection?

11. Have you used Stitch MongoDB Service?

12. What are the Best practices in MongoDB ?

13. How would you See the execution plan of a MongoDB Query?

14. Can MongoDB and Redis work together?

15. What's new in MongoDB 3.4?

16. Have you migrated your traditional Database to mongoDB ? Explain your experience?

17. Can you do document validation in a Collection? Explain

18. What are the validation levels in Mongodb?

19. Does MongoDB supports Views?

20. How to create views in MongoDB?

21. How would you relate documents in MongoDB?

22. Have you worked with Embedded documents in MongoDB? What's the advantage and why do you used it?

23. What are Range and Key Value Queries?

24. Explain Aggregation in MongoDB? Why you selected it over Mapreduce?

25. What are Compound indexes ? When to use them?

26. How to do a ascending index in Mongodb?

27. How to use partialFilterExpression in MongoDB indexing?

28. How and When do you do reindexing?

29. What are Partial Indexes and Why they are used ?

30. If a field is present in all documents, What's the type of index you wil recommend?
[sparse index]

31. How do you index arrays?

32. What is your Project MongoDB Topology you used?\

33. What are the critical issues you faced when working with MongoDB?

34. Can you use MongoDB for Transaction ? Explain how will you ?

35. Does MongoDB supports Atomicity?

36. How you used javascript with MongoDB?

37. How does replication works in Mongodb? How election works?

38. What is the use of ismaster and freeze in replication?

39. Why we need Query Router?

40. What are the types of Sharding MongoDB Supports?

41. What's the advantage of hash Sharding?

42. What's the use of $lookup and $graphLookup?

43. Have you used MongoDB compose?

44. What's the GUI client you used and What are its frequently used features?

45. Have you worked with MongoDB on containers?

46. How MongoDB backup is done? Have you faced any issues?

47. How mongodb is monitored? Which software you used for that?

48. Have you ever rolled back to a previous database backup of MongoDB?

49. How can you lock a Database in MongoDB?

50. How to get the index details?

51. Which connection pooling you used in your code?

52. What is BSONdump?

53. How will you insert multiple documents in Mongodb?

54. How to do batch operations in Mongodb?

55. What is the difference between save and insert in MongoDB?

56. What is meant by write acknowledgement?

57. Whats is the replacement for SQL joins in MongoDB?

58. How to use regex in MongoDB? Give some examples?

59. What is soundex?

60. How to find distinct values in documents?

61. How to print all the users available in Mongodb?

62. What does show profile does?

63. How will you search a document with a String 'hello'?

64. Whats is the alternative for $in operator?

65. How do you do text search in mongoDB?

66. How will you search documents with array of Size 5?

67. What does $elemMatch does? explain with example

68. How will you check a Document , for a field's presence?

69. What does $unset does?

70. How will you get the current date in mongodb?
$currentDate

71. What does the difference between update and upsert?

72. How will you append a Value to an Array?

$push

73. What does $addtoset does?

74. What's the difference between $pull and $pullall?

75. In aggregation pipeline What does $unwind does?

76. In aggregation pipeline how will you output the resultant documents to a Collection?

77. How will you group documents in mongoDB?

78. Does MongoDB supports Encryption?

79. How do you store password and sensitive data in MongoDB?

80. How you migrated your data from sql databases to mongoDB?

81. What is the size limit of a MongoDB Document?

82. How Gridfs works? explain?

83. How will you upload images into mongodb? explain the architecture

84. What are the storage engines used by MongoDB?

85. What is the equivalent of Partitioning in Mongodb?

86. Explain Sharding in MongoDB ? List the steps

87. How will you add shards?

88. How to enable sharding in a Database?

89. Whats is Mongos?

90. What is Mongod process do?

91. Explain about objectid?

92. What are the alternatives to MongoDB?

93. Explain the stages in Aggregation pipeline?

94. Have you used any reporting or analytics tool with MongoDB?

95. What kind of ETL tool you used with mongoDB?

96. Explain your application architecture

97. How will you create a Database? and What are the considerations before creating a database?

98. How will you create a Collection? and What are the considerations before creating a database?

99. What is Vertical and Horizontal scaling?

100. What is $cmd?

101. What is the type of Authentication method you used?

102. What is BSON and What are the types of BSON?

103. Have you imported CSV content to Mongodb ?

104. What is meant by journaling?

105. Have you used server side javascript? and why it is not recommended?

106. What's the default data directory of MongoDB?

107. What are namespaces? What is the maximum length of Namespaces?


1. Why NOSQL and why not mysql  or any other Relational Classic DB?
               Hint: OLAP,https://www.mongodb.com/compare/mongodb-mysql

2. Why MEAN / MERN web stacks are preferred by Startups? [ What is the single
main Advantage of using MongoDB for Web Projects?]
              Hint: Launch Time

3. Can you please read at least two use cases of MongoDB from its website?.
Please select case studies of famous companies
                Hint:Metlife,Expedia

4. What are the top Client softwares for MongoDB ? Which one is having
intellisense with snippets and smart documentation display support?
              Hint:MongoChef,Mongo Booster,Nosql Manager
5. What are collection and Documents? What is the  relation between JSON and
Documents?

6. What is BSON and why the document is saved as BSON internally and not as
JSON?

7. Can you download the MongoDB document mentioning SQL and MongoDB
methods - Difference and relation , from the MongoDB website?
              Hint:https://docs.mongodb.com/manual/reference/sql-comparison/
8. How tables and columns in Traditional RDBMS are replaced in MongoDB? Name
them .

9. What is the  difference  javascript methods and commands in  MongoDB ?
10.How Big files are handled in MongoDB ?What is GRIDFS?



11. List 5 collection methods used for CRUD operation in MongoDB?
           Hint:https://docs.mongodb.com/manual/reference/method/js-collection/

12.What is the default index created in MongoDB ? what is the Field on which it is
created?
        Hint:_id

13.How arrays ​in JSON documents are handled in MongoDB?
        Hint:https://docs.mongodb.com/manual/tutorial/query-arrays/

14.What is the use of Fulltext search and Regex in Databases?

15.What is Mapreduce and why is it famous?

16.What is the Difference between Aggregation pipeline and Mapreduce in
MongoDB ? and why Aggregation is preferred?

17.What is meant by Sharding? And how it differs from Replication?
          Hint- Horizontal Scaling

18.What are embedded docs and which is the best way to relate documents?

19.What are TTL and Capped Collection ? what's their importance?

20.What are the programming languages does MongoDB drivers are available?
        Hint-https://docs.mongodb.com/ecosystem/drivers/

