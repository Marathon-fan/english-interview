
# advancedQueriesArraysFulltextSearch and js   


## load data into mongo

```sh
mongoimport -d <dbName> -c <collectionName> -type csv --file <absolute file path>
```

## insert

```
for (i = 0; i < 10000; i++) {
	db.sampleCollection.insert({titile:"test", views: 1000})
}

```



## query

```sh
db.region.find({RegionDescription:"Northern"})
db.region.find({RegionDescription:"Northern"}, {_id:false, RegionID:0})

db.region.find({RegionDescription:"Northern"}).count()
```

```sh
db.region.find({RegionDescription:"Northern"}).limit(2)

db.region.find({RegionDescription:"Northern"}).skip(1)

db.region.find({RegionDescription:"Northern"}).sort({RegionId: 1})
db.region.find({RegionDescription:"Northern"}).sort({RegionId: -1})


```

```sh
db.production.find({UnitPrice:{$lt:100}})

db.production.find({UnitPrice:{$gt:100}})

db.production.find({UnitPrice:{$eq:100}})

db.production.find({UnitPrice:{$gt:50, $lt:100}})

```


```sh
db.production.find({$and: [{Discountinued:1}, {UnitPrice: {$gt:100}}]})

db.production.find({$or: [{Discountinued:1}, {UnitInStock: {$eq:0}}]})
```

```sh
db.production.update({"CompanyName":"Speedy Express"}, {$set: {Phone:'1234567'}})


db.production.update({}, {$set: {Phone:'xxx-123'}}, {multiple: true})    = db.production.updateMany({}, {$set: {Phone:'xxx-123'}})  

db.production.updateOne({}, {$set: {Phone:'xxx-123'}});

db.production.find({$or: [{Discountinued:1}, {UnitInStock: {$eq:0}}]})
```

```sh
db.production.updateOne({CompanyName:'someCompany'}, {$set: {Phone:'xxx-123'}, {upsert: true}} );

```


```sh
db.customer_delDemo.distinct("Country")

db.customer_delDemo.deleteOne({Country:"xyz"})

db.customer_delDemo.remove({Country:"xyz"}, {justOne: true})

```

deal with array
```sh
db.customer_delDemo.distinct({"grade.mean":{$eq:99}})

db.students.update({}, {$push: {scores:99}})

db.students.update({}, {$addToSet: {scores:99}}, {multi:true})

$pull

$pop    # 1,   -1

$elemMatch

db.students.find(
	{scores:{$elemMatch: {
		$gt:40, $lt:50
	}}}, {"scores":true, _id:0}
) 

```

```
projection:
```
Both the $ operator and the $elemMatch operator project the first matching element from an array based on a condition.



```sh
$in
$nin
$all     # exact the same match
```


```sh
# useful methods
limit()
sort()
skip()
insertOne()
insertMany()
find()
findAndModify()
deleteOne()
deleteMany()

# update
$set,  $unset
$inc, $mul
$push, $pull, $pop
$rename
$currentDate, $min, $max

# logical 
$and
$not
$or
$nor

# comparison
$gt $gte
$lt $lte
$eq $ne
$in $nin $all


```


find
```sh
find(Query )
```

find
```sh

```

find
```sh

```

find
```sh

```

find
```sh

```

find
```sh

```


db.collection.distinct()   only works for single collection

