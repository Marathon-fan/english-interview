
# background - RDBMS, NoSQL and JSON

## data normalization

why Normalization
```
efficiently organize data
eliminate redundant data
```

normalization forms
```
1NF - eliminates repeating groups
2NF - eliminates redundant data, and 
3NF - eliminates columns that are not dependent on Key
```

## noSQL

ACID behavior

```
atomiciyt
consistency
isolation
durability
```

SQL
```
enforces strict ACID properities
loss of flexibility

use SQL(structured query language) for interaction
```

NoSQL
```
trades some ACID properties
flexible data model

horizontal scaling(you scale by adding more machines, partitions or low cost hardware)
uses object-based APIs for interaction
```

## types of NoSQL databases
```
colunmar databases

key-value store

graph database

document database

```

## JSON fundamentals

JSON - JavaScript Object Notation





