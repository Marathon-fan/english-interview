
# setup

## setup dynamoDB

1 add dynamoDB user in IAM 
```
1  access type(only programmatic access)
2 permission: amazonDynamoDBFullAccess (attach existing policies directly, then choose AmazonDynamoDBFullAccess)
```

install aws cli
```sh
$ curl -O https://bootstrap.pypa.io/get-pip.py
$ python3 get-pip.py --user
$ pip3 install awscli --upgrade --user

# in  ~/.bash_profile, add the following lines

# aws cli
export PATH=$PATH:/Users/james/Library/Python/3.7/bin

# then
source ~/.bash_profile

# then use aws configure to configure aws_access_key_id , aws_secret_access_key , region, and output(json)
aws configure
# then we can see the config in  ~/.aws/config and ~/.aws/credentials

```


```sh
# dynamoDB
# IN .env, add the following vars

DYNAMODB_AWS_ACCESS_KEY_ID=xxxx
DYNAMODB_AWS_SECRET_ACCESS_KEY=xxx
DYNAMODB_AWS_DEFAULT_REGION=us-east-1
```

3 install vscode




