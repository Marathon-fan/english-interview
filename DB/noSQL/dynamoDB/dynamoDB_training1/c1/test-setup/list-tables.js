require('dotenv').load();

const AWS = require("aws-sdk");
AWS.config.accessKeyId = process.env.DYNAMODB_AWS_ACCESS_KEY_ID;
AWS.config.secretAccessKey = process.env.DYNAMODB_AWS_SECRET_ACCESS_KEY;
AWS.config.region = process.env.DYNAMODB_AWS_DEFAULT_REGION;

const dynamodb = new AWS.DynamoDB();

dynamodb.listTables((err, data)=>{
    if(err) {
        console.log(err);
    } else {
        console.log(data);
    }
});
