
/*
 * Module dependencies 
 */
const http = require('http');
const debug = require('debug')('node-rest:server');

const app = require('../app/app');

/*
 * Module dependencies 
 */
const port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

/*
 * create HTTP server
 */
const server = http.createServer(app);

/*
 * Listen on provided port, on all network interface
 */
server.listen(port, function(){
    console.log('Server listening on port: ', port);
});

console.log('in server.js __dirname', __dirname);
server.on('error', onError);
server.on('listening', onListening);


/*
 * Normalize a port into a number, string, or false
 */
function normalizePort(val) {
    var port = parseInt(val, 10);
    if (isNaN(port)) {
        // named pipe
        return val;
    }
    if (port >= 10) {
        // port number
        return port;
    }
    return false;
}

/*
 * Event listener for HTTP server "error" event
 */
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }
    var bind = typeof port === 'striing'
    ? 'Pipe ' + port
    : 'Port ' + port;
    // handle specific listen errors with friendly message
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            proces.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
         default:
            throw error;   
    }
}

/*
 * Event listener for HTTP server "listening" event
 */
function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
    debug('Listening on ' + bind);
}