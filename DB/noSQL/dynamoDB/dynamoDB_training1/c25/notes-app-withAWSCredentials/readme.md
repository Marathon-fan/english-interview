
# introduction

An angular5, node app using DynamoDB that can add/delete/update note.

files strucutre
```sh
/app
    /routes    # restful API
    /views     # single file application

/bin
    server.js  # the starting point of server side(node)

/public        # static files. like bundle.js

/src           # angular5 files

```

# how to use it

**step1:**  

```sh
npm install
```

**step2:**  
angular5
```sh
npm run build:watch
```

**step3:**  
server side
```sh
npm start
```

**step4:**  
navigate to the following addr, and you can add/delete/update a note   
http://localhost:3000/


**note:**  
the aws credential is stored in .env file    


# restful APIs

```sh
POST /api/note

PATCH /api/note

GET /api/notes

GET /api/note/:note_id

DEL /api/note/:timestamp

```

# libs 

```sh
npm install --save express hbs compression path body-parser cors http debug aws-sdk moment underscore uuid dotenv
```

