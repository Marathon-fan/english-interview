
# readme


## aws cli cmd for dynamoDB

```sh
aws dynamodb list-tables

aws dynamodb describe-table --table-name readTableName

# the primary key here is  user_id and timestamp
aws dynamodb create-table --table-name td_notes_test --attribute-definitions AttributeName=user_id,AttributeType=S AttributeName=timestamp,AttributeType=N --key-schema AttributeName=user_id,KeyType=HASH AttributeName=timestamp,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=1,WriteCapacityUnits=1

aws dynamodb describe-table --table-name td_notes_test

aws dynamodb delete-table --table-name td_notes_test

aws dynamodb describe-table --table-name td_notes_test

aws dynamodb list-tables


```

## write operations - item level operations with aws cli

insert
```sh
aws dynamodb put-item --table-name td_notes_test --item file://item.json
```


update
```sh
aws dynamodb update-item --table-name td_notes_test --key file://key.json --update-expression "SET #t = :t" --expression-attribute-names file://attribute-names.json --expression-attribute-values file://attribute-values.json 
```

delete
```sh
aws dynamodb delete-item --table-name td_notes_test --key file://key.json 
```

batch write(PutRequest, DeleteRequest)
```sh
aws dynamodb batch-write-item --request-items file://items.json
```

## read operations - item level operations with AWS CLI

```sh
aws dynamodb batch-write-item --request-items file://more-items.json
```


```sh
aws dynamodb get-item --table-name td_notes_test --key file://read-key.json
```


```sh
aws dynamodb query --table-name td_notes_test --key-condition-expression "user_id = :uid" --expression-attribute-value file://expression-attribute-values.json
```

```sh
aws dynamodb query --table-name td_notes_test --key-condition-expression "user_id = :uid AND #t > :t" --expression-attribute-value file://expression-attribute-values2.json --expression-attribute-names file://expression-attribute-names2.json
```

filter
```sh
aws dynamodb query --table-name td_notes_test --key-condition-expression "user_id = :uid AND #t > :t" --expression-attribute-value file://expression-attribute-values2.json --expression-attribute-names file://expression-attribute-names2.json --filter-expression "cat = :cat"
```

```sh
aws dynamodb query --table-name td_notes_test --key-condition-expression "user_id = :uid AND #t > :t" --expression-attribute-value file://expression-attribute-values2.json --expression-attribute-names file://expression-attribute-names2.json --filter-expression "cat = :cat" --return-consumed-capacity INDEXES
```

```sh
aws dynamodb query --table-name td_notes_test --key-condition-expression "user_id = :uid AND #t > :t" --expression-attribute-value file://expression-attribute-values2.json --expression-attribute-names file://expression-attribute-names2.json --filter-expression "cat = :cat" --return-consumed-capacity INDEXES --consistent-read
```

```sh
aws dynamodb query --table-name td_notes_test --key-condition-expression "user_id = :uid AND #t > :t" --expression-attribute-value file://expression-attribute-values2.json --expression-attribute-names file://expression-attribute-names2.json --filter-expression "cat = :cat" --return-consumed-capacity INDEXES --consistent-read --no-scan-index-forward
```

batch-get
```sh

```

scan operation
```sh
aws dynamodb scan --table-name td_notes_test
```

```sh
aws dynamodb scan --table-name td_notes_test --filter-expression "username = :uname" --expression-attribute-values file://uname.json
```



## creating REST API Routes



