# README


https://www.linkedin.com/learning/switching-to-es6-in-node-js/rest-parameters

content table
```sh
Using arrow functions

Using default parameters

Reviewing the class structure in ES6

Assigning variables with let and const

Array destructuring and object destructuring

Replacing callbacks with promises

Keeping values unique with Set

Dropping the prototype with Map

Working with generators

Using yield with arguments


```


## 1 Functions   

### use =>(fat arrow) instead of the keyword function    

```java
var slicePizza = (pizza, sliceNum) => {
	var slices = [];
	for (var i = 0; i < sliceNum; i++) {
		slices.push(pizza.slice());
	}
	return slices;
};


//------
(pizza) => {
	console.log(pizza);
}

pizza => {
	console.log(pizza);
}

//------
(pizza) => {
	return bake(pizza);
}

pizza => bake(pizza);

```   


### Lexical scoping(physical position) in arrow functions    

```



```



