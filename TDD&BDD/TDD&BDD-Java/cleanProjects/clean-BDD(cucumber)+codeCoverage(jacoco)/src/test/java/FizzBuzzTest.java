import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.testng.Assert.assertNull;

/**
 * @author
 * @Date
 * @Version
 */
public class FizzBuzzTest {

    private FizzBuzz fizzBuzz;

    @BeforeTest
    public void setUp() {

        fizzBuzz = new FizzBuzz();
        System.out.println(" in setUp()");
    }

    @Test
    public void getFizzBuzzWord_NumberIsMultipleOfThree_ShouldReturnFizz() {
        System.out.println("in getFizzBuzzWord_NumberIsMultipleOfThree_ShouldReturnFizz() " + fizzBuzz.getFizzBuzzWord(3));
        assertThat( fizzBuzz.getFizzBuzzWord(3), is("Fizz"));
    }

    @Test
    public void getFizzBuzzWord_NumberIsMultipleOfFive_ShouldReturnBuzz() {
        assertThat(fizzBuzz.getFizzBuzzWord(5), is("Buzz"));
    }

    @Test
    public void getFizzBuzzWord_NumberIsMultipleOfThreeAndFive_ShouldReturnFizzBuzz() {
        assertThat(fizzBuzz.getFizzBuzzWord(15), is("FizzBuzz"));
    }

    @Test
    public void getFizzBuzzWord_NumberIsNotMultipleOfThreeOrFive_ShouldReturnNull() {
        assertNull(fizzBuzz.getFizzBuzzWord(4));
    }
}
