



## run test

use the following cmd to run test cases(configured in testng.xml)
```
mvn test
```

use the following cmd to run single test, for example: FizzBuzzTest 
```
mvn -Dtest=FizzBuzzTest test
```


## cucumber for BDD  

The cucumber example code can be seen in 
```
LoginProfile.feature
UpdateProfile.feature
stepdefs/*
TestRunner
```


## Jacoco for code coverage  

mvn clean test (Creates code coverage report for unit tests)   
mvn clean verify -P integration-test (Creates code coverage report for integration tests)    
mvn clean verify -P all-tests (Creates code coverage reports for unit and integration tests)   


view reports
view reports
```
installed the coverage plugin(Emma plugin) in intelliJ. 
Then "Analyse -> Show Coverage Data..." and pick your Jacoco output file(target/coverage-reports/jacoco-ut.exec). 
```

