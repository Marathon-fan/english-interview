package stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import models.ProdctMap;
import models.Product;
import models.ShoppingCart;

import java.text.DecimalFormat;
import java.util.List;
import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.hamcrest.core.IsNull;


public class StepDefinitions {

    ShoppingCart shoppingCart = null;
    ProdctMap prodMap = new ProdctMap();

    @Given("^A user enters the shop, and is assigned with a shopping cart$")
    public void a_user_enters_the_shop_and_is_assigned_with_a_shopping_cart() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        shoppingCart = new ShoppingCart();
    }

    @Given("^The shop has a type of products called \"([^\"]*)\" with a unit price of \"([^\"]*)\"$")
    public void the_shop_has_a_type_of_products_called_with_a_unit_price_of(String arg1, String arg2) throws Throwable {
        prodMap.addProduct(new Product(arg1, new BigDecimal(arg2)));
    }

    @Given("^The tax rate is \"([^\"]*)\"$")
    public void the_tax_rate_is(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        shoppingCart.setTaxRate(new BigDecimal(arg1));
    }

    @When("^The user adds \"([^\"]*)\" units of \"([^\"]*)\" to the shopping cart$")
    public void the_user_adds_units_of_to_the_shopping_cart(String arg1, String arg2) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        final int number = Integer.parseInt(arg1);
        final Product prod = prodMap.getProductMap().get(arg2);
        if (prod != null) {
            for (int i = 0; i < number; i++) {
                shoppingCart.addProd((Product)prod.clone());
            }
        }
    }

    @Then("^The shopping cart should contain \"([^\"]*)\" unit\\(s\\) of product \"([^\"]*)\" with a unit price of \"([^\"]*)\"$")
    public void the_shopping_cart_should_contain_unit_s_of_product_with_a_unit_price_of(String arg1, String arg2, String arg3) throws Throwable {
        final int number = Integer.parseInt(arg1);
        final String name = arg2;
        final BigDecimal price = new BigDecimal(arg3);
        final List<Product> prodList = shoppingCart.getProdListByName(name);

        assertThat(prodList, is(IsNull.notNullValue()));
        assertThat(number, is(prodList.size()));

        for (int i = 0; i < prodList.size(); i++) {
            final Product cur = prodList.get(i);
            assertThat(cur.getName(), is(name));
            assertThat(cur.getPrice(), is(price));
        }

    }

    @Then("^The shopping cart's total tax amount should equal \"([^\"]*)\"$")
    public void the_shopping_cart_s_total_tax_amount_should_equal(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        assertThat(shoppingCart.getTaxAmount().toString(), is(arg1));
    }

    @Then("^The shopping cart's total price should equal \"([^\"]*)\"$")
    public void the_shopping_cart_s_total_price_should_equal(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        assertThat(shoppingCart.getTotalPrice().toString(), is(arg1));
    }

}
