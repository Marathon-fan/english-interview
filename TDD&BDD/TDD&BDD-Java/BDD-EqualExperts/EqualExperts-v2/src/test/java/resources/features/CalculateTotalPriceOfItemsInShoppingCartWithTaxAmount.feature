@CalculateTheTaxRateOfTheShoppingCartWithMultipleItems
Feature: Calculate The Tax Rate Of The Shopping Cart With Multiple Items
  The shop owner wants to have a system that can record the price of products.
  A customer enters the shop and is allocated with a shopping cart
  The customer wants to add a number of products to the shopping cart
  The customer wants to add a number of products(the different kind) to the shopping cart
  The shopping cart can record the number of the each type of products inside, the total price as well as the total tax amount

  Background: The shop opens. A customer enters it and is assigned with a shopping cart
    Given A user enters the shop, and is assigned with a shopping cart
    Given The shop has a type of products called "Dove Soap" with a unit price of "39.99"
    Given The shop has a type of products called "Axe Deo" with a unit price of "99.99"
    Given The tax rate is "0.125"

  Scenario: User can buy different products
    When The user adds "2" units of "Dove Soap" to the shopping cart
    When The user adds "2" units of "Axe Deo" to the shopping cart
    Then The shopping cart should contain "2" unit(s) of product "Dove Soap" with a unit price of "39.99"
    And The shopping cart should contain "2" unit(s) of product "Axe Deo" with a unit price of "99.99"
    And The shopping cart's total tax amount should equal "35.00"
    And The shopping cart's total price should equal "314.96"