package models;

import models.Product;
import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

/**
 * @author
 * @Date
 * @Version
 */
public class ShoppingCart {

    List<Product> prodList = new ArrayList<>();

    BigDecimal totalRawPrice = new BigDecimal("0");

    BigDecimal taxRate = new BigDecimal("0");

    final int SCALE = 2;

    public ShoppingCart() {
    }

    public boolean addProd(Product prod) {
        this.prodList.add(prod);
        this.totalRawPrice = this.totalRawPrice.add(prod.getPrice());
        return true;
    }

    public List<Product> getProdListByName(String prodName) {
        List<Product> list = new ArrayList<>();
        for (Product product : prodList) {
            if (product.getName().equals(prodName)) {
                list.add(product);
            }
        }
        return list;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    public BigDecimal getTaxAmount() {
        return this.totalRawPrice.multiply(this.taxRate).setScale(SCALE, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal getTotalPrice() {

        return  totalRawPrice.multiply(this.taxRate.add(new BigDecimal("1"))).setScale(SCALE, BigDecimal.ROUND_HALF_UP);
    }
}

