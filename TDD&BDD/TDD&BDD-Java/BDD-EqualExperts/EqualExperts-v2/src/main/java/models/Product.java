package models;
import java.math.BigDecimal;


public class Product implements Cloneable {
    String name;
    BigDecimal price;

    public Object clone() throws CloneNotSupportedException{
        return super.clone();
    }

    public Product(String name, BigDecimal price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }


    public BigDecimal getPrice() {
        return price;
    }

}
