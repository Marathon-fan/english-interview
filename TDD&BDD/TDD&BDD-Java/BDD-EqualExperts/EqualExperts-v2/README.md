

## Equal Expert version number   
version number: ​741e2021841b2ee9dfe6da974bbeb934b579947f  

## Introduction    

```sh
1 use cucumber for BDD  
2 use Jacoco for code coverage  
3 use testNG API     
```

## Environment  
Java 8

## Run test  
use the following cmd to run feature test cases
```
mvn clean test
```

## View code coverage(Jacoco)  

view reports
```
installed the coverage plugin(Emma plugin) in IntelliJ. 
then "Analyse -> Show Coverage Data..." and pick your Jacoco output file(target/coverage-reports/jacoco-ut.exec). 
```
