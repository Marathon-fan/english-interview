

## introduction    

```sh
1 use cucumber for BDD  
2 use Jacoco for code coverage  
3 use testNG api     
```

## environment  
java 8

## run test  
use the following cmd to run feature test cases
```
mvn clean test
```

## view code coverage(Jacoco)  

view reports
```
installed the coverage plugin(Emma plugin) in intelliJ. 
Then "Analyse -> Show Coverage Data..." and pick your Jacoco output file(target/coverage-reports/jacoco-ut.exec). 
```

## space for improvement  

1 how to use profile? do we need profile in this project?  

2 step 1's branch coverage is 75%. Does it need to be over 90%?

