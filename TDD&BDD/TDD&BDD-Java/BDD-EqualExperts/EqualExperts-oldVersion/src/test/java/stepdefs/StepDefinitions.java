package stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import models.ProdctMap;
import models.Product;
import models.ShoppingCart;

import java.text.DecimalFormat;
import java.util.List;
import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class StepDefinitions {

    ShoppingCart shoppingCart = null;
    ProdctMap prodMap = null;
    DecimalFormat df = null;

    @Given("^A user enters the shop, and is assigned with a shopping cart$")
    public void a_user_is_assigned_with_shopping_cart() throws Throwable {
        shoppingCart = new ShoppingCart();
        prodMap = new ProdctMap();
        df = new DecimalFormat("#.00");
        df.setRoundingMode(java.math.RoundingMode.CEILING);
    }

    @Given("^The shop has a type of products called \"([^\"]*)\" with a unit price of \"([^\"]*)\"$")
    public void the_shop_adds_a_product_category(String prodName, String prodPrice) throws Throwable {
        prodMap.addProduct(new Product(prodName.trim(), Double.parseDouble(prodPrice.trim())));
    }

    @Given("^The shop has a tax rate \"([^\"]*)\"$")
    public void the_shop_has_a_tax_rate(String taxRate) throws Throwable {
        Double taxRateDouble = Double.parseDouble(taxRate);
        shoppingCart.setTaxRate(taxRateDouble);
    }

    @When("^The user adds \"(\\d+)\" units of \"([^\"]*)\" to the shopping cart$")
    public void the_user_adds_products_first_time(int number, String prodName) throws Throwable {
        Product selectedProd = prodMap.getProductMap().get(prodName);
        for (int i = 0; i < number; i++) {
            shoppingCart.addProd((Product)selectedProd.clone());
        }
    }

    @And("^The user adds another \"([^\"]*)\" units of \"([^\"]*)\" to the shopping cart$")
    public void the_user_adds_products_second_time(int number, String prodName) throws Throwable {
        Product selectedProd = prodMap.getProductMap().get(prodName);
        for (int i = 0; i < number; i++) {
            shoppingCart.addProd((Product)selectedProd.clone());
        }
    }

    @Then("^The shopping cart should contain \"([^\"]*)\" unit\\(s\\) of product \"([^\"]*)\" with a unit price of \"([^\"]*)\"$")
    public void product_name_number_price_should_be_right(String number, String prodName, String prodPrice) throws Throwable {
        List<Product> prodList =  shoppingCart.getProdListByName(prodName);

        int prodNumber = Integer.parseInt(number);
        assertThat(prodNumber, is(prodList.size()));

        assertThat(prodName, is(prodList.get(0).getProdName()));

        double prodPriceShoppingCart = prodList.get(0).getProdPrice();

        assertThat(prodPrice, is(df.format(prodPriceShoppingCart)));
    }

    @And("^The total tax amount should equal \"([^\"]*)\"$")
    public void the_total_tax_amount_should_equal(String totalPrice) throws Throwable {
        String taxAmountShoppingCart = df.format(shoppingCart.getTaxAmount());
        assertThat(totalPrice, is(taxAmountShoppingCart));
    }

    @And("^The shopping cart's total price should equal \"([^\"]*)\"$")
    public void the_shopping_cart_s_total_price_should_equal(String totalPrice) throws Throwable {
        String totalPriceShoppingCart = df.format(shoppingCart.getTotalPrice());
        assertThat(totalPrice, is(totalPriceShoppingCart));
    }

}
