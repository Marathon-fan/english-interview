@CalculateTaxRateOfShoppingCartWithMultipleItems
Feature: Calculate the tax rate of the shopping cart with multiple items
  The shop owner wants to have a system to record the price of different products.
  The customer wants to add a number of products to the shopping cart.
  The customer wants to buy more products of the other type and thus add additional products(of different type) to the shopping cart.
  The shopping cart can record the number of the products(different types) inside it, also, it can calculate the total price and tax amount

  Background: The shop opens. A customer enters it and is assigned with a shopping cart
    Given A user enters the shop, and is assigned with a shopping cart
    Given The shop has a type of products called "Dove Soap" with a unit price of "39.99"
    Given The shop has a type of products called "Axe Deo" with a unit price of "99.99"
    Given The shop has a tax rate "0.125"

  Scenario: User can buy different products
    When The user adds "2" units of "Dove Soap" to the shopping cart
    And The user adds "2" units of "Axe Deo" to the shopping cart
    Then The shopping cart should contain "2" unit(s) of product "Dove Soap" with a unit price of "39.99"
    And The shopping cart should contain "2" unit(s) of product "Axe Deo" with a unit price of "99.99"
    And The total tax amount should equal "35.00"
    And The shopping cart's total price should equal "314.96"