package models;

public class Product implements Cloneable {
    String prodName;
    double prodPrice;

    public Object clone()throws CloneNotSupportedException{
        return super.clone();
    }

    public Product(String prodName, double prodPrice) {
        this.prodName = prodName;
        this.prodPrice = prodPrice;
    }

    public String getProdName() {
        return prodName;
    }


    public double getProdPrice() {
        return prodPrice;
    }

}
