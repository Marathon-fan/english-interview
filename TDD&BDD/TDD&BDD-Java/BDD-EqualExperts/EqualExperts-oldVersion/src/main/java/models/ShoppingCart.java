package models;

import models.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * @author
 * @Date
 * @Version
 */
public class ShoppingCart {

    List<Product> prodList = new ArrayList<>();
    double totalRawPrice = 0;
    double taxRate = 0;

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public ShoppingCart() {
    }

    public boolean addProd(Product prod) {
        this.prodList.add(prod);
        this.totalRawPrice += prod.getProdPrice();
        return true;
    }

    public List<Product> getProdListByName(String prodName) {
        List<Product> list = new ArrayList<>();
        for (Product product : prodList) {
            if (product.getProdName().equals(prodName)) {
                list.add(product);
            }
        }
        return list;
    }

    public double getTaxAmount() {
        return totalRawPrice * taxRate;
    }

        public double getTotalPrice() {
        return totalRawPrice * (1 + taxRate);
    }
}
