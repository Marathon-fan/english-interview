package models;

import java.util.HashMap;
import java.util.Map;

public class ProdctMap {
    Map<String, Product> productMap = new HashMap<>();

    public boolean addProduct(Product prod) {  // add or update
        this.productMap.put(prod.getProdName(), prod);
        return true;
    }

    public Map<String, Product> getProductMap() {
        return productMap;
    }
}
