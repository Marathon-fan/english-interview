
# introduction to TDD    

for Java BDD example1, please see folder: BDD-cucumber-Java-skeleton-gradle&mvn  

for Java BDD example1, please see folder: BDD-cucumber-multipleExamples-mvn  

for Java TDD example1, please see:   
https://bitbucket.org/aceedustation/springbootjavaprepare/src/master/     the package called TDDTestDoublesExample   


JUnit4 and TestNG Feature comparison  
Both TestNG and JUnit4 looks similar, except one or two features. Let's have a comparison between the two to quickly decide, which technology is more favourable for Unit Testing. 
![TestNG vs JUnit](./pics/jUnitVsTestNG.jpg)


## Java big decimal

use big decimal for precise calculation   

```
https://blog.csdn.net/boonya/article/details/53376698
```


## what is TDD   

TDD is an agile methodology   

test types - different level
```
full system test
automated integration/acceptance
testing in concert
component test
unit test
single function test
```

## the test driven development cycle  

small changes safely, step by step
```
1 write test
2 test fails
3 write code
4 test passes    - valid test  
5 refactor       - regression test(the application code first,  and may also the test code)  
```

also be known as **Red Green refactor**

## why TDD

Proactive VS Reactive   

Proactive: seek out problems early on, validate requirements, validate code   
Reactive: test late, only respond to defects, focused on debugging  

Requirements  
Drive out requirements issues early  

Rapid feedback    
Many small changes vs One big change    

Collaboration   
Enables developers to work together   

Values refactoring   
Refactor often to lower impact and risk   

Design to test   
Testing driving good design practice   

Tests as information   
Documenting decisions and assumptions   


# test doubles

## isolating dependencies  

test doubles 
```
stubs fakes mocks
```

## stubs  

returnning canned answers    
recording that state   
providing state-based verification   

## fakes  

emulating the real app bahavior, like database,   

providing simplified replacements   

## mocks   

providing behaviour based verification   
you need to set up expectations    


```
stubs        assertions made
fakes        directly in test method
mocks        assertions make by mock object
```

## frameworks  

### cucumber    

Cucumber is a testing approach which supports Behavior Driven Development (BDD). It explains the behavior of the application in a simple English text using Gherkin language.

Learn more at - https://www.guru99.com/cucumber-tutorials.html


### Selenium    

Selenium is an automation tool for Functional Testing of the web-based application. Selenium supports different language like java, ruby, python C#, etc.


### integrate Cucumber with Selenium Webdriver   

///////////////////

# BDD   

BDD: Behavior Driven Development  

**in BDD, features= stories**   

apply BDD concepts and identify stories (features) in a practical Agile environment    

The business person specifies behaviors they want to see in the system.

The developer asks questions based on their understanding of the system, while also writing down additional behaviors needed from a development perspective.

The testers decides on which system behaviors the acceptance tests will be written.

**Cucumber is first and foremost a collaboration tool that aims to bring a common understanding to software teams — across roles.**
```
Cucumber features should drive the implementation, not reflect it.
Cucumber is not a tool for testing software. It is a tool for testing people’s understanding of how software (yet to be written) should behave.
```

**Feature Mapping** is a simple collaborative practice
```
1 Define a feature or story, or pick one from the backlog.
2 Understand what actors are involved in the story.
3 Break the feature into tasks to identify the main flows.
4 Identify examples that illustrate a principle or variant flow. Ask questions like: “But what if…”; “what else could lead to this outcome”; “what other outcomes might happen”; and use the answers to create new examples. Use rules to explain and give context to your examples.
5 Rinse and repeat for other rules and examples.
6 Create Executable Specifications: automate the main high-level flows with pending steps.
```


The following are the features we discuss. Notice that they are very high-level and abstract.
```
Feature: Login Profile
  As an employee of the company
  I want to login my employee profile using my credentials
  In order to collaborate with my colleagues
Feature: Update Profile
  As an employee of the company
  I want to be able to update my name, projects, email, and phone numbers on my profile
  In order to share my contact information with my colleagues
```

In the case, notice that there is only one actor — the employee

Now we are ready to break feature into tasks and scenarios.
```
Feature: Login Profile
    Scenario: Successful login
    Scenario: Failed login using wrong credentials

Feature: Update Profile
    Scenario: Update name
    Scenario: Add new projects
```

background scenario which helps to successfully perform our expected scenario, successful login
```
Background: User navigates to Company home page
Steps: 1) Go to company home page 
       2) See login option (this is a consequence of above step)
Scenario: Successful login
Steps: 1) Enter correct credentials 
       2) Login to account
       3) See welcome message (this is a consequence of above step)
```

Given/When/Then format of Gherkin language
```
 Background: User navigates to Company home page
  Given I am on the "Company home" page on URL "www.mycomany.com"
  Then I should see "Log In as Employee" message
Scenario: Successful login
  When I fill in "Username" with "Test"
  And I fill in "Password" with "123"
  And I click on the "Log In" button
  Then I am on the "My profile" page on URL "www.mycompany.com/myprofile"
  And I should see "Welcome to your profile" message
  And I should see the "Log out" button
```

//////////////////////////////////////////////////
Gherkin is a language, which is used to write Features, Scenarios, and Steps. 

a good Gherkin tutorial
```
http://docs.behat.org/en/v2.5/guides/1.gherkin.html
```

Gherkin Format and Syntax  

Like YAML or Python, Gherkin is a line-oriented language that uses indentation to define structure. Line endings terminate statements (called steps) and either spaces or tabs may be used for indentation. (We suggest you use spaces for portability.) Finally, most lines in Gherkin start with a special keyword:

Gherkin files are plain text Files and have the extension .feature. Each line that is not blank has to start with a Gherkin keyword, followed by any text you like. The keywords are −

```

Feature

Scenario

Given, When, Then, And, But (Steps)

Background

Scenario Outline

Examples

""" (Doc Strings)

| (Data Tables)

@ (Tags)

# (Comments)

*
```

**Scenario Outlines** allow us to more concisely express these examples through the use of a template with placeholders:
```
Scenario Outline: Eating
  Given there are <start> cucumbers
  When I eat <eat> cucumbers
  Then I should have <left> cucumbers

  Examples:
    | start | eat | left |
    |  12   |  5  |  7   |
    |  20   |  5  |  15  |
```

**features**
```
Every *.feature file conventionally consists of a single feature. Lines starting with the keyword Feature: (or its localized equivalent) followed by three indented lines starts a feature. A feature usually contains a list of scenarios. You can write whatever you want up until the first scenario, which starts with Scenario: (or localized equivalent) on a new line. You can use tags to group features and scenarios together, independent of your file and directory structure.

Every scenario consists of a list of steps, which must start with one of the keywords Given, When, Then, But or And (or localized one). Behat treats them all the same, but you shouldn’t. Here is an example:
```

```
Feature: Serve coffee
  In order to earn money
  Customers should be able to
  buy coffee at all times

  Scenario: Buy last coffee
    Given there are 1 coffees left in the machine
    And I have deposited 1 dollar
    When I press the coffee button
    Then I should be served a coffee
```

**scenarios**
Scenario is one of the core Gherkin structures. Every scenario starts with the Scenario: keyword (or localized one), followed by an optional scenario title. Each feature can have one or more scenarios, and every scenario consists of one or more steps.

The following scenarios each have 3 steps:
```
Scenario: Wilson posts to his own blog
  Given I am logged in as Wilson
  When I try to post to "Expensive Therapy"
  Then I should see "Your article was published."

Scenario: Wilson fails to post to somebody else's blog
  Given I am logged in as Wilson
  When I try to post to "Greg's anti-tax rants"
  Then I should see "Hey! That's not your blog!"

Scenario: Greg posts to a client's blog
  Given I am logged in as Greg
  When I try to post to "Expensive Therapy"
  Then I should see "Your article was published."
```

The Scenario Outline uses placeholders, which are contained within < > in the Scenario Outline’s steps. For example:
```
Given <I'm a placeholder and I'm ok>
```

Think of a placeholder like a variable. It is replaced with a real value from the Examples: table row, where the text between the placeholder angle brackets matches that of the table column header. The value substituted for the placeholder changes with each subsequent run of the Scenario Outline, until the end of the Examples table is reached.


when running the first row of our example:
```
Scenario Outline: controlling order
  Given there are <start> cucumbers
  When I eat <eat> cucumbers
  Then I should have <left> cucumbers

  Examples:
    | start | eat | left |
    |  12   |  5  |  7   |
```    
The scenario that is actually run is:

```
Scenario Outline: controlling order
  # <start> replaced with 12:
  Given there are 12 cucumbers
  # <eat> replaced with 5:
  When I eat 5 cucumbers
  # <left> replaced with 7:
  Then I should have 7 cucumbers
```

**backgrounds** set the context for all scenarios  

Backgrounds allows you to add some context to all scenarios in a single feature. A Background is like an untitled scenario, containing a number of steps. The difference is when it is run: the background is run before each of your scenarios, but after your BeforeScenario hooks (Hooking into the Test Process - Hooks).

![background](./pics/gherkin-bdd-background.jpg)

```
Feature: Multiple site support

  Background:
    Given a global administrator named "Greg"
    And a blog named "Greg's anti-tax rants"
    And a customer named "Wilson"
    And a blog named "Expensive Therapy" owned by "Wilson"

  Scenario: Wilson posts to his own blog
    Given I am logged in as Wilson
    When I try to post to "Expensive Therapy"
    Then I should see "Your article was published."

  Scenario: Greg posts to a client's blog
    Given I am logged in as Greg
    When I try to post to "Expensive Therapy"
    Then I should see "Your article was published."
```

Steps
```
Givens
Whens
Thens
And, But   
```

**givens**  
The purpose of Given steps is to put the system in a known state before the user (or external system) starts interacting with the system (in the When steps). Avoid talking about user interaction in givens. If you have worked with use cases, givens are your preconditions.

**whens**  
The purpose of When steps is to describe the key action the user performs (or, using Robert C. Martin’s metaphor, the state transition).

**thens**  
The purpose of Then steps is to observe outcomes. The observations should be related to the business value/benefit in your feature description. The observations should inspect the output of the system (a report, user interface, message, command output) and not something deeply buried inside it (that has no business value and is instead part of the implementation).


**and, but**
If you have several Given, When or Then steps you can write:
```
Scenario: Multiple Givens
  Given one thing
  Given an other thing
  Given yet an other thing
  When I open my eyes
  Then I see something
  Then I don't see something else
```

Or you can use And or But steps, allowing your Scenario to read more fluently:
```
Scenario: Multiple Givens
  Given one thing
  And an other thing
  And yet an other thing
  When I open my eyes
  Then I see something
  But I don't see something else
```

