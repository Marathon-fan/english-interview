var expect = require('chai').expect;

before('root setup code', function(){
	console.log('Root setup code');
});

after('root teardown code', function(){
	console.log('Root teardown code');
});

beforeEach('root setup for each test', function() {    // before each test case
	console.log('setup code for each test');
});
afterEach('root teardown for each test', function(){   // after each test case
	console.log('teardown code for each test');
});


describe('test_suite1 in mocha_beforeAfter', function(){
	before('setup code', function(){         // before all the test cases
		console.log('Setup code');
	});

	after('teardown code', function(){      // after all the test cases 
		console.log('Teardown code');
	});

	beforeEach('setup for each test', function() {    // before each test case
		console.log('setup code for each test');
	});
	afterEach('teardown for each test', function(){   // after each test case
		console.log('teardown code for each test');
	});
	it('test1', function(){
		console.log('test1');
		expect(true).to.equal(true);
	});
	it('test2', function(){
		console.log('test2');
		expect(true).to.equal(true);
	});
});


