var expect = require('chai').expect;

describe( 'test_suite1 in mochat_nestedTests', function(){
	it('test1', function(){
		expect(true).to.equal(true);
	});
});

describe( 'test_suite2 in mochat_nestedTests', function(){       // nested test suite
	describe('test_suite3', function(){
		it('test3', function(){
			expect(true).to.equal(true);
		});
	});
});



it('expects true to equal true', function(){
    expect(true).to.equal(true);
});
