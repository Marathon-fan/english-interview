var expect = require('chai').expect;

async function myAsyncFunction(callback) {
	setTimeout(function(){
		callback("blah");
	}, 50);
}

it("test_async1", function(){
	myAsyncFunction(function(str){
		console.log('in test_async');
		expect(str).to.equal("doh");
		done();		
	});
});

it("test_async2", function(done){    //???????????
	myAsyncFunction(function(str){
		console.log('in test_async');
		expect(str).to.equal("blah");
		done();		
	});
});

function myPromiseFunction() {
	return new Promise(function(resolve, reject){
		setTimeout(function(){
			resolve("blah");
		}, 50);
	});
}


// 	// it("test_promise", function(){
// 	// 	return myPromisefunction().then(function(res){
// 	// 		expect(res).to.equal("blah");
// 	// 	});
// 	// });

// try {
// 	it("test_async_await", async function(){
// 		//var result =  await myPromiseFunction();
// 		var result =   await myPromiseFunction(); //.resolve();
// 		await expect(result).to.equal("blah");
// 	});
// } catch(err) {
//     console.log(err)
// }
	
