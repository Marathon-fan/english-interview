
# readme


**In the Red phase** you implement a failing unit test. 
**in the Green phase** you make it pass. 
**in the refactor phase** you modify the production code and the test to remove duplication and make the code clean.


## initialize a project with node, cat and chai

use the following cmd to execute testing-for cha test cases   
**./node_modules/mocha/bin/mocha tests**

```sh
npm init -y

# list mocha as a dependency for the dev environment
# for the dependency that we want to use in prod environment, use "npm install --save-prod moduleName"
npm install --save-dev mocha
cat package.json 

# list chai as a dependency for the dev environment
npm install --save-dev chai
cat package.json 

mkdir tests

vi tests/mocha_tests.js
then enter the following content
///////////////a always fail file
var expect = require('chai').expect;

it('expects true to equal true', function(){
    expect(true).to.equal(false);
});
///////////////

./node_modules/mocha/bin/mocha tests
# then we can see mocha report the test case fails

then we change the file to
///////////////a always fail file
var expect = require('chai').expect;

it('expects true to equal true', function(){
    expect(true).to.equal(false);
});
///////////////

./node_modules/mocha/bin/mocha tests

# then we can see mocha report the test case passed

```

Unit tests are generally the lowest level of testing performed by the developer usually at the class or function level.

Unit tests are generally the lowest level of testing performed by the developer usually at the class or function level.

In Test Drive Development you write one failing unit test and then you write the production code to make it pass.

In the Red phase you implement a failing unit test. Then in the Green phase you make it pass. Then in the refactor phase you modify the production code and the test to remove duplication and make the code clean.


TDD's three phases
```sh
TDD has the following phases in its work flow:

RED
• Write a failing unit test (the RED phase)

• Write just enough production code to make that test pass (the GREEN phase)

• Refactor the unit test and the production code to make it clean (the REFACTOR phase).

• Repeat until the feature is complete.
```


Mocha
```
BDD

privides hooks to execute code before and after each individual test or suites of tests
privides an API for testing asynchronous code via Promises
```

Chai
```
Chai is a javascript assertion library

Chai implements an API for specifying "expectations" that follows Behavior Driven Development(BDD) style of testing

Also provides an API for TDD

the BDD API provides a set of test calls that can be chained together to create an expectation that can be read as natural language
i.e. expect(result).to.equal(1)
```


creating a unit test with Mocha
```java
var expect = require('chai').expect

it('returns true', function(){
	expect(call()).to.equal(true);
});
```


test discovery

mocha will automatically search for tests in a "test" directory inside the current working directory

Alternative test directories can be specified on the command line: i.e. 
```
mocha --recursive test_dir_one test_dir_two
or
mocha tests/test_file1.js
```

==============

chai 

truthy in javascript
```
false
0
empty string
null
undefined
NaN(not a number)
```
A value is "Truthy" if it is not "Falsy"

