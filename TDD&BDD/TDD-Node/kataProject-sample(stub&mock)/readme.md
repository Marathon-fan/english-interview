
# readme


**In the Red phase** you implement a failing unit test. 
**in the Green phase** you make it pass. 
**in the refactor phase** you modify the production code and the test to remove duplication and make the code clean.

**Chai**  
Chai is a BDD / TDD assertion library for [node](http://nodejs.org) and the browser that can be delightfully paired with any javascript testing framework.

**Mocha**  
Mocha is a feature-rich JavaScript test framework running on Node.js and in the browser, making asynchronous testing simple and fun.

**Sinon**  
Standalone test spies, stubs and mocks for JavaScript. Works with any unit testing framework.


The Fake test double type actually implements an alternative functional implementation.

Spy keeps track of the function.


Stub expects to be called and return canned data. 
Stub modifies the behavior of the stubbed function call.      
![testStub](./pics/testStub.png)


Mock objects are pre-programmed with expectations that form a specification of the calls they are expected to receive. In fact mocks are a certain kind of stub or fake. However, the additional feature mock objects offer on top of acting as simple stubs or fakes is that they provide a flexible way to specify more directly how your function under test should actually operate.  
![TestMOCK](./pics/TestMOCK.png)


Dummy test doubles rarely expect to actually be called and will often throw an exception if they are.

Dummy objects
Dummy objects are meant to satisfy compile-time check and runtime execution. Dummies do not take part to the test scenario. Some method signatures of the CUT may require objects as parameters. If neither the test nor the CUT care about these objects, we may choose to pass in a Dummy Object. This can be a null reference, an empty object or a constant. Dummy objects are passed around (to dependencies for instance) but never actually used. Usually they are just used to fill parameter lists. They are meant to replace input/output parameters of the components that the CUT interacts with.

In the current example, the parameter delta of the doBusiness method can be set to null or any Integer value without interfering with the test. Of course, this might be different for another test.



## initialize a project with node, cat and chai

use the following cmd to execute testing-for cha test cases   
**./node_modules/mocha/bin/mocha tests**

```sh
npm init -y

# list mocha as a dependency for the dev environment
# for the dependency that we want to use in prod environment, use "npm install --save-prod moduleName"
npm install --save-dev mocha
cat package.json 

# list chai as a dependency for the dev environment
npm install --save-dev chai
cat package.json 

mkdir tests

vi tests/mocha_tests.js
then enter the following content
///////////////a always fail file
var expect = require('chai').expect;

it('expects true to equal true', function(){
    expect(true).to.equal(false);
});
///////////////

# use the following cmd for testing
npm test
# or simply use the following cmd
./node_modules/mocha/bin/mocha tests
# then we can see mocha report the test case fails

then we change the file to
///////////////a always fail file
var expect = require('chai').expect;

it('expects true to equal true', function(){
    expect(true).to.equal(false);
});
///////////////

./node_modules/mocha/bin/mocha tests

# then we can see mocha report the test case passed

```

Unit tests are generally the lowest level of testing performed by the developer usually at the class or function level.

Unit tests are generally the lowest level of testing performed by the developer usually at the class or function level.

In Test Drive Development you write one failing unit test and then you write the production code to make it pass.

In the Red phase you implement a failing unit test. Then in the Green phase you make it pass. Then in the refactor phase you modify the production code and the test to remove duplication and make the code clean.


TDD's three phases
```sh
TDD has the following phases in its work flow:

RED
• Write a failing unit test (the RED phase)

• Write just enough production code to make that test pass (the GREEN phase)

• Refactor the unit test and the production code to make it clean (the REFACTOR phase).

• Repeat until the feature is complete.
```


Mocha
```
BDD

privides hooks to execute code before and after each individual test or suites of tests
privides an API for testing asynchronous code via Promises
```

Chai
```
Chai is a javascript assertion library

Chai implements an API for specifying "expectations" that follows Behavior Driven Development(BDD) style of testing

Also provides an API for TDD

the BDD API provides a set of test calls that can be chained together to create an expectation that can be read as natural language
i.e. expect(result).to.equal(1)
```


creating a unit test with Mocha
```java
var expect = require('chai').expect

it('returns true', function(){
	expect(call()).to.equal(true);
});
```


test discovery

mocha will automatically search for tests in a "test" directory inside the current working directory

Alternative test directories can be specified on the command line: i.e. 
```
mocha --recursive test_dir_one test_dir_two
or
mocha tests/test_file1.js
```

==============

chai 

truthy in javascript
```
false
0
empty string
null
undefined
NaN(not a number)
```
A value is "Truthy" if it is not "Falsy"


///////////////////////////for the promiseSample


```sh
node promiseSample.js 
```

///////////////////////////for index.js

```sh
# lodash
npm install underscore --save

```

```sh
node index.js 
```

///////////////////////////for node server

```sh
npm install express --save
npm install body-parser --save
```

```
node server

then use postman,
addr: http://localhost:3000/hello
header: key: Content-Type   value: application/json
body:
{
	"name": "Jimmy"
}

```


## isolating the unit tests   

**test double definition: test doubles are objects created in the test to replace the real production system collaborators.**
Amost all code depends on and collaborates with other parts of the system. Those other parts of the system are not always easy to replicate in the unit test environment or would make tests slow if used directly.

### unit test isolation with dummies, fakes, stubs, spies, and mocks

what are test doubles?
```
almost all code depends on and collaborates with other parts of the system

those other parts of the system are not always easy to replicate in the unit test environment or would make tests slow if used directly
```

Types of test doubles
```sh
dummy
used when a parameter is needed for the tested method but without actually needing to use the parameter

fake
used as a simpler implementation, e.g. using an in-memory database in the tests instead of doing real database access

stub
used for providing the tested code with "indirect input"
stub are different from spies in that they do NOT call the wrapped function
These objects provide implementations with canned answers that are suitable for the test.

spies
used for verifying "indirect output" of the tested code, by asserting the expectations afterwards, without having defined the expectations before the tested code is executed. It helps in recording information about the indirect object created
These objects provide implementations that record the values that were passed in so they can be sued by the test.

mocks
used for verifying "indirect output" of the tested code, by first defining the expectations before the tested code is executed
The objects are pre-programmed to expect specific calls and parameters and can throw exceptions when necessary
```

spy in Sinon  
A spy keeps track of:  
1 how many times a function was called  
2 what parameters were passed to the function  
3 what value the function returned or if it threw an exception
```js
// example
it ('tests spies', function(){
    var callback = sinon.spy();
    prodFunction(callback);
    expect(callback).to.have.been.called();
})
```

spy in Sinon  
A spy keeps track of:  
1 how many times a function was called  
2 what parameters were passed to the function  
3 what value the function returned or if it threw an exception



APIs for testing calls made to a spy
```js
spy.callCount  
spy.called  
spy.calledWith(arg1, arg2, ...)  
spy.returnValues   
spy.threw   
```

```js
// example-anonymous spy
it ('tests spies', function(){
    var callback = sinon.spy();
    prodFunction(callback);
    expect(callback).to.have.been.called();
})

// method-wrapping spies
// i.e., created on existing functions such as class methods
it('tests spies', function(){
    var tc = new TestClass();
    sinon.spy(tc, "testFunc");
    tc.tetstFunc();
    expect(tc.testFunc).to.have.been.called();
})
```



stub in Sinon   
Stubs are like spies in that they can be anonymous or wrap existing functions   
Stubs support the full Spy testing API   
Stubs are different from spies in that they do NOT call the wrapped function  
Stubs allow you to modify the behavior of the stubbed function call  

```js
// example- stub
it ('tests stub', function(){
    var tc = new TestClass();
    sinon.stub(tc, "testFunc");
    testCall(tc);
    expect(tc.testFunc).to.have.been.called();
})

//The following example is yet another test from PubSubJS which shows how to create an anonymous stub that throws an exception when called.
it(it ('tests stub', function(){
    var message = 'an example message';
    var stub = sinon.stub().throws();
    var spy1 = sinon.spy();
    var spy2 = sinon.spy();

    PubSub.subscribe(message, stub);
    PubSub.subscribe(message, spy1);
    PubSub.subscribe(message, spy2);

    PubSub.publishSync(message, undefined);

    assert(spy1.called);
    assert(spy2.called);
    assert(stub.calledBefore(spy1));
)

```


mock in Sinon   
Sinon mocks provide all the capabilities of Sinon spies and stubs with the addition of pre-programmed expectations  
A mock will verify that the specified expectations have occurred and if not will fail the test

```js
// Sinon mocks
it('tests mock', function(){
    var tc = new TestClass();
    var mock = sinon.mock(tc);
    mock.expects('func').once();
    testCall(tc);
    mock.verify();
})
```

APIs for testing calls made to a mock
```js
expectation.atLeast      -- the mock was called at least the specified number of times
expectation.never        -- verifies the mock was never called
expectation.once         -- verifies the mock was called once
expectation.withArgs     -- the mock was called with the specified arguments and possibly others
expectation.on(obj)      -- the mock was called with the specified object as "this"
```

Sinon Cleanup
```javascript
afterEach(() => {
    sinon.restore();
});
```


mock frameworks
```sh
Sinon.JS

Standalone test spies, stubs and mocks for JavaScript. Works with any unit testing framework.

```

### install sinon     

```sh

npm install --save-dev sinon sinon-chai 

npm install --save request

```


## Best practices for Unit Testing and TDD   

**1 Always do the next simplest test case**

**2 Use Descriptive Test Names**

**3 Keep Test Fast**

**4 Use code coverage tools**  
Istanbul is easy to install(npm install --save-dev nyc) code coverage tool

nyc -- Istanbul's state of the art command line interface, with support for:

**5 Run Your tests multiple times and in Random order**

Running the tests in random order ensures that the tests don't have any dependencies between each other

**5 Use a static code Analysis tool**
JSHint is an excellent open source static code analysis tool

### code coverage analysis    

**1 Line**

**2 Statement**

**3 Branch**

**4 Modified Condition/Decision**

Istanbul is easy to install(npm install --save-dev nyc) code coverage tool. It provides Line, Statement, and Branch coverage

# wrap

unit test

mocha and chai unit testing

test doubles  

best practices of Unit testing and TDD

