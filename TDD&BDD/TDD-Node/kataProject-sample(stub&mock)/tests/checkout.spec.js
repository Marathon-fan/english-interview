const Checkout = require('../checkout.js');
var expect = require('chai').expect;
var checkout;

var chai = require("chai");
var sinon = require('sinon');
var sinonChai = require("sinon-chai");
var request = require("request");
chai.should();
chai.use(sinonChai);

it('can instantiate checkout', function(){
	var ini = new Checkout();
})

beforeEach(function(){
	checkout = new Checkout();
	checkout.addItemPrice('a', 1);
	checkout.addItemPrice('b', 2);

});

it('Can calculate the current total', function(){
	checkout.addItem('a');
	expect(checkout.calculateTotalPrice()).to.equal(1);
});

it('Can add multiple items and get correct total', function(){
	checkout.addItem('a');
	checkout.addItem('b');
	expect(checkout.calculateTotalPrice()).to.equal(3);
});

it('Can add discount rule', function(){
	checkout.addDiscount('a', 3, 2);
});

//xit('Can apply discount rules to the total', function(){   // skip the test case

it('Can apply discount rules to the total', function(){   // skip the test case
	checkout.addDiscount('a', 3, 2);
	checkout.addItem('a');
	checkout.addItem('a');
	checkout.addItem('a');
	expect(checkout.calculateTotalPrice()).to.equal(2);
});

it('Throws when item added with no price', function(){
	expect(function(){
		checkout.addItem('c')
	}).to.throw();
})


describe('read item price from a file', function(){
    // it('Can call getUsers', function(){
    //     getUsers();
    // });
    var spy;

    beforeEach(function(){
		checkout = new Checkout();
		checkout.addItemPrice('a', 1);
		checkout.addItemPrice('b', 2);

        spy = sinon.spy();
        // sinon.stub(request, 'get').callsFake(function(url, callback){
        //     callback({}, {body:'{"fileName":"sampleFile", "filePath":"/samplePath"}'});
        // }); // modify the behavior of the stubbed function call  
    });
    afterEach(function(){
        // sinon.restore();
    });

	it ('Can read item price from a file - no callback', function(){
		sinon.spy(checkout, "readPrice2");
		checkout.readPrice2('sampleFile', '/samplePath');
		expect(checkout.readPrice2).to.have.been.calledWith("sampleFile", "/samplePath");
        //spy.should.have.been.calledWith("sampleFile", "/samplePath");
    });

    it ('Can read item price from a file - callback', function(){
        checkout.readPrice1('sampleFile', '/samplePath', spy);
        spy.should.have.been.calledWith("sampleFile", "/samplePath");
	});

});

