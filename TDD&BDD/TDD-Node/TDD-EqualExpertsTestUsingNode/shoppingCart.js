module.exports = class shoppingCart{
    constructor() {
        this.prodsMap = new Map();
        this.cart = new Map();
        this.taxRate = 0;
        this.totalRawPrice = 0;
        // one problem is that taxAmount is not independent, sovle the problem in next commit
    }

    addProdToMap(prod){
        // console.log('prod.name.trim()', prod.name.trim());
        // console.log('prod.price', prod.name.trim());
        this.prodsMap.set(prod.name.trim(), prod.price);
    }
    getProdMap(){
        return this.prodsMap;
    }

    addProdToCart(prodName, number){
        if(!this.prodsMap.has(prodName)) {   // if there is no such product
            console.log('no so such product', prodName, 'in addProdToCart()');
            return;
        }
        if(!this.cart.has(prodName)) {
            this.cart.set(prodName, 0);
        }
        this.cart.set(prodName, this.cart.get(prodName) + number);
        this.totalRawPrice += parseFloat(this.prodsMap.get(prodName)) * number;
    }

    setTaxRate(rate){
        this.taxRate = rate;
    }
    getTaxAmount(){
        //console.log('in getTaxAmount()', 'this.totalRawPrice ', this.totalRawPrice , 'this.taxRate ', this.taxRate );
        let taxAmountWithoutRound = this.totalRawPrice * this.taxRate;      
        return parseFloat(this.roundToFixed(taxAmountWithoutRound, 2)).toString();
    }

    getProdPrice(prodName) {
        if(!this.prodsMap.has(prodName)) {   // if there is no such product
            console.log('no so such product', prodName, 'in getProdPrice()');
            return -1;
        }
        return parseFloat(this.prodsMap.get(prodName));
    }

    getNumberOfProdInCart(prodName) {
        if(!this.prodsMap.has(prodName)) {   // if there is no such product
            console.log('no so such product', prodName, 'in getNumberOfProdInCart()');
            return -1;
        }
        return parseInt(this.cart.get(prodName));
    }

    getTotalPriceWithTax() {
        console.log('this.totalRawPrice ', this.totalRawPrice);
        let totalPriceWithTaxWithoutRound = (this.totalRawPrice * (1 + this.taxRate));  
        return parseFloat(this.roundToFixed(totalPriceWithTaxWithoutRound, 2)).toString();
    }

    roundToFixed(num, dec){
        dec= dec || 0;
        var  s = String(num);
        console.log('s', s);
        if(num % 1)s= s.replace(/5$/, '6');
        return Number((+s).toFixed(dec));
     }
}