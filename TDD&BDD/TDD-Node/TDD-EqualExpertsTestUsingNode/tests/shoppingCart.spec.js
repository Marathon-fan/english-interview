const shoppingCart = require('../shoppingCart.js');
var expect = require('chai').expect;

var chai = require("chai");
var sinon = require('sinon');
var sinonChai = require("sinon-chai");
var request = require("request");
chai.should();
chai.use(sinonChai);

// it('Can initialize shoppingCart', function(){
//     let ini = new shoppingCart();
// });

describe('test suite for Step1', function(){

    let myShoppingCart;

    beforeEach(function(){
        myShoppingCart = new shoppingCart();
        myShoppingCart.addProdToMap({name:'Dove Soap', price:'39.99'});
    });

    it('Can add 5 ​Dove Soaps ​to the shopping cart with right unit price and total price', function(){

        myShoppingCart.addProdToCart('Dove Soap', 5);

        expect(myShoppingCart.getNumberOfProdInCart('Dove Soap').toString()).to.equal('5');    
        expect(myShoppingCart.getProdPrice('Dove Soap').toString()).to.equal('39.99');    
        expect(myShoppingCart.getTotalPriceWithTax().toString()).to.equal('199.95');
    });

});

describe('test suite for Step2', function(){

    let myShoppingCart;

    beforeEach(function(){
        myShoppingCart = new shoppingCart();
        myShoppingCart.addProdToMap({name:'Dove Soap', price:'39.99'});
    });

    it('Can add 8 ​Dove Soaps ​to the shopping cart with right unit price and total price', function(){

        myShoppingCart.addProdToCart('Dove Soap', 5);
        myShoppingCart.addProdToCart('Dove Soap', 3);

        expect(myShoppingCart.getNumberOfProdInCart('Dove Soap').toString()).to.equal('8');    
        expect(myShoppingCart.getProdPrice('Dove Soap').toString()).to.equal('39.99');
        expect(myShoppingCart.getTotalPriceWithTax().toString()).to.equal('319.92');
    });

});

describe('test suite for Step3', function(){

    let myShoppingCart;

    beforeEach(function(){
        myShoppingCart = new shoppingCart();
        myShoppingCart.addProdToMap({name:'Dove Soap', price:'39.99'});
        myShoppingCart.addProdToMap({name:"Axe Deo", price:'99.99'});
        // myShoppingCart.addProdToMap({name:'​ Axe Deo', price:'99.99'});
        myShoppingCart.setTaxRate(0.125);

        //console.log('myShoppingCart.getProdMap()', myShoppingCart.getProdMap());
    });

    it('Can add 2 ​Dove Soaps, 2 Axe Deo ​to the shopping cart with right unit price, tax amount and total price', function(){

        myShoppingCart.addProdToCart('Dove Soap', 2);
        myShoppingCart.addProdToCart('Axe Deo', 2);

        expect(myShoppingCart.getNumberOfProdInCart('Dove Soap').toString()).to.equal('2');    
        expect(myShoppingCart.getProdPrice('Dove Soap').toString()).to.equal('39.99');

        expect(myShoppingCart.getNumberOfProdInCart('Axe Deo').toString()).to.equal('2');    
        expect(myShoppingCart.getProdPrice('Axe Deo').toString()).to.equal('99.99');

        expect(myShoppingCart.getTaxAmount('Axe Deo').toString()).to.equal(parseFloat('35.00').toString());
        expect(myShoppingCart.getTotalPriceWithTax().toString()).to.equal('314.96');
    });

});

